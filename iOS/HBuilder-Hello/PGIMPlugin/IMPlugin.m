//
//  IMPlugin.m
//  HBuilder
//
//  Created by 江源泉 on 2018/2/1.
//  Copyright © 2018年 DCloud. All rights reserved.
//

#import "IMPlugin.h"
#import "IMConfig.h"

typedef enum {
    IMG = 0,
    AUDIO,
    VIDEO,
    FILES
} FileType;


@implementation IMPlugin
static IMPlugin *_sharedInstance = nil;


- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initIM];
        
        
    }
    return self;
}

+ (instancetype)sharedInstance
{
   

    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        if (_sharedInstance == nil) {
            _sharedInstance = [[IMPlugin alloc] init];
            
            
          
            
            }
        
    });
   
  
    return _sharedInstance;
    
    // TODO:
//    return [self init];
}

//-(NSMutableArray *)listners{
//    _listners = [[NSMutableArray alloc] init];
//    return _listners;
//}
-(NSString *)getConCallBackId{
    _getConCallBackId = [NSString string];
    return _getConCallBackId;
}
-(NSDictionary *)getConDic{
    _getConDic = [NSMutableDictionary dictionary];
    return _getConDic;
}

-(NSDictionary *)convertjsonStringToDict:(NSString *)jsonString{
    
    NSDictionary *retDict = nil;
    if ([jsonString isKindOfClass:[NSString class]]) {
        NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        retDict = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:NULL];
        return  retDict;
    }else{
        return retDict;
    }
    
}
-(void)testSend:(NSString *)callbackId{
    PDRPluginResult *result = [PDRPluginResult resultWithStatus:PDRCommandStatusOK messageAsString:@"test back succ11"];
    [self toCallback:callbackId withReslut:[result toJSONString]];
}
-(BOOL)isLogin{
     NSString *currentUser =  [[TIMManager sharedInstance] getLoginUser];
    if (currentUser == nil) {
        return NO;
    }
    return YES;
}
-(void)callIMCmdOnLogin:(NSString *)cbId resultDic:(NSDictionary *)resultDic{
    NSString *actionStr = resultDic[@"action"];
    if ([actionStr isEqualToString:@"cmd_create_group"]){
        
        NSLog(@"cmd_create_group:%@",resultDic[@"action"]);
        //创建群组
        [self creatGroup:cbId Result:resultDic];
    }else if ([actionStr isEqualToString:@"cmd_send_msg"]){
        NSLog(@"cmd_send_msg:%@",resultDic[@"action"]);
        //发送语音文件消息
        [self sendMessage:cbId Result:resultDic];
        
    }else if ([actionStr isEqualToString:@"cmd_msg_list"]){
        //获取本地缓存会话消息
        NSLog(@"cmd_msg_list:%@",resultDic[@"action"]);
        [self getLocalMessageList:cbId Result:resultDic];
        
    }else if ([actionStr isEqualToString:@"cmd_sound_file"]){
        NSLog(@"cmd_sound_file:%@",resultDic[@"action"]);
        // 录音并获取语音文件
        [self getVoiceFile:cbId Result:resultDic];
        
    }else if ([actionStr isEqualToString:@"cmd_quit_group"]){
        NSLog(@"cmd_quit_group:%@",resultDic[@"action"]);
        //退出群组
        [self quitGroup:cbId Result:resultDic];
    }else if ([actionStr isEqualToString:@"cmd_delete_group_member"]){
        NSLog(@"cmd_delete_group_member:%@",resultDic[@"action"]);
        //踢出群成员
        [self deleteGroupMember:cbId Result:resultDic];
    }else if ([actionStr isEqualToString:@"cmd_add_group_member"]){
        NSLog(@"cmd_add_group_member:%@",resultDic[@"action"]);
        //添加群成员
        [self addGroupMember:cbId Result:resultDic];
    }else if ([actionStr isEqualToString:@"cmd_get_group_member"]){
        NSLog(@"cmd_get_group_member:%@",resultDic[@"action"]);
        //获取群成员信息
        [self getGroupMember:cbId Result:resultDic];
    }else if ([actionStr isEqualToString:@"cmd_update_group_name"]){
        NSLog(@"cmd_update_group_name:%@",resultDic[@"action"]);
        //更新群名
        [self updateGroupName:cbId Result:resultDic];
    }else if ([actionStr isEqualToString:@"cmd_read_msg"]){
        NSLog(@"cmd_read_msg:%@",resultDic[@"action"]);
        //设置会话全部消息已读
        [self setReadMsg:cbId Result:resultDic];
    }else if ([actionStr isEqualToString:@"cmd_resend_msg"]){
        NSLog(@"cmd_resend_msg:%@",resultDic[@"action"]);
        //发送文本消息
        [self resendMessage:cbId Result:resultDic];
    }
}
-(void)reLogin:(NSString *)cbId resultDic:(NSDictionary *)resultDic{
    IMPlugin *IMPluginManager = [IMPlugin sharedInstance];
    TIMLoginParam *loginParam =  [[TIMLoginParam alloc] init];
    NSString *currentIdentifier  = [[NSUserDefaults standardUserDefaults] objectForKey:kIMPlugin_CurrentUserIdentifier_Key];
   
        loginParam.identifier = currentIdentifier;
    
   
    NSString *currentUserSig  = [[NSUserDefaults standardUserDefaults] objectForKey:kIMPlugin_CurrentUserSig_Key];
//    NSString *resultUserSig  = [NSString stringWithFormat:@"%@",testUsetSign];
    //                loginParam.userSig = resultUserSig;   //测试userSign
    loginParam.userSig = currentUserSig;
    [[TIMManager sharedInstance] login: loginParam succ:^(){
        [[IMPlugin sharedInstance] setAutoLogin:YES];
        
        
        NSString *currentUser =  [[TIMManager sharedInstance] getLoginUser];
        if ([self isLogin]) {
            [[NSUserDefaults standardUserDefaults] setObject:loginParam.identifier forKey:kIMPlugin_CurrentUserIdentifier_Key];
            [[NSUserDefaults standardUserDefaults] setObject:loginParam.userSig forKey:kIMPlugin_CurrentUserSig_Key];
            [self setAutoSendLogin:NO];
            [self callIMCmdOnLogin:cbId resultDic:resultDic];
         
            
            
        }
        
        
        
        
        NSLog(@"Login Succ is %@",currentUser);
    } fail:^(int code, NSString * err) {
        [[IMPlugin sharedInstance] setAutoLogin:NO];
        NSLog(@"Login Failed: %d->%@", code, err);
    }];
}
- (void)callIMCmd:(PGMethod*)commands
{
    if ( commands ) {
        
        // CallBackid 异步方法的回调id，H5+ 会根据回调ID通知JS层运行结果成功或者失败
        NSString* cbId = [commands.arguments objectAtIndex:0];
        
        // 用户的参数会在第二个参数传回
        NSString* pArgument1 = [commands.arguments objectAtIndex:1];
        NSDictionary *resultDic =  [self convertjsonStringToDict:pArgument1];
        
        NSString *actionStr = resultDic[@"action"];
        if ([actionStr isEqualToString:@"cmd_init"]) {
            NSLog(@"cmd_init:%@",resultDic[@"action"]);

            IMPlugin *IMPluginManager = [IMPlugin sharedInstance];
            NSArray *returnArr = [[NSMutableArray alloc] init];
            PDRPluginResult *result = [PDRPluginResult resultWithStatus:PDRCommandStatusOK messageAsArray:returnArr];
            // 通知JS层Native层运行结果
            [self toCallback:cbId withReslut:[result toJSONString]];
            
        }else if ([actionStr isEqualToString:@"cmd_login"]){
            NSLog(@"cmd_login:%@",resultDic[@"action"]);
          
                IMPlugin *IMPluginManager = [IMPlugin sharedInstance];
                TIMLoginParam* loginParam =  [[TIMLoginParam alloc] init];
            [[NSUserDefaults standardUserDefaults] setObject:resultDic[@"identifier"] forKey:kIMPlugin_CurrentUserIdentifier_Key];
                NSString *currentIdentifier  = [[NSUserDefaults standardUserDefaults] objectForKey:kIMPlugin_CurrentUserIdentifier_Key];
                if (resultDic[@"identifier"] == nil) {
                    loginParam.identifier = currentIdentifier;
                }else{
                    loginParam.identifier = resultDic[@"identifier"];
                }
            [[NSUserDefaults standardUserDefaults] setObject:resultDic[@"userSig"] forKey:kIMPlugin_CurrentUserSig_Key];
            NSString *currentUserSig  = [[NSUserDefaults standardUserDefaults] objectForKey:kIMPlugin_CurrentUserSig_Key];
            if (resultDic[@"userSig"] == nil) {
                loginParam.userSig = currentUserSig;
            }else{
                loginParam.userSig = resultDic[@"userSig"];
            }
                
//                NSString *resultUserSig  = [NSString stringWithFormat:@"%@",testUsetSign];
//                loginParam.userSig = resultUserSig;   //测试userSign
//                loginParam.userSig = resultDic[@"userSig"];
//            [self login:loginParam cbId:cbId resultDic:resultDic succ:^{
//                [self setAutoSendLogin:NO];
//            } fail:^(int code, NSString *msg) {
//                [self setAutoSendLogin:NO];
//            }];
            
            
            BOOL isSendLogin = [self isAutoSendLogin] ;
            if (isSendLogin) {
                PDRPluginResult *result = [PDRPluginResult resultWithStatus:PDRCommandStatusOK messageAsString:@"Auto Send Login Command,please Send after 50 second"];
                // 通知JS层Native层运行结果
                [self toCallback:cbId withReslut:[result toJSONString]];
            }else{
                [self setAutoSendLogin:YES];
                //注册登录成功后返回 群组列表和会话列表的通知
                
                [self login:loginParam cbId:cbId resultDic:resultDic succ:^{
                   
                } fail:^(int code, NSString *msg) {
                   
                }];
            }
          
//                [self loginGetGroupList:loginParam cbId:cbId resultDic:resultDic succ:^{
//
//
//                } fail:^(int code, NSString *msg) {
//
//                }];
            
            
//            IMPlugin *IMPluginManager = [IMPlugin sharedInstance];
//             TIMLoginParam* loginParam =  [[TIMLoginParam alloc] init];
//            loginParam.identifier = resultDic[@"identifier"];
//            NSString *resultUserSig  = [NSString stringWithFormat:@"%@",testUsetSign];
////            loginParam.userSig = resultUserSig;   //测试userSign
//            loginParam.userSig = resultDic[@"userSig"];
//            [self login:loginParam cbId:cbId resultDic:resultDic succ:^{
//
//            } fail:^(int code, NSString *msg) {
//
//            }];
           
          
        }else if ([actionStr isEqualToString:@"cmd_logout"]){
             NSLog(@"cmd_logout:%@",resultDic[@"action"]);
            [self logout:cbId Result:resultDic succ:^{
                [self removeListnersWithPath:@"listners"];
            } fail:^(int code, NSString *msg) {
                
            }];
        }else if ([actionStr isEqualToString:@"cmd_conversations"]){
            
            NSString *conversationType = resultDic[@"type"];
            if ([conversationType isEqualToString:@"c2c"]) {
       
                NSLog(@"notCheckUser:%@",resultDic[@"notCheckUser"]);
                NSString *isNotCheckUser = resultDic[@"notCheckUser"];
                if ([isNotCheckUser boolValue]) {

                }else{
                    IMPlugin *IMPluginManager = [IMPlugin sharedInstance];
                    TIMLoginParam* loginParam =  [[TIMLoginParam alloc] init];
                    NSString *currentIdentifier  = [[NSUserDefaults standardUserDefaults] objectForKey:kIMPlugin_CurrentUserIdentifier_Key];
                    if (resultDic[@"identifier"] == nil) {
                        loginParam.identifier = currentIdentifier;
                    }else{
                        loginParam.identifier = resultDic[@"identifier"];
                    }

                    NSString *currentUserSig  = [[NSUserDefaults standardUserDefaults] objectForKey:kIMPlugin_CurrentUserSig_Key];
                     NSString *resultUserSig  = [NSString stringWithFormat:@"%@",testUsetSign];
                    if (currentUserSig == nil) {
                        loginParam.userSig = resultUserSig;
                    }else{
                        loginParam.userSig = currentUserSig;
                    }
                    
                 
                    
                    
                    [self loginGetConversationList:loginParam cbId:cbId resultDic:resultDic succ:^{
                       

                    } fail:^(int code, NSString *msg) {

                    }];

                }
            }else{
                NSLog(@"notCheckUser:%@",resultDic[@"notCheckUser"]);
                NSString *isNotCheckUser = resultDic[@"notCheckUser"];
                if ([isNotCheckUser boolValue]) {
                    
                }else{
                    IMPlugin *IMPluginManager = [IMPlugin sharedInstance];
                    TIMLoginParam* loginParam =  [[TIMLoginParam alloc] init];
                    NSString *currentIdentifier  = [[NSUserDefaults standardUserDefaults] objectForKey:kIMPlugin_CurrentUserIdentifier_Key];
                    if (resultDic[@"identifier"] == nil) {
                        loginParam.identifier = currentIdentifier;
                    }else{
                        loginParam.identifier = resultDic[@"identifier"];
                    }
                    
                    NSString *currentUserSig  = [[NSUserDefaults standardUserDefaults] objectForKey:kIMPlugin_CurrentUserSig_Key];
                    NSString *resultUserSig  = [NSString stringWithFormat:@"%@",testUsetSign];
                    if (currentUserSig == nil) {
                        loginParam.userSig = resultUserSig;
                    }else{
                        loginParam.userSig = currentUserSig;
                    }
                    
//                    NSString *resultUserSig  = [NSString stringWithFormat:@"%@",testUsetSign];
//                    loginParam.userSig = resultUserSig;
                    //测试userSign
                    //            loginParam.userSig = resultDic[@"userSig"];
                    [self loginGetGroupList:loginParam cbId:cbId resultDic:resultDic succ:^{
                        
                        
                    } fail:^(int code, NSString *msg) {
                        
                    }];
                    
                }
            }
           
//            IMPlugin *IMPluginManager = [IMPlugin sharedInstance];
            
            
          
           
        }else if ([actionStr isEqualToString:@"cmd_send_event"]){
            NSLog(@"cmd_send_event:%@",resultDic[@"action"]);
            //注册通知事件
            [self sendEvent:cbId Result:resultDic];
        }else if ([actionStr isEqualToString:@"registerIMListener"]){
            NSLog(@"registerIMListener:%@",resultDic[@"action"]);
            //注册通知事件
            [self registerIMListener:cbId Result:resultDic];
//            [self sendEvent:cbId Result:resultDic];
        }else{
             NSLog(@"else1%@",resultDic[@"action"]);
            if ([self isLogin]) {
                [self callIMCmdOnLogin:cbId resultDic:resultDic];
            }else{
                [self reLogin:cbId resultDic:resultDic];
            }
            
        }
        NSLog(@"1%@",resultDic);
        NSLog(@"2%@",resultDic[@"action"]);
        
//        NSDictionary* pResultDic = [NSDictionary dictionaryWithObjects:pArgument1 forKeys:[NSMutableArray arrayWithObjects:@"RetArgu1",@"RetArgu2",@"RetArgu3", nil]];
//        for (NSInteger i = 0; i < pArgument1.count; i++) {
//            NSLog(@"%@",pArgument1[i]);
//        }
       
        // 创建一个作为返回值的NSDictionary
       
        // 运行Native代码结果和预期相同，调用回调通知JS层运行成功并返回结果
//        PDRPluginResult *result = [PDRPluginResult resultWithStatus:PDRCommandStatusOK messageAsDictionary:resultDic];
        
        // 如果Native代码运行结果和预期不同，需要通过回调通知JS层出现错误，并返回错误提示
        //PDRPluginResult *result = [PDRPluginResult resultWithStatus:PDRCommandStatusError messageAsString:@"惨了! 出错了！ 咋(wu)整(liao)"];
        
        // 通知JS层Native层运行正确结果
//        [self toCallback:cbId withReslut:[result toJSONString]];
    }
}

- (void)registerIMListener:(PGMethod*)commands
{
    NSLog(@"registerIMListener");
    if ( commands ) {
        
        // CallBackid 异步方法的回调id，H5+ 会根据回调ID通知JS层运行结果成功或者失败
        NSString* cbId = [commands.arguments objectAtIndex:0];
        
        // 用户的参数会在第二个参数传回
        NSString* pArgument1 = [commands.arguments objectAtIndex:1];
        NSDictionary *resultDic =  [self convertjsonStringToDict:pArgument1];
        
        NSString *actionStr = resultDic[@"action"];
        [self addListner:cbId action:actionStr];
        
        
        
        
        // 运行Native代码结果和预期相同，调用回调通知JS层运行成功并返回结果
        PDRPluginResult *result = [PDRPluginResult resultWithStatus:PDRCommandStatusOK messageAsDictionary:resultDic];
        
        // 如果Native代码运行结果和预期不同，需要通过回调通知JS层出现错误，并返回错误提示
        //PDRPluginResult *result = [PDRPluginResult resultWithStatus:PDRCommandStatusError messageAsString:@"惨了! 出错了！ 咋(wu)整(liao)"];
        
        // 通知JS层Native层运行结果
        [self toCallback:cbId withReslut:[result toJSONString]];
    }
}

-(void)addListner:(NSString *)callbackId action:(NSString *)action{
    NotifyModel *notifyModel = [[NotifyModel alloc] init];
    notifyModel.callbackId = callbackId ;
    notifyModel.action = action ;
    if (_listnersArr == nil) {
        _listnersArr = [[NSArray alloc] init];
         _listnersArr = [self getNotifyModelArrDataByPath:@"listners"];
        if (_listnersArr.count == 0) {
            _listners = [[NSMutableArray alloc] init];
        }else{
            _listners = [_listnersArr mutableCopy];
//            [NSMutableArray arrayWithArray:_listnersArr];
        }
    }
    
    if (_listners == nil) {
       
//        _listners = [NSMutableArray sarray];
       
//        if (listnersArr.count == 0) {
//            _listners = [[NSMutableArray alloc] init];
//        }else{
//            _listners = [NSMutableArray arrayWithArray:listnersArr];
//        }
        
        
    }
    
    NSInteger listnersCount = _listners.count ;
    
    for (NSInteger i = 0; i < listnersCount; i++) {
        NotifyModel *itemModel = _listners[i];
//        notifyModel *items = [notifyModel init];
//        itemModel = _listners[i];
        if ([itemModel.callbackId isEqualToString:callbackId]) {
            NSLog(@"already exsit listeners===%@",action);
            return;
        }
    }
    //否则添加
    [_listners addObject:notifyModel];
    [self saveNotifyModelArr:_listners path:@"listners"];
    
//    NSMutableArray *notifyModelArr  =[self getNotifyModelArrDataByPath:@"listners"];
    
}
-(void)removeListeners:(NSString *)callbackId{
    if (_listners == nil) {
//        _listners = [[NSMutableArray alloc] init];
        _listners = [NSMutableArray arrayWithArray: [self getNotifyModelArrDataByPath:@"listners"]];
    }
    if (_listners.count > 0) {
        for (NSInteger i = _listners.count -1; i>= 0; i--) {
            NotifyModel *notifyModel = [[NotifyModel alloc] init];
            notifyModel = _listners[i];
            if ([notifyModel.callbackId isEqualToString:callbackId]) {
                [_listners removeObjectAtIndex:i];
                NSLog(@"remove the listener ==%@,action=%@",callbackId,notifyModel.action);
                break;
            }
        }
        [self saveNotifyModelArr:_listners path:@"listners"];
    }else{
        
    }
   
    
}


- (NSData*)callIMCmdSync:(PGMethod*)command
{
   
    // 用户的参数会在第二个参数传回
    NSString* pArgument1 = [command.arguments objectAtIndex:0];
    NSDictionary *resultDic =  [self convertjsonStringToDict:pArgument1];
    
    NSString *actionStr = resultDic[@"action"];
    if ([actionStr isEqualToString:@"sync_cmd_login_status"]) {
        return [self resultWithBool:[self isLogin]];
        
    }else if ([actionStr isEqualToString:@"sync_cmd_delete_msg"]){
        
           return [self resultWithBool:[self deleteMessage:resultDic]];
    }
    
    // 按照字符串方式返回结果
       return [self resultWithString: actionStr];
}

- (void)unregisterIMListener:(PGMethod*)command
{
    
    // CallBackid 异步方法的回调id，H5+ 会根据回调ID通知JS层运行结果成功或者失败
    NSString* cbId = [command.arguments objectAtIndex:0];
    // 用户的参数会在第二个参数传回
    NSString* pArgument1 = [command.arguments objectAtIndex:1];
    NSDictionary *resultDic =  [self convertjsonStringToDict:pArgument1];
    
    NSString *actionStr = resultDic[@"action"];
    NSString *removeListenerCbId = resultDic[@"CallBackID"];
    [self removeListeners:removeListenerCbId];
    
    // 运行Native代码结果和预期相同，调用回调通知JS层运行成功并返回结果
//    PDRPluginResult *result = [PDRPluginResult resultWithStatus:PDRCommandStatusOK messageAsDictionary:resultDic];
    
    // 如果Native代码运行结果和预期不同，需要通过回调通知JS层出现错误，并返回错误提示
    //PDRPluginResult *result = [PDRPluginResult resultWithStatus:PDRCommandStatusError messageAsString:@"惨了! 出错了！ 咋(wu)整(liao)"];
    
    // 通知JS层Native层运行结果
//    [self toCallback:cbId withReslut:[result toJSONString]];
  
}

//***********************     初始化IMSDK     ***********************//
-(void)initIM{
    TIMManager * manager = [TIMManager sharedInstance];
    [manager setEnv:0];
    [self setConversationList:NO];
    [self setGroupList:NO];
    [self setAutoSendLogin:NO];
    [self removeListnersWithPath:@"listners"];

    
    //添加消息回调
    [[TIMManager sharedInstance] addMessageListener:self];
    
    TIMSdkConfig *config = [[TIMSdkConfig alloc] init];
    config.sdkAppId = [kSdkAppId intValue] ;
    config.accountType = kSdkAccountType;
    config.disableCrashReport = NO;
    
    
    config.connListener = self;
    [manager initSdk:config];
    
    TIMUserConfig *userConfig = [[TIMUserConfig alloc] init];
    //    userConfig.disableStorage = YES;//禁用本地存储（加载消息扩展包有效）
    //    userConfig.disableAutoReport = YES;//禁止自动上报（加载消息扩展包有效）
    //    userConfig.enableReadReceipt = YES;//开启C2C已读回执（加载消息扩展包有效）
    userConfig.disableRecnetContact = NO;//不开启最近联系人（加载消息扩展包有效）
    userConfig.disableRecentContactNotify = YES;//不通过onNewMessage:抛出最新联系人的最后一条消息（加载消息扩展包有效）
    userConfig.enableFriendshipProxy = YES;//开启关系链数据本地缓存功能（加载好友扩展包有效）
    userConfig.enableGroupAssistant = YES;//开启群组数据本地缓存功能（加载群组扩展包有效）
    TIMGroupInfoOption *giOption = [[TIMGroupInfoOption alloc] init];
    giOption.groupFlags = 0xffffff;//需要获取的群组信息标志（TIMGetGroupBaseInfoFlag）,默认为0xffffff
    giOption.groupCustom = nil;//需要获取群组资料的自定义信息（NSString*）列表
    userConfig.groupInfoOpt = giOption;//设置默认拉取的群组资料
    TIMGroupMemberInfoOption *gmiOption = [[TIMGroupMemberInfoOption alloc] init];
    gmiOption.memberFlags = 0xffffff;//需要获取的群成员标志（TIMGetGroupMemInfoFlag）,默认为0xffffff
    gmiOption.memberCustom = nil;//需要获取群成员资料的自定义信息（NSString*）列表
    userConfig.groupMemberInfoOpt = gmiOption;//设置默认拉取的群成员资料
    TIMFriendProfileOption *fpOption = [[TIMFriendProfileOption alloc] init];
    fpOption.friendFlags = 0xffffff;//需要获取的好友信息标志（TIMProfileFlag）,默认为0xffffff
    fpOption.friendCustom = nil;//需要获取的好友自定义信息（NSString*）列表
    fpOption.userCustom = nil;//需要获取的用户自定义信息（NSString*）列表
    userConfig.friendProfileOpt = fpOption;//设置默认拉取的好友资料
    userConfig.userStatusListener = self;//用户登录状态监听器
    userConfig.refreshListener = self;//会话刷新监听器（未读计数、已读同步）（加载消息扩展包有效）
    //    userConfig.receiptListener = self;//消息已读回执监听器（加载消息扩展包有效）
    //    userConfig.messageUpdateListener = self;//消息svr重写监听器（加载消息扩展包有效）
    //    userConfig.uploadProgressListener = self;//文件上传进度监听器
    //    userConfig.groupEventListener todo
//    userConfig.messgeRevokeListener = self.conversationMgr;
    userConfig.friendshipListener = self;//关系链数据本地缓存监听器（加载好友扩展包、enableFriendshipProxy有效）
    userConfig.groupListener = self;//群组据本地缓存监听器（加载群组扩展包、enableGroupAssistant有效）
    [manager setUserConfig:userConfig];
}

#pragma mark --- TIMConnListenerDelegate
- (void)onConnSucc {
    NSLog(@"Connect Succ");
}

- (void)onConnFailed:(int)code err:(NSString*)err {
//      code 错误码：具体参见错误码表
    NSLog(@"Connect Failed: code=%d, err=%@", code, err);
}

- (void)onDisconnect:(int)code err:(NSString*)err {
//      code 错误码：具体参见错误码表
    NSLog(@"Disconnect: code=%d, err=%@", code, err);
}
#pragma mark --- TIMMessageListenerDelegate
- (void)onNewMessage:(NSArray*) msgs {
    NSLog(@"NewMessages: %@", msgs);
    NSDictionary *data = [NSDictionary dictionary];
    NSArray *newMessageArr = [self parseMsgList:msgs];
    
    data = [self createListenerData:newMessageArr];
    [self notifyListeners:@"action_new_msg" data:data];
    
}
-(void) toErrorCallback: (NSString*) callbackId withCode:(int)errorCode  withMessage:(NSString*)message keepCallback:(BOOL)keepCallback{
    NSLog(@"toErrorCallback: %@", message);
}
#pragma mark --- TIMRefreshListenerDelegate
-(void)onRefreshConversations:(NSArray *)conversations{
    NSDictionary *data = [NSDictionary dictionary];
    data = [self createListenerData:[NSDictionary dictionary]];
    [self notifyListeners:@"action_update_conversation2" data:data];
}

#pragma mark --- TIMRefreshListenerDelegate

/**
 *  有新用户加入群时的通知回调
 *
 *  @param groupId     群ID
 *  @param membersInfo 加群用户的群资料（TIMGroupMemberInfo*）列表
 */
- (void)onMemberJoin:(NSString*)groupId membersInfo:(NSArray*)membersInfo{
    NSDictionary *data = [NSDictionary dictionary];
    data = [self createListenerData:[NSDictionary dictionary]];
    [self notifyListeners:ACTION_UPDATE_CONVERSATION data:data];
}

/**
 *  有群成员退群时的通知回调
 *
 *  @param groupId 群ID
 *  @param members 退群成员的identifier（NSString*）列表
 */
- (void)onMemberQuit:(NSString*)groupId members:(NSArray*)members{
    NSDictionary *data = [NSDictionary dictionary];
    data = [self createListenerData:[NSDictionary dictionary]];
    [self notifyListeners:ACTION_UPDATE_CONVERSATION data:data];
}

/**
 *  群成员信息更新的通知回调
 *
 *  @param groupId     群ID
 *  @param membersInfo 更新后的群成员资料（TIMGroupMemberInfo*）列表
 */
- (void)onMemberUpdate:(NSString*)groupId membersInfo:(NSArray*)membersInfo{
    NSDictionary *data = [NSDictionary dictionary];
    data = [self createListenerData:[NSDictionary dictionary]];
    [self notifyListeners:ACTION_UPDATE_CONVERSATION data:data];
}

/**
 *  加入群的通知回调
 *
 *  @param groupInfo 加入群的群组资料
 */
- (void)onGroupAdd:(TIMGroupInfo*)groupInfo{
    NSDictionary *data = [NSDictionary dictionary];
    data = [self createListenerData:[NSDictionary dictionary]];
    [self notifyListeners:ACTION_UPDATE_CONVERSATION data:data];
}

/**
 *  解散群的通知回调
 *
 *  @param groupId 解散群的群ID
 */
- (void)onGroupDelete:(NSString*)groupId{
    NSDictionary *data = [NSDictionary dictionary];
    data = [self createListenerData:[NSDictionary dictionary]];
    [self notifyListeners:ACTION_UPDATE_CONVERSATION data:data];
}

/**
 *  群资料更新的通知回调
 *
 *  @param groupInfo 更新后的群资料信息
 */
- (void)onGroupUpdate:(TIMGroupInfo*)groupInfo{
    NSDictionary *data = [NSDictionary dictionary];
    data = [self createListenerData:[NSDictionary dictionary]];
    [self notifyListeners:ACTION_UPDATE_CONVERSATION data:data];
}

//***********************     登录IMSDK     ***********************//
- (int)login:(TIMLoginParam*)param cbId:(NSString *)cbId resultDic:(NSDictionary *)resultDic succ:(TIMLoginSucc)succ fail:(TIMFail)fail{
    TIMLoginParam * login_param = [[TIMLoginParam alloc ]init];
    
    // identifier为用户名，userSig 为用户登录凭证
    // appidAt3rd 在私有帐号情况下，填写与sdkAppId 一样
    login_param.identifier = param.identifier;
    login_param.userSig = param.userSig;
    login_param.appidAt3rd = kSdkAppId;
    
    NSString *currentUser =  [[TIMManager sharedInstance] getLoginUser];
    if (currentUser == nil && param.identifier == nil) {
        return 0;
    }else if(currentUser == nil){
//          [self setAutoSendLogin:YES];
        [[TIMManager sharedInstance] login: login_param succ:^(){
            [[IMPlugin sharedInstance] setAutoLogin:YES];
           
            
            NSString *currentUser =  [[TIMManager sharedInstance] getLoginUser];
            if (currentUser) {
                [[NSUserDefaults standardUserDefaults] setObject:param.identifier forKey:kIMPlugin_CurrentUserIdentifier_Key];
                 [self setAutoSendLogin:NO];
                
                // 2.创建通知
                
                NSNotification *notification =[NSNotification notificationWithName:@"NotifiGetConversationList" object:nil userInfo:nil];
                
                // 3.通过 通知中心 发送 通知
                [[NSNotificationCenter defaultCenter] postNotification:notification];
                
              
            }
            
            PDRPluginResult *result = [PDRPluginResult resultWithStatus:PDRCommandStatusOK messageAsString:@"登录成功"];
            // 通知JS层Native层运行结果
            [self toCallback:cbId withReslut:[result toJSONString]];
            
            
            
            NSLog(@"Login Succ is %@",currentUser);
        } fail:^(int code, NSString * err) {
            [[IMPlugin sharedInstance] setAutoLogin:NO];
//             [self setAutoSendLogin:NO];
            NSLog(@"Login Failed: %d->%@", code, err);
        }];
    }else{
        
    }
    
  
    
    return 0;
}
//通知获取群组列表
-(void)NotifiGetGroupList:(NSNotification *)notify{
    
}
//通知获取会话列表
-(void)NotifiGetConversationList:(NSNotification *)notify{
     [self setAutoSendLogin:NO];
    
     [self getConversationList:_getConCallBackId Result:_getConDic];
    NSLog(@"%@",notify.userInfo);
    
    NSLog(@"---接收到通知---");
}
- (int)loginGetGroupList:(TIMLoginParam*)param cbId:(NSString *)cbId resultDic:(NSDictionary *)resultDic succ:(TIMLoginSucc)succ fail:(TIMFail)fail{
    TIMLoginParam * login_param = [[TIMLoginParam alloc ]init];
    
    // identifier为用户名，userSig 为用户登录凭证
    // appidAt3rd 在私有帐号情况下，填写与sdkAppId 一样
    login_param.identifier = param.identifier;
    login_param.userSig = param.userSig;
    login_param.appidAt3rd = kSdkAppId;
    
    NSString *currentUser =  [[TIMManager sharedInstance] getLoginUser];
    if (currentUser == nil && param.identifier == nil) {
        return 0;
    }else if(currentUser == nil){
        [[TIMManager sharedInstance] login: login_param succ:^(){
            [[IMPlugin sharedInstance] setAutoLogin:YES];
            
            NSString *currentUser =  [[TIMManager sharedInstance] getLoginUser];
            if (currentUser) {
                [[NSUserDefaults standardUserDefaults] setObject:param.identifier forKey:kIMPlugin_CurrentUserIdentifier_Key];
//                IMPlugin *IMPluginManager = [IMPlugin sharedInstance];
                [self getGroupList:cbId Result:resultDic];
                
            }
            
//                    PDRPluginResult *result = [PDRPluginResult resultWithStatus:PDRCommandStatusOK messageAsString:@"returnBackSucc"];
//                    // 通知JS层Native层运行结果
//                    [self toCallback:cbId withReslut:[result toJSONString]];
            [self setAutoSendLogin:NO];
            NSLog(@"Login Succ is %@",currentUser);
            
            
        } fail:^(int code, NSString * err) {
            [[IMPlugin sharedInstance] setAutoLogin:NO];
            NSLog(@"Login Failed: %d->%@", code, err);
        }];
    }else{
        NSString *currentUser =  [[TIMManager sharedInstance] getLoginUser];
        if (currentUser) {
            [[NSUserDefaults standardUserDefaults] setObject:param.identifier forKey:kIMPlugin_CurrentUserIdentifier_Key];
//            IMPlugin *IMPluginManager = [IMPlugin sharedInstance];
            [self getGroupList:cbId Result:resultDic];
            
        }
    }
    
   
    
    return 0;
}
- (int)loginGetConversationList:(TIMLoginParam*)param cbId:(NSString *)cbId resultDic:(NSDictionary *)resultDic succ:(TIMLoginSucc)succ fail:(TIMFail)fail{
    TIMLoginParam * login_param = [[TIMLoginParam alloc ]init];
    
   
    // identifier为用户名，userSig 为用户登录凭证
    // appidAt3rd 在私有帐号情况下，填写与sdkAppId 一样
    login_param.identifier = param.identifier;
    login_param.userSig = param.userSig;
    login_param.appidAt3rd = kSdkAppId;
    
    NSString *currentUser =  [[TIMManager sharedInstance] getLoginUser];
    if (currentUser == nil && param.identifier == nil) {
        return 0;
    }else if(currentUser == nil){
     BOOL isAutoSend = [self isAutoSendLogin];
        
        if (isAutoSend) {
            NSString *currentUser =  [[TIMManager sharedInstance] getLoginUser];
            if (currentUser) {
                [[NSUserDefaults standardUserDefaults] setObject:param.identifier forKey:kIMPlugin_CurrentUserIdentifier_Key];
                
                [self getConversationList:cbId Result:resultDic];
                
            }else{
                
                 [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(NotifiGetConversationList:) name:@"NotifiGetConversationList" object:nil];
                _getConDic = [[NSDictionary alloc] initWithDictionary:resultDic];
                _getConCallBackId = [[NSString alloc] initWithString:cbId] ;
                NSLog(@"_getConDic:%@",_getConDic);
                NSLog(@"_getConCallBackId:%@",_getConCallBackId);
                
                
            }
        }else{
            [self setAutoSendLogin:YES];
            [[TIMManager sharedInstance] login: login_param succ:^(){
                [[IMPlugin sharedInstance] setAutoLogin:YES];
                
                NSString *currentUser =  [[TIMManager sharedInstance] getLoginUser];
                if (currentUser) {
                    [[NSUserDefaults standardUserDefaults] setObject:param.identifier forKey:kIMPlugin_CurrentUserIdentifier_Key];
                     [[NSUserDefaults standardUserDefaults] setObject:param.userSig forKey:kIMPlugin_CurrentUserSig_Key];
                    [self setAutoSendLogin:NO];
                    [self getConversationList:cbId Result:resultDic];
                    
                }
                
                
                //                    PDRPluginResult *result = [PDRPluginResult resultWithStatus:PDRCommandStatusOK messageAsString:@"returnBackSucc"];
                //                    // 通知JS层Native层运行结果
                //                    [self toCallback:cbId withReslut:[result toJSONString]];
                NSLog(@"Login Succ is %@",currentUser);
                
                
            } fail:^(int code, NSString * err) {
                [[IMPlugin sharedInstance] setAutoLogin:NO];
//                [self setAutoSendLogin:NO];
                NSLog(@"Login Failed: %d->%@", code, err);
            }];
        }
      
    }else{
        NSString *currentUser =  [[TIMManager sharedInstance] getLoginUser];
        if (currentUser) {
            [[NSUserDefaults standardUserDefaults] setObject:param.identifier forKey:kIMPlugin_CurrentUserIdentifier_Key];
            
            [self getConversationList:cbId Result:resultDic];
            
        }
    }
    
    
    
    return 0;
}
// 设置是否发送登录指令  ，因为发送登录指令后50s内不能重复发送，否则报错
- (void)setAutoSendLogin:(BOOL)autoSendLogin{
    [[NSUserDefaults standardUserDefaults] setObject:@(autoSendLogin) forKey:kIMPlugin_AutoSendLogin_Key];
}
// 判断是否发送登录指令
- (BOOL)isAutoSendLogin{
    NSNumber *num = [[NSUserDefaults standardUserDefaults] objectForKey:kIMPlugin_AutoSendLogin_Key];
    return [num boolValue];
}
// 设置是否登录
- (void)setAutoLogin:(BOOL)autologin
{
    [[NSUserDefaults standardUserDefaults] setObject:@(autologin) forKey:kIMPlugin_AutoLogin_Key];
}
// 判断是否登录
- (BOOL)isAutoLogin
{
    NSNumber *num = [[NSUserDefaults standardUserDefaults] objectForKey:kIMPlugin_AutoLogin_Key];
    return [num boolValue];
}
// 设置是否获取群组列表
- (void)setGroupList:(BOOL)autoGet
{
    [[NSUserDefaults standardUserDefaults] setObject:@(autoGet) forKey:kIMPlugin_AutoGetGroupList_Key];
}
// 判断是否获取群组列表
- (BOOL)isGetGroupList
{
    NSNumber *num = [[NSUserDefaults standardUserDefaults] objectForKey:kIMPlugin_AutoGetGroupList_Key];
    return [num boolValue];
}
// 设置是否获取会话列表
- (void)setConversationList:(BOOL)autoGet
{
    [[NSUserDefaults standardUserDefaults] setObject:@(autoGet) forKey:kIMPlugin_AutoGetConversationList_Key];
}

// 判断是否获取会话列表
- (BOOL)isGetConversationList
{
    NSNumber *num = [[NSUserDefaults standardUserDefaults] objectForKey:kIMPlugin_AutoGetConversationList_Key];
    return [num boolValue];
}

//***********************     登出IMSDK     ***********************//
- (void)logout:(NSString *)callBackid Result:(NSDictionary *)resultDic succ:(TIMLoginSucc)succ fail:(TIMFail)fail
{
    
    NSString *identifier = resultDic[@"identifier"];
    if (identifier != NULL) {
        [[TIMManager sharedInstance] logout:^{
            [self setAutoLogin:NO];
            if (succ)
            {
             
                NSLog(@"logout succ");
                PDRPluginResult *result = [PDRPluginResult resultWithStatus:PDRCommandStatusOK messageAsArray:@[@"logout succ"]];
                // 通知JS层Native层运行结果
                [self toCallback:callBackid withReslut:[result toJSONString]];
                succ();
            }
        } fail:^(int code, NSString *err) {
            [self setAutoLogin:YES];
            if (fail)
            {
                NSLog(@"logout fail: code=%d err=%@", code, err);
                fail(code, err);
            }
        }];
    }
    
   
}

//***********************     获取群列表     ***********************//
-(void)getGroupList:(NSString *)callBackid Result:(NSDictionary *)resultDic{
    
    BOOL isAutoLogin = [self isAutoLogin];
    if(!isAutoLogin){
        NSLog(@"user not init ok");
        return;
    }
    BOOL isAutoGet = [self isGetGroupList];
    if (isAutoGet) {
        NSLog(@"GroupList is Get");
        return;
    }
    TIMGroupManager *groupManager = [TIMGroupManager sharedInstance];
    NSString *currentUser =  [[TIMManager sharedInstance] getLoginUser];
    NSLog(@"Login Succ2 is %@",currentUser);
    [self setGroupList:YES];
    int groupStatu =   [groupManager getGroupList:^(NSArray *list) {
//        NSLog(@"getGroupList1:%@",list);
        //获取群组列表数据后，设置为未获取群组列表，为了下次可以重新获取群组列表信息
            [self setGroupList:NO]; //
        [self parseGroupList:list callBackid:callBackid];
        
    } fail:^(int code, NSString* err) {
        NSLog(@"failed code: %d %@", code, err);
    }];
    if (groupStatu == 0) {
        //发送获取群组列表指令后，设置为已经发送获取群组指令状态
        [self setGroupList:YES];
    }else{
        //发送获取群组列表指令失败后，设置为未发送获取群组指令状态
        [self setGroupList:NO];
    }
    
}
//***********************     获取会话对讲机列表     ***********************//
-(void)getConversationList:(NSString *)callBackid Result:(NSDictionary *)resultDic{
    
    BOOL isAutoLogin = [self isAutoLogin];
    if(!isAutoLogin){
        NSLog(@"user not init ok");
        return;
    }
    BOOL isAutoGet = [self isGetConversationList];
    if (isAutoGet) {
        NSLog(@"ConversationList is Get");
        return;
    }
    TIMManager *currentManager =[TIMManager sharedInstance];
    NSString *currentUser =  [currentManager getLoginUser];
    NSLog(@"Login Succ2 is %@",currentUser);
   
    NSArray *conversationList  = [currentManager getConversationList];
    [self setConversationList:YES];
    [self parseConversationList:conversationList Result:^(id response, NSError *error) {
       
        NSArray *resultArr = (NSArray *)response;
        NSLog(@"parseConversationList is %@",resultArr);
         //发送获取群组列表指令失败后，设置为未发送获取群组指令状态
         [self setConversationList:NO];
//        if (resultArr.count > 0) {
//            //发送获取群组列表指令后，设置为已经发送获取群组指令状态
//            [self setConversationList:YES];
//        }else{
//            //发送获取群组列表指令失败后，设置为未发送获取群组指令状态
//            [self setConversationList:NO];
//        }
        PDRPluginResult *result = [PDRPluginResult resultWithStatus:PDRCommandStatusOK messageAsArray:(NSArray *)response];
        // 通知JS层Native层运行结果
        if (_getConCallBackId) {
            
        }else{
            
        }
//        NSLog(@"one:%@",callBackid);
        NSLog(@"_getConDic:%@",_getConDic);
        NSLog(@"_getConCallBackId:%@",_getConCallBackId);
        
        [self toCallback:callBackid withReslut:[result toJSONString]];
        
        
        
    }];
    
   
    
}
//解析会话列表
-(void)parseConversationList:(NSArray *)ConversationList Result:(Result)callback{
    NSMutableArray *filterConversationList = [NSMutableArray array];
    //把单聊会话都放进同一个数组
    for (TIMConversation *conversation in ConversationList) {
        if (conversation.getType == TIM_C2C) {
            [filterConversationList addObject:conversation];
        }
    }
    NSMutableArray *newArr =[NSMutableArray array];
    for (NSInteger i = 0; i < filterConversationList.count; i++) {
        TIMConversation *filterConversation = filterConversationList[i];
        NSDictionary *filterConversationDic = [NSMutableDictionary  dictionary];
        NSString *getReceiver  = filterConversation.getReceiver;
        [filterConversationDic setValue:getReceiver forKey:@"identifier"];
        [filterConversationDic setValue:C2C forKey:@"type"];
        //获取未读
        int unReadNum = [filterConversation getUnReadMessageNum];
        [filterConversationDic setValue:[NSString stringWithFormat:@"%d",unReadNum] forKey:@"unread"];
        [newArr addObject:filterConversationDic];
        
        
    }
    
    
    
    callback(newArr,nil);
    
}
//解析群组列表
-(void)parseGroupList:(NSArray *)GroupList callBackid:(NSString *)callBackid{
    NSMutableArray *indexList = [NSMutableArray array];
    NSMutableArray *groupListArray = [NSMutableArray array];
    
    for (NSInteger i = 0; i < GroupList.count ; i++) {
        TIMGroupInfo *groupInfo = GroupList[i];
        NSDictionary *groupDic = [NSMutableDictionary dictionary];
        [groupDic setValue:groupInfo.group forKey:@"identifier"];
        [groupDic setValue:groupInfo.groupName forKey:@"name"];
        [groupDic setValue:@"group" forKey:@"type"];
        //根据群列表获取群会话
        TIMConversation * group_conversation = [[TIMManager sharedInstance] getConversation:TIM_GROUP receiver:groupInfo.group];
        NSLog(@"%@",group_conversation.getReceiver);
        //获取会话未读消息计数
        NSInteger UnReadMessageNum = [group_conversation getUnReadMessageNum];
        [groupDic setValue:[NSString stringWithFormat:@"%ld",UnReadMessageNum] forKey:@"unread"];
        //获取时间戳
        uint32_t groupJoinTime = groupInfo.selfInfo.joinTime;
        [groupDic setValue:[NSString stringWithFormat:@"%u",groupJoinTime]  forKey:@"timestamp"];
        //获取最后一条信息
//         [groupDic setValue:NULL  forKey:@"lastmsg"];
        
        [groupListArray addObject:groupDic];
        //指定上次获取的最后一条消息，如果last传nil，从最新的消息开始读取 size 为消息数
        [self getMessage:group_conversation groupDic:groupDic timMessage:nil size:1 callBackid:callBackid callback:^(id response, NSError *error) {
            NSDictionary *responseDic = (NSDictionary *)response;
            NSLog(@"%@",responseDic);
             NSDictionary *tempConversationJson = groupListArray[i];
            if ([responseDic count]== 0) {
               NSLog(@"%@",responseDic);
            }else{
                 [tempConversationJson setValue:responseDic  forKey:@"lastmsg"];
            }
           
            [indexList addObject:[NSString stringWithFormat:@"1"]];
            
            if (indexList.count == groupListArray.count) {
                // 运行Native代码结果和预期相同，调用回调通知JS层运行成功并返回结果
                            PDRPluginResult *result = [PDRPluginResult resultWithStatus:PDRCommandStatusOK messageAsArray:groupListArray];
                //            // 通知JS层Native层运行结果
                            [self toCallback:callBackid withReslut:[result toJSONString]];
            }
 
            
        }];
        
        
        
    }
//    for (TIMGroupInfo * info in GroupList) {
//        NSLog(@"group=%@ type=%@ name=%@", info.group, info.groupType, info.groupName);
//    }
}
//获取会话扩展实例
-(TIMConversation *)getConversation:(NSDictionary *)resultDic{
    
    NSString *type = resultDic[@"type"];
    NSString *identifier = resultDic[@"identifier"];
    NSLog(@"identifier==%@ ;type==%@",identifier,type);
    TIMConversationType timConversationType = TIM_C2C;
    if ([type isEqualToString:GROUP]) {
        timConversationType = TIM_GROUP;
    }
    TIMConversation *con = [[TIMManager sharedInstance] getConversation:timConversationType receiver:identifier];
    return con;
}
//***********************     创建群组     ***********************//
-(void)creatGroup:(NSString *)callBackid Result:(NSDictionary *)resultDic{
    NSString *name = resultDic[@"name"];
    NSArray *members = resultDic[@"members"];
    //创建公开群，且不自定义群ID
    // 创建群组信息
    TIMCreateGroupInfo *groupInfo = [[TIMCreateGroupInfo alloc] init];
    groupInfo.group = nil;
    groupInfo.groupName = name;
    groupInfo.groupType = @"Private";
    groupInfo.addOpt = TIM_GROUP_ADD_FORBID;  //需要管理员同意入群
//    groupInfo.maxMemberNum = 3;  //最大群成员数量
    groupInfo.notification = @"hello world";
    groupInfo.introduction = @"welcome to our group";
    groupInfo.faceURL = nil;
    
    //编辑群成员信息
    NSMutableArray *membersInfo = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < members.count; i++) {
        NSDictionary *memberDic = members[i];
        // 创建群成员信息
        TIMCreateGroupMemberInfo *memberInfo = [[TIMCreateGroupMemberInfo alloc] init];
        memberInfo.member = [NSString stringWithFormat:@"%@",memberDic[@"username"]];
        memberInfo.role = TIM_GROUP_MEMBER_ROLE_MEMBER;
        NSLog(@"memberInfo.member:%@",memberInfo.member);
        // 添加群成员信息
        [membersInfo addObject:memberInfo];
        
    }
    groupInfo.membersInfo = membersInfo;
    NSLog(@"membersInfo:%@",membersInfo);
    // 创建指定属性群组
    [[TIMGroupManager sharedInstance] createGroup:groupInfo succ:^(NSString * group) {
        NSLog(@"create group succ, sid=%@", group);
        PDRPluginResult *result = [PDRPluginResult resultWithStatus:PDRCommandStatusOK messageAsString:[NSString stringWithFormat:@"%@",group]];
        // 通知JS层Native层运行结果
        [self toCallback:callBackid withReslut:[result toJSONString]];
        
    } fail:^(int code, NSString* err) {
        NSLog(@"failed code: %d %@", code, err);
    }];
    
}
//***********************     发送语音文件消息     ***********************//
-(void)sendMessage:(NSString *)callBackid Result:(NSDictionary *)resultDic{
    //构造一条消息
    TIMMessage *msg = [[TIMMessage alloc] init];
    
    //添加语音
    
    TIMElem *elem = [[TIMElem alloc] init];
    NSString *msgType  = resultDic[@"msgType"];
    
    NSLog(@"addMessage msgType=%@",msgType);
    
    if ([msgType isEqualToString:@"voice"]) {
        TIMSoundElem *soundElem = [[TIMSoundElem alloc] init];
    
        NSString *filePath = [NSString stringWithFormat:@"%@",resultDic[@"filePath"]];
        [soundElem setPath:filePath];
       
        
        NSNumber *duration = resultDic[@"duration"];
        
        [soundElem setSecond:[duration intValue]];
        elem = soundElem;
        NSLog(@"voice filePath==%@;duration== %@,result== %@",filePath,duration,resultDic);
    }else if([msgType isEqualToString:@"text"]){
        TIMTextElem *textElem = [[TIMTextElem alloc] init];
        NSString *content = [NSString stringWithFormat:@"%@",resultDic[@"content"]];
        NSLog(@"Text==%@",content);
//        textElem.text = content;
        [textElem setText:content];
        elem = textElem;
   
        
    }
    else if([msgType isEqualToString:@"image"]){
        TIMImageElem *imageElem = [[TIMImageElem alloc] init];
        NSString *filePath = [NSString stringWithFormat:@"%@",resultDic[@"filePath"]];
        //在filePath中找到file://
        NSRange range = [filePath rangeOfString:@"file://" options:NSCaseInsensitiveSearch];
        if (range.location == NSNotFound) {
            
        }else{
            //剪切
            [filePath substringWithRange:range];
            
        }
        
        [imageElem setPath:filePath];
        elem = imageElem;
        
        NSLog(@"filePath==%@",filePath);
    }
    
    
    //将elem添加到消息
    if([msg addElem:elem] != 0) {
       NSLog(@"addElement failed");
        return;
    }
    //获取会话
    TIMConversation *conversation = [self getConversation:resultDic];
    //发送消息
    [conversation sendMessage:msg succ:^{
        
        NSDictionary *msgDic = [NSMutableDictionary dictionary];
        msgDic = [self parseMsg:msg];
        NSLog(@"msgDic ===%@",msgDic);
        
        PDRPluginResult *result = [PDRPluginResult resultWithStatus:PDRCommandStatusOK messageAsDictionary:msgDic];
        // 通知JS层Native层运行结果
        [self toCallback:callBackid withReslut:[result toJSONString]];
        
    } fail:^(int code, NSString *msg) {
        
    }];
    
}
//***********************     获取本地消息列表     ***********************//
-(void)getLocalMessageList:(NSString *)callBackid Result:(NSDictionary *)resultDic{
   
    TIMConversation *conversation = [self getConversation:resultDic];
    NSNumber *size = resultDic[@"size"];
    NSLog(@"%@",size);
    [self getLocalMessage:conversation timMessage:nil size:[size intValue] callBackid:callBackid];
    
        
   
   
}
//删除消息
-(BOOL )deleteMessage:(NSDictionary *)resultDic{
    TIMMessage *msg = [self findCursorMessage:resultDic];
    
    if (msg !=NULL) {
        BOOL isremove = [msg remove];
       return isremove;
    }
    return false;
}
//在本地消息列表中查询指定消息
-(TIMMessage *)findCursorMessage:(NSDictionary *)resultDic{
    NSLog(@"%@",resultDic);
    NSString *msgId = resultDic[@"msgId"];
    id getMsgUniqueId = resultDic[@"uniqueId"];
    long getSeq = resultDic[@"seq"];
    long timestamp = resultDic[@"timestamp"];
    long rand = resultDic[@"rand"];
    Boolean isSelf = resultDic[@"self"];
   
    
    
    if(msgId == NULL){
        return NULL;
    }
    
    
    
//    NSArray *messageListArr = [self getDataWriteToFile:@"arr" model:@"message" path:@"messageList"];
    for (NSInteger i = 0; i < _messageList.count; i++) {
        TIMMessage *timMessage = _messageList[i];
        //seq, rand, timestamp, isSelf
         NSLog(@"find message uniqueId==%@",timMessage.msgId);
        if ([timMessage.msgId isEqualToString:msgId]) {
            NSLog(@"find message from cache==%@",timMessage);
            return timMessage;
        }
//        if ([getMsgUniqueId isEqualToString:[NSString stringWithFormat:@"%llu",timMessage.uniqueId]]) {
//            NSLog(@"find message from cache==%@",timMessage);
//            return timMessage;
//        }
       
    }
//    Log.i(tag,"messageList=="+messageList.size());
    NSLog(@"messageList==%lu",_messageList.count);
    return  NULL;
   
}
//往本地消息列表中添加消息
-(void)addMessageModelList:(NSDictionary *)message{
    
    NSArray *messageListArr = [self getDataWriteToFile:@"arr" model:@"message" path:@"messageList"];
    
    if (messageListArr == nil) {
        _messageModelList = [[NSMutableArray alloc] init];
        
    }else{
        //解档 转变为数组类型
       
        _messageModelList = [NSMutableArray arrayWithArray:messageListArr];
    }
    for (NSInteger i = 0; i < _messageModelList.count; i++) {
        NSDictionary *timMessageModel = _messageModelList[i];
        //seq, rand, timestamp, isSelf
        NSLog(@"message:%@",message);
        if ([timMessageModel[@"msgId"] isEqualToString:message[@"msgId"]]) {
            NSLog(@"update message from cache==%@",timMessageModel);
           
            [_messageModelList replaceObjectAtIndex:i withObject:message];
            [self saveDataWriteToFile:@"arr" model:@"message" path:@"messageList" dataObject:_messageModelList];
            [_messageModelList removeAllObjects];
            return ;
        }
        
    }
    
  
   
    [_messageModelList addObject:message];
    

    
    //存入本地
    
    [self saveDataWriteToFile:@"arr" model:@"message" path:@"messageList" dataObject:_messageModelList];
    [_messageModelList removeAllObjects];
}
-(void)addCursorMessage:(TIMMessage *)message{
   
    
    if (_messageList == nil) {
        _messageList = [[NSMutableArray alloc] init];
        
    }else{
        //解档 转变为数组类型
        
//        _messageModelList = [NSMutableArray arrayWithArray:messageListArr];
    }
    for (NSInteger i = 0; i < _messageList.count; i++) {
        TIMMessage *timMessageModel = _messageList[i];
        //seq, rand, timestamp, isSelf
        NSLog(@"message:%@",message);
        if (timMessageModel.msgId == message.msgId) {
            NSLog(@"update message from cache==%@",timMessageModel);
            
            [_messageList replaceObjectAtIndex:i withObject:message];
//            [self saveDataWriteToFile:@"arr" model:@"message" path:@"messageList" dataObject:_messageList];
//            [_messageList removeAllObjects];
            return ;
        }
        
    }
    
    
    
    [_messageList addObject:message];
    
    NSLog(@"_messageModelList:%@",_messageList);
    
    //存入本地
    
//    [self saveDataWriteToFile:@"arr" model:@"message" path:@"messageList" dataObject:_messageList];
//    [_messageList removeAllObjects];
}


//获取会话本地消息列表
-(void)getLocalMessage:(TIMConversation *)Conversation timMessage:(TIMMessage *)timMessage size:(int)size callBackid:(NSString *)callBackid{
    TIMConversation *conversation =[[TIMConversation alloc] init];
    conversation = Conversation ;
    
    NSLog(@"%@",conversation.getReceiver);
    int getMessageStatu = [Conversation getMessage:size last:timMessage succ:^(NSArray * msgList) {
        //        NSArray *responseArr = (NSArray *)response;
        
        NSArray *resultArr = [NSMutableArray array];
        resultArr = [self parseMsgList:msgList];
        
        NSLog(@"%@",resultArr);
        
        
        
        // 运行Native代码结果和预期相同，调用回调通知JS层运行成功并返回结果
        PDRPluginResult *result = [PDRPluginResult resultWithStatus:PDRCommandStatusOK messageAsArray:resultArr];
                    // 通知JS层Native层运行结果
        
        [self toCallback:callBackid withReslut:[result toJSONString]];
     
    }fail:^(int code, NSString * err) {
        NSLog(@"Get Message Failed:%d->%@", code, err);
    }];
}
//获取会话最新消息
-(void)getMessage:(TIMConversation *)Conversation groupDic:(NSDictionary *)groupDic timMessage:(TIMMessage *)timMessage size:(int)size callBackid:(NSString *)callBackid callback:(Result)callback{
    TIMConversation *conversation =[[TIMConversation alloc] init];
    conversation = Conversation ;
    NSLog(@"%@",conversation.getReceiver);
    int getMessageStatu = [Conversation getMessage:size last:timMessage succ:^(NSArray * msgList) {
//        NSArray *responseArr = (NSArray *)response;
        
        NSArray *resultArr = [NSMutableArray array];
        resultArr = [self parseMsgList:msgList];
        NSDictionary *returnNoneDic = [NSMutableDictionary dictionary];
        NSLog(@"%@",resultArr);
        if (resultArr != NULL && resultArr.count ==0) {
           callback(returnNoneDic,nil);
        }else{
            //获取最后一条信息
            TIMMessage *msg = msgList[0];
            NSString *conversationId = [msg getConversation].getReceiver;
            for (NSInteger j = 0; j < resultArr.count; j++) {
                
                NSString *tempConversationId = groupDic[@"identifier"];
                if ([conversationId isEqualToString:tempConversationId]) {
                    
                    
                    callback(resultArr[0],nil);
                    
                    
                }
            }
        }



    }fail:^(int code, NSString * err) {
        NSLog(@"Get Message Failed:%d->%@", code, err);
    }];
}
//解析会话消息列表
-(NSArray *)parseMsgList:(NSArray *)MsgList{
    NSMutableArray *returnMsgListArr = [NSMutableArray array];
            for (TIMMessage * msg in MsgList) {
                if ([msg isKindOfClass:[TIMMessage class]]) {
                    NSLog(@"GetOneMessage:%@", msg);
                    //  lastMsg = msg;
                    //可以通过timestamp()获得消息的时间戳, isSelf()是否为自己发送的消息
                    NSDictionary *msgDic = [self parseMsg:msg];
                    [returnMsgListArr addObject:msgDic];
                  
                }
            }
    
    return returnMsgListArr;
}
//解析消息转变为jsonDic 格式

-(NSDictionary *)parseMsg:(TIMMessage *)msg{
    
    NSDictionary *msgDicJson = [NSMutableDictionary dictionary];
     TIMElem *elem = [[TIMElem alloc] init];
    if ([msg isKindOfClass:[TIMMessage class]]) {
       
        
        elem = [msg getElem:0];
        
    }
   
    if ([elem isKindOfClass:[TIMTextElem class]]) {
        TIMTextElem * text_elem = [[TIMTextElem alloc] init] ;
        text_elem = (TIMTextElem *)elem ;
        [msgDicJson setValue:@"text" forKey:@"type"];
        [msgDicJson setValue:text_elem.text forKey:@"content"];
        
    }
    else if ([elem isKindOfClass:[TIMImageElem class]]) {
        TIMImageElem * image_elem = [[TIMImageElem alloc] init];;
        image_elem = (TIMImageElem *)elem ;
        [msgDicJson setValue:@"image" forKey:@"type"];
        [msgDicJson setValue:image_elem.path forKey:@"path"];
        [msgDicJson setValue:[NSString stringWithFormat:@"file://%@",image_elem.path] forKey:@"content"];
    }
    else if ([elem isKindOfClass:[TIMSoundElem class]]) {
        TIMSoundElem * voice_elem = [[TIMSoundElem alloc] init];
        voice_elem = (TIMSoundElem *)elem ;
        [msgDicJson setValue:@"sound" forKey:@"type"];
        if ([voice_elem.path isEqualToString:@""]) {
            [msgDicJson setValue:[self getDataWithMsgId:msg.msgId type:@"sound"] forKey:@"path"];
        }else{
            
            [msgDicJson setValue:voice_elem.path  forKey:@"path"];
        }
        
        //语音时长
        
        [msgDicJson setValue:[NSNumber numberWithInt:voice_elem.second]  forKey:@"duration"];
        
        
    }
    else if ([elem isKindOfClass:[TIMGroupTipsElem class]]) {
        TIMGroupTipsElem * groupTips_elem = [[TIMGroupTipsElem alloc] init];
        groupTips_elem = (TIMGroupTipsElem *)elem ;
        [msgDicJson setValue:@"tips" forKey:@"type"];
        NSString *groupTipsStr = [self parseGroupTips:msgDicJson TipsMsg:msg];
        
        [msgDicJson setValue:groupTipsStr forKey:@"content"];
    }

    
    [msgDicJson setValue:msg.msgId forKey:@"msgId"];
    [msgDicJson setValue:[NSString stringWithFormat:@"%llu",msg.uniqueId] forKey:@"uniqueId"];
    [msgDicJson setValue:[NSString stringWithFormat:@"%@",msg.timestamp] forKey:@"timestamp"];
    
    [msgDicJson setValue:[NSString stringWithFormat:@"%llu",msg.locator.rand] forKey:@"rand"];
    
     [msgDicJson setValue:[NSString stringWithFormat:@"%d",[msg isSelf]] forKey:@"self"];
    
     [msgDicJson setValue:[NSString stringWithFormat:@"%llu",msg.locator.seq] forKey:@"seq"];
    [msgDicJson setValue:[NSString stringWithFormat:@"%@",msg.sender] forKey:@"sender"];
    
    
    [msgDicJson setValue:[NSString stringWithFormat:@"%@",[msg getConversation].getReceiver ] forKey:@"peer"];
    //判断消息状态
    TIMMessageStatus status = msg.status;
    NSString *statuStr = [NSString string];
    
    
    if (status == TIM_MSG_STATUS_SENDING) {
        //消息发送中
        statuStr = @"sending";
    }else if (status == TIM_MSG_STATUS_SEND_SUCC){
         //消息发送成功
        statuStr = @"sendSucc";
    }
    else if (status == TIM_MSG_STATUS_SEND_FAIL){
         //消息发送失败
         statuStr = @"sendFail";
        
    }
    [msgDicJson setValue:statuStr forKey:@"status"];
    
    //判断消息会话类型
    NSString *conversationType = [NSString stringWithFormat:@"%@",C2C];
    TIMConversation *conversation  =[msg getConversation];
    if (conversation.getType == TIM_GROUP) {
        conversationType = GROUP;
    }
    [msgDicJson setValue:conversationType forKey:@"conversationType"];
    
    [self addCursorMessage:msg];
//    [self addMessageModelList:msgDicJson];
    
    
    
    return msgDicJson;
}
-(NSString *)getDataWithMsgId:(NSString *)msgId type:(NSString *)type{
   
    
    if (_messageList == nil) {
        
        
    }else{
        //解档 转变为数组类型
        
        for (NSInteger i = 0; i < _messageList.count; i++) {
            TIMMessage *timMessage = _messageList[i];
            //seq, rand, timestamp, isSelf
            NSLog(@"messageID:%@",msgId);
            NSLog(@"timMessageModelID:%@",timMessage.msgId);
            if ([timMessage.msgId isEqualToString:msgId]) {
                NSLog(@"update message from cache==%@",timMessage);
                if ([type isEqualToString:@"sound"]) {
                    TIMElem *elem1 = [[TIMElem alloc] init];
                    elem1 = [timMessage getElem:0];
                    if ([elem1 isKindOfClass:[TIMSoundElem class]]) {
                        TIMSoundElem *soundElem = (TIMSoundElem *)elem1;
                        NSLog(@"soundElemPath:%@",soundElem.path);
                        return soundElem.path;
                    }
                    return @"";
                    
                }else if([type isEqualToString:@"text"]){
                    TIMElem *elem2 = [[TIMElem alloc] init];
                    elem2 = [timMessage getElem:0];
                    if ([elem2 isKindOfClass:[TIMTextElem class]]) {
                        TIMTextElem *textElem = (TIMTextElem *)elem2;
                        NSLog(@"textElemText:%@",textElem.text);
                        return textElem.text;
                    }
                    return @"";
                }else if([type isEqualToString:@"image"]){
                    TIMElem *elem3 = [[TIMElem alloc] init];
                    elem3 = [timMessage getElem:0];
                    if ([elem3 isKindOfClass:[TIMImageElem class]]) {
                        TIMImageElem *imageElem = (TIMImageElem *)elem3;
                        NSLog(@"imageElemPath:%@",imageElem.path);
                        return imageElem.path;
                    }
                    return @"";
                }
                
                
                
                
            }
            
        }
    }
    
    //    NSDictionary *timMessage = [self findCursorMessage:resultDic];
    //    NSLog(@"find timMessage%@ ,get message count:%@,get resultDic%@",timMessage[@"msgId"],size,resultDic);
    return @"";
}
//-(NSString *)getDataWithMsgId:(NSString *)msgId type:(NSString *)type{
//    NSArray *messageListArr = [self getDataWriteToFile:@"arr" model:@"message" path:@"messageList"];
//
//    if (messageListArr == nil) {
//
//
//    }else{
//        //解档 转变为数组类型
//
//        for (NSInteger i = 0; i < messageListArr.count; i++) {
//            NSDictionary *timMessageModel = messageListArr[i];
//            //seq, rand, timestamp, isSelf
//            NSLog(@"messageID:%@",msgId);
//            NSLog(@"timMessageModelID:%@",timMessageModel[@"msgId"]);
//            if ([timMessageModel[@"msgId"] isEqualToString:msgId]) {
//                NSLog(@"update message from cache==%@",timMessageModel);
//                if ([type isEqualToString:@"sound"]) {
//                     return timMessageModel[@"path"];
//                }else if([type isEqualToString:@"text"]){
//                    return timMessageModel[@"path"];
//                }else if([type isEqualToString:@"image"]){
//                    return timMessageModel[@"path"];
//                }
//
//
//
//
//            }
//
//        }
//    }
//
////    NSDictionary *timMessage = [self findCursorMessage:resultDic];
////    NSLog(@"find timMessage%@ ,get message count:%@,get resultDic%@",timMessage[@"msgId"],size,resultDic);
//    return @"";
//}
//解析群系统消息 ，返回显示消息
-(NSString *)parseGroupTips:(NSDictionary *)groupDic TipsMsg:(TIMMessage *)TipsMsg{
    NSString *returnStr = [NSString string];
    TIMGroupTipsElem *groupTipsElem = (TIMGroupTipsElem *)[TipsMsg getElem:0];
    NSDictionary *changedGroupMemberInfoDic = groupTipsElem.changedGroupMemberInfo;
    switch (groupTipsElem.type) {
            //取消管理员
        case TIM_GROUP_TIPS_TYPE_CANCEL_ADMIN:
            //管理员变更设置
        case TIM_GROUP_TIPS_TYPE_SET_ADMIN:
            return @"管理员变更";
            break;
        case TIM_GROUP_TIPS_TYPE_INVITE:
            NSLog(@"%@加入群",changedGroupMemberInfoDic);
            //////////处理字典//////////
            //遍历
            NSString *returnName = [NSString string];
            NSEnumerator *dictEnumerator = [changedGroupMemberInfoDic keyEnumerator];
                NSString *key;
                while ((key = [dictEnumerator nextObject]) != nil) {
                       TIMGroupMemberInfo *obj = changedGroupMemberInfoDic[key];
                    returnName = [self getName:obj];
                    }
            return [NSString stringWithFormat:@"[%@]加入群",returnName];
            
            
            break;
        case TIM_GROUP_TIPS_TYPE_KICKED:
            NSLog(@"[%@]被踢出群",changedGroupMemberInfoDic);
            return [NSString stringWithFormat:@"[%@]被踢出群",groupTipsElem.userList[0]];
            break;
        case TIM_GROUP_TIPS_TYPE_MEMBER_INFO_CHANGE:
            NSLog(@"%@加入群",changedGroupMemberInfoDic);
            //////////处理字典//////////
            //遍历
            NSString *returnName1 = [NSString string];
            NSEnumerator *dictEnumerator1 = [changedGroupMemberInfoDic keyEnumerator];
            NSString *keys = nil;
            while ((key = [dictEnumerator1 nextObject]) != nil) {
                TIMGroupMemberInfo *obj = changedGroupMemberInfoDic[keys];
                returnName1 = [self getName:obj];
            }
            return [NSString stringWithFormat:@"[%@]资料变更",returnName1];
            
            
            break;
        case TIM_GROUP_TIPS_TYPE_QUIT_GRP:
            NSLog(@"[%@]退出群",changedGroupMemberInfoDic[@"groupTipsElem"]);
            return [NSString stringWithFormat:@"[%@]退出群",groupTipsElem.opUser];
            break;
        case TIM_GROUP_TIPS_TYPE_INFO_CHANGE:
            NSLog(@"%@群资料变更",changedGroupMemberInfoDic);
            return @"群资料变更";
            break;
            
        default:
            break;
    }
    
    return returnStr;
}
//***********************     录音并获取语音文件     ***********************//
-(void)getVoiceFile:(NSString *)callBackid Result:(NSDictionary *)resultDic{
    
    TIMMessage *timMessage = [self findCursorMessage:resultDic];
    if (timMessage == NULL) {
        NSLog(@"not find the message info==%@",resultDic);
        return;
    }
    NSLog(@"%@",resultDic);
    
//    [1]    __NSCFString *    @"{\"action\":\"cmd_sound_file\",\"msgId\":\"3997229441\",\"uniqueId\":\"6530830741450141130\",\"timestamp\":\"2018-03-09 06:38:45 +0000\",\"rand\":\"542518730\",\"seq\":\"8278\",\"self\":\"1\"}"    0x000000015ee117c0
   
    NSString *returnMsgId = resultDic[@"msgId"];
    NSString *soundPath = [NSString string];
    soundPath = [self getDataWithMsgId:returnMsgId type:@"sound"];
    NSLog(@"soundFilePath:%@",soundPath);

    NSString *returnStr = [NSString string];
    
    if ([soundPath isEqualToString:@""]) {
         returnStr = @"getSound Fail";
        NSLog(@"not find the message info==%@",resultDic);
//        PDRPluginResult *result = [PDRPluginResult resultWithStatus:PDRCommandStatusOK messageAsString:returnStr];
//        // 通知JS层Native层运行结果
//        [self toCallback:callBackid withReslut:[result toJSONString]];
        return;
    }else{
       
//            TIMMessage *timMessage = [[TIMMessage alloc] init];
            TIMSoundElem *elem = (TIMSoundElem*)[timMessage getElem:0];
//        NSString *soundFilePath =  [self getTempFile:AUDIO];
        [elem getSound:soundPath succ:^{
            PDRPluginResult *result = [PDRPluginResult resultWithStatus:PDRCommandStatusOK messageAsString:soundPath];
            // 通知JS层Native层运行结果
            [self toCallback:callBackid withReslut:[result toJSONString]];

        } fail:^(int code, NSString *msg) {

        }];
      
    }
   
//    NSString *soundFilePath =  [self getTempFile:AUDIO];
//    [elem getSound:soundFilePath succ:^{
//

//    } fail:^(int code, NSString *msg) {
//
//    }];
    
}
-(NSString *)getName:(TIMGroupMemberInfo *)info{
    if ([info.nameCard isEqualToString:@""]) {
        return info.member;
    }
    return info.nameCard;
}
//创建临时文件夹
-(NSString *)getTempFile:(FileType)fileType{
    //    //Home目录
    //    NSString *homeDirectory = NSHomeDirectory();
    //
    //    //Document目录
    //    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //    NSString *path = [paths objectAtIndex:0];
    //
    //    //Cache目录
    //    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    //    NSString *path = [paths objectAtIndex:0];
    //
    //    //PreferencePanes目录
    //    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSPreferencePanesDirectory, NSUserDomainMask, YES);
    //    NSString *path = [paths objectAtIndex:0];
    //
    //    //Libaray目录NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    //    NSString *path = [paths objectAtIndex:0];
    
    //tmp目录
    NSString *tmpDir = NSTemporaryDirectory();
    NSString *tmpPath = [NSString string];
    if (fileType == VIDEO) {
        
    }else if (fileType == IMG){
        
    }else if (fileType == AUDIO){
        tmpPath = [NSString stringWithFormat:@"%@/audio",tmpDir];
    }else if (fileType == FILES){
        
    }
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:tmpPath]) {
        //文件夹已存在
        return tmpPath;
    } else {
        //创建文件夹
        [[NSFileManager defaultManager] createDirectoryAtPath:tmpPath withIntermediateDirectories:YES attributes:nil error:nil];
        return tmpPath;
    }
}
//***********************     退出群组     ***********************//
-(void)quitGroup:(NSString *)callBackid Result:(NSDictionary *)resultDic{
    //退出群组
    NSString *groupId = resultDic[@"identifier"];
    [[TIMGroupManager sharedInstance] quitGroup:groupId succ:^{
       
        PDRPluginResult *result = [PDRPluginResult resultWithStatus:PDRCommandStatusOK messageAsString:@"OK"];
        // 通知JS层Native层运行结果
        [self toCallback:callBackid withReslut:[result toJSONString]];
    } fail:^(int code, NSString *msg) {
        
    }];
}
//***********************     删除群成员     ***********************//
-(void)deleteGroupMember:(NSString *)callBackid Result:(NSDictionary *)resultDic{
    //创建待踢出群组的用户列表
    NSMutableArray *deleteMemberArr = [NSMutableArray array];
    NSArray *userArr = [NSMutableArray array];
    userArr  =resultDic[@"members"];
    for (NSInteger i = 0; i < userArr.count; i++) {
        [deleteMemberArr addObject:userArr[i][@"username"]];
    }
    
    NSString *groupId = resultDic[@"identifier"];
    TIMGroupManager *groupManager = [TIMGroupManager sharedInstance];
    [groupManager deleteGroupMemberWithReason:groupId reason:@"delete members" members:deleteMemberArr succ:^(NSArray *members) {
        for (TIMGroupMemberResult *result in members) {
            NSLog(@"result:%ld , user:%@",(long)result.status,result.member);
        }
        NSMutableArray *resultArr = [NSMutableArray array];
        PDRPluginResult *result = [PDRPluginResult resultWithStatus:PDRCommandStatusOK messageAsString:@"delete members Succ"];
        // 通知JS层Native层运行结果
        [self toCallback:callBackid withReslut:[result toJSONString]];
    } fail:^(int code, NSString *msg) {
        
    }];
    
}
//***********************     添加群成员     ***********************//
-(void)addGroupMember:(NSString *)callBackid Result:(NSDictionary *)resultDic{
    //创建待加入群组的用户列表
    NSMutableArray *addMembersArr = [NSMutableArray array];
    NSArray *usersArr = resultDic[@"members"];
    for (NSInteger i = 0; i < usersArr.count; i++) {
        [addMembersArr addObject:usersArr[i][@"username"]];
    }
    NSString *groupId = resultDic[@"identifier"];
    TIMGroupManager *groupManager = [TIMGroupManager sharedInstance];
    [groupManager inviteGroupMember:groupId members:addMembersArr succ:^(NSArray *members) {
        for (TIMGroupMemberResult *result in members) {
            NSLog(@"result:%ld , user:%@",(long)result.status,result.member);
        }
        NSMutableArray *resultArr = [NSMutableArray array];
        PDRPluginResult *result = [PDRPluginResult resultWithStatus:PDRCommandStatusOK messageAsString:@"add members Succ"];
        // 通知JS层Native层运行结果
        [self toCallback:callBackid withReslut:[result toJSONString]];
    } fail:^(int code, NSString *msg) {
        
    }];
}
//***********************     获取群成员信息     ***********************//
-(void)getGroupMember:(NSString *)callBackid Result:(NSDictionary *)resultDic{
    
    NSString *groupId = resultDic[@"identifier"];
    TIMGroupManager *groupManager = [TIMGroupManager sharedInstance];
    [groupManager getGroupMembers:groupId succ:^(NSArray *members) {
         NSMutableArray *resultArr = [NSMutableArray array];
        for (TIMGroupMemberInfo *memberInfo in members) {
            NSLog(@"user:%@ , join time:%ld , role:%ld",memberInfo.member,memberInfo.joinTime,memberInfo.role);
            
            NSMutableDictionary *memberDic = [NSMutableDictionary dictionary];
            [memberDic setValue:memberInfo.member forKey:@"username"];
            
            NSString *roleName = [NSString string];
            
            if (memberInfo.role ==TIM_GROUP_MEMBER_UNDEFINED ) {
                //未定义，没有获取该字段
                roleName = @"NotMember";
            } else if(memberInfo.role ==TIM_GROUP_MEMBER_ROLE_MEMBER ){
                //群成员
                roleName = @"Normal";
            }
            else if(memberInfo.role ==TIM_GROUP_MEMBER_ROLE_ADMIN ){
                //群管理员
                roleName = @"Admin";
            }
            else if(memberInfo.role ==TIM_GROUP_MEMBER_ROLE_SUPER ){
                //群主
                roleName = @"Owner";
            }
            [memberDic setValue:roleName forKey:@"role"];
            [resultArr addObject:memberDic];
            
            
        }
        
       
        PDRPluginResult *result = [PDRPluginResult resultWithStatus:PDRCommandStatusOK messageAsArray:resultArr];
        // 通知JS层Native层运行结果
        [self toCallback:callBackid withReslut:[result toJSONString]];
    } fail:^(int code, NSString *msg) {
        
    }];
}
//***********************     更新群组名     ***********************//
-(void)updateGroupName:(NSString *)callBackid Result:(NSDictionary *)resultDic{
    NSString *groupId = resultDic[@"identifier"];
    NSString *newName = resultDic[@"name"];
    TIMGroupManager *groupManager = [TIMGroupManager sharedInstance];
    [groupManager modifyGroupName:groupId groupName:newName succ:^{
        NSLog(@"modify groupName succ%@",newName);
        PDRPluginResult *result = [PDRPluginResult resultWithStatus:PDRCommandStatusOK messageAsString:@"modify groupName succ"];
        // 通知JS层Native层运行结果
        [self toCallback:callBackid withReslut:[result toJSONString]];
        
    } fail:^(int code, NSString *msg) {
        
    }];
}
//***********************     设置会话的所以消息标记为已读     ***********************//
-(void)setReadMsg:(NSString *)callBackid Result:(NSDictionary *)resultDic{
    NSString *identifier = resultDic[@"identifier"];
      NSString *type = resultDic[@"type"];
    TIMConversationType conType = TIM_C2C;
    if ([type isEqualToString:GROUP]) {
        conType = TIM_GROUP;
    }
    //获取会话实例
    TIMConversation *conversation = [[TIMManager sharedInstance] getConversation:conType receiver:identifier];
    //设置全部消息为已读
    [conversation setReadMessage:NULL succ:^{
        NSLog(@"setReadMessage succ");
        PDRPluginResult *result = [PDRPluginResult resultWithStatus:PDRCommandStatusOK messageAsString:@"setReadMessage succ"];
        // 通知JS层Native层运行结果
        [self toCallback:callBackid withReslut:[result toJSONString]];
        
    } fail:^(int code, NSString *msg) {
        
    }];
}
//***********************     发送文本消息     ***********************//
-(void)resendMessage:(NSString *)callBackid Result:(NSDictionary *)resultDic{
    //构造一条消息
    TIMMessage *msg  = [[TIMMessage alloc] init];
     TIMMessage   *oldMsg = [self findCursorMessage:resultDic];
    
    if (oldMsg == nil) {
        
       
    }else{
        //添加文本
        
        TIMTextElem *elem = [[TIMTextElem alloc] init];
        TIMElem *oldElem = [oldMsg getElem:0];
        if ([oldElem isKindOfClass:[TIMTextElem class]]) {
            TIMTextElem *oldTextElem = (TIMTextElem *)oldElem;
            NSLog(@"textElem:%@",oldTextElem.text);
            [elem setText:oldTextElem.text];
           
        }
        if([msg addElem:elem] != 0) {
            NSLog(@"addElement failed");
            return;
        }
        
    }
    TIMConversation *conversation = [self getConversation:resultDic];
    [conversation sendMessage:msg succ:^(){
        NSDictionary *msgDic = [NSMutableDictionary dictionary];
        NSLog(@"uniqueId:%llu",msg.uniqueId);
        NSLog(@"msgId:%@",msg.msgId);
        msgDic = [self parseMsg:msg];
        NSLog(@"reSendMsg ok ==%@",msgDic);
        PDRPluginResult *result = [PDRPluginResult resultWithStatus:PDRCommandStatusOK messageAsDictionary:msgDic];
        // 通知JS层Native层运行结果
        [self toCallback:callBackid withReslut:[result toJSONString]];
    } fail:^(int code, NSString *msg) {
        
    }];
//    [conversation sendMessage:msg succ:^(TIMMessage *msg){
//
//
//    } fail:^(int code, NSString *msg) {
//
//    }];
}
//***********************     发送事件通知     ***********************//
-(void)sendEvent:(NSString *)callBackid Result:(NSDictionary *)resultDic{
    NSString *event = resultDic[@"event"];
    NSDictionary *info = resultDic[@"data"];
    NSDictionary *data = [self createListenerData:info];
    [self notifyListeners:event data:data];
    
    
}
-(NSDictionary *)createListenerData:(NSObject *)info
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setObject:info forKey:@"data"];
    return data;
    
}
-(void)notifyListeners:(NSString *)event data:(NSDictionary *)data{
    if (_listnersArr == nil) {
        _listnersArr = [[NSArray alloc] init];
        _listnersArr = [self getNotifyModelArrDataByPath:@"listners"];
        if (_listnersArr.count == 0) {
            _listners = [[NSMutableArray alloc] init];
        }else{
            _listners = [_listnersArr mutableCopy];
            //            [NSMutableArray arrayWithArray:_listnersArr];
        }
    }
    NSInteger listnersCount = _listners.count ;
//    if (_listners == nil) {
////        _listners = [[NSMutableArray alloc] init];
//         _listners = [NSMutableArray arrayWithArray: [self getNotifyModelArrDataByPath:@"listners"]];
//    }
    if (listnersCount>0) {
        for (NSInteger i = listnersCount -1; i >= 0; i--) {
            NotifyModel *notifyListener = _listners[i];
            if([notifyListener.action isEqualToString:ACTION_NEW_MSG]){
                PDRPluginResult *result = [PDRPluginResult resultWithStatus:PDRCommandStatusOK messageAsDictionary:data];
                // 通知JS层Native层运行结果
                result.keepCallback = YES;
                [self toCallback:notifyListener.callbackId withReslut:[result toJSONString]];
                break;
            }
            else if ([notifyListener.action isEqualToString:ACTION_UPDATE_CONVERSATION]) {
                PDRPluginResult *result = [PDRPluginResult resultWithStatus:PDRCommandStatusOK messageAsDictionary:data];
                // 通知JS层Native层运行结果
                
                [result setKeepCallback:YES];
                [self toCallback:notifyListener.callbackId withReslut:[result toJSONString]];
                break;
            }
            //        if ([notifyListener.action isEqualToString:event]) {
            PDRPluginResult *result = [PDRPluginResult resultWithStatus:PDRCommandStatusOK messageAsDictionary:data];
            result.keepCallback = YES;
            // 通知JS层Native层运行结果
            [self toCallback:notifyListener.callbackId withReslut:[result toJSONString]];
            //        }
        }
    }else{
        
    }
  
}
//***********************     注册事件通知     ***********************//
-(void)registerIMListener:(NSString *)callBackid Result:(NSDictionary *)resultDic{
    NSString *typeStr = resultDic[@"type"];
    NSLog(@"%@",typeStr);
      [self addListner:callBackid action:typeStr];
//    NSDictionary *data = [self createListenerData:info];
//    [self notifyListeners:event data:data];
    
    
}

-(void)saveDataWriteToFile:(NSString *)type model:(NSString *)model path:(NSString *)path dataObject:(id)dataObject{
    NSArray *arr = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    //虽然该方法返回的是一个数组，但是由于一个目标文件夹只有一个目录，所以数组中只有一个元素。
    
    NSString *cachePath = [arr lastObject];
    NSString *filePath = [cachePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",path]];
    
    NSLog(@"%@",filePath);
    
    
    if ([type isEqualToString:@"arr"]) {
        //创建数据
        
       NSArray *content = [NSArray arrayWithArray:dataObject];
        //将数据存到目标路径的文件中(这个时候如果该路径下文件不存在将会自动创建)
        
        //用writeToFile方法写文件会覆盖掉原来的内容
       
       BOOL isWrite = [content writeToFile:filePath atomically:YES];
//        NSDictionary *dic = [NSDictionary dictionaryWithContentsOfFile:filePath];
        
//        NSLog(@"%@",dic);
//        NSLog(@"%@",isWrite);
    }else{
        //创建数据
         NSDictionary *content = [NSDictionary dictionaryWithDictionary:dataObject];
        //将数据存到目标路径的文件中(这个时候如果该路径下文件不存在将会自动创建)
        
        //用writeToFile方法写文件会覆盖掉原来的内容
        
        [content writeToFile:filePath atomically:YES];
        
    }
   
    
    
    
}
-(id)getDataWriteToFile:(NSString *)type model:(NSString *)model path:(NSString *)path{
    NSArray *arr = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    //虽然该方法返回的是一个数组，但是由于一个目标文件夹只有一个目录，所以数组中只有一个元素。
    
    NSString *cachePath = [arr lastObject];
    NSString *filePath = [cachePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",path]];
    
    NSLog(@"%@",filePath);
    
    //读取数据(通过字典的方式读出文件中的内容)
    NSObject *dataObj = [[NSObject alloc] init];
    if ([type isEqualToString:@"arr"]) {
        dataObj = [NSArray arrayWithContentsOfFile:filePath];
    }else{
        dataObj = [NSDictionary dictionaryWithContentsOfFile:filePath];
    }
    
    
    NSLog(@"%@",dataObj);
    return dataObj;
}
-(void)saveNotifyModelArr:(NSMutableArray *)notifyModelArr path:(NSString *)path{
    //创建一个自定义类的实例
//    for (NSInteger i= 0; i < notifyModelArr.count; i++) {
//        NotifyModel *model = [[NotifyModel alloc] init];
//        model = notifyModelArr[i];
//
//        model.callbackId =
//    }
//
//    personA.name = @"张三";
//    personA.age = 20;
//    personA.sex = @"男";
//
//    Person *personB = [[Person alloc] init];
//    personB.name = @"李四";
//    personB.age = 15;
//    personB.sex = @"女";
//
//    Person *personC = [[Person alloc] init];
//    personC.name = @"王五";
//    personC.age = 30;
//    personC.sex = @"男";
    
    
    
//    NSMutableArray *personsArr = [NSMutableArray arrayWithObjects:personA, personB, personC, nil];
    NSString *paths = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
    NSLog(@"Caches: %@", paths);
    NSString *tempPath = [NSString stringWithFormat:@"/%@.plist",path];
    NSString *notifyArrPath = [paths stringByAppendingString:tempPath];
    NSArray *saveNotifyModelArr = [[NSArray alloc] init];
     saveNotifyModelArr = [notifyModelArr copy];
    BOOL isSave = [NSKeyedArchiver archiveRootObject:saveNotifyModelArr toFile:notifyArrPath];
    NSLog(@"personsArrPath: %@", notifyArrPath);
    
    NSArray *newPersonsArr = [NSKeyedUnarchiver unarchiveObjectWithFile:notifyArrPath];
    
    NSLog(@"反归档: %@", newPersonsArr);
    
    for (NotifyModel *tempPerson in newPersonsArr) {
        NSLog(@"callbackId: %@, action: %@", tempPerson.callbackId, tempPerson.action);
    }
    
    
    NSLog(@"文件已储存");
    
   
    
}
-(NSArray *)getNotifyModelArrDataByPath:(NSString *)path{
    
    //获取文件路径
    
    NSString *paths = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
    NSLog(@"Caches: %@", paths);
    NSString *tempPath = [NSString stringWithFormat:@"/%@.plist",path];
    NSString *notifyArrPath = [paths stringByAppendingString:tempPath];
    
    NSArray *newPersonsArr = [NSKeyedUnarchiver unarchiveObjectWithFile:notifyArrPath];
    
    NSLog(@"反归档: %@", newPersonsArr);
    
    for (NotifyModel *tempPerson in newPersonsArr) {
        NSLog(@"callbackId: %@, action: %@", tempPerson.callbackId, tempPerson.action);
    }
    
//    NSLog(@"name = %@ , age =%ld , sex = %@ , familyMubers = %@",person.name,person.age,person.sex,person.familyMumbers);
    
    NSLog(@"文件已提取");
    if (newPersonsArr == nil) {
        newPersonsArr = [[NSArray alloc] init];
    }
    return newPersonsArr;
  
}
-(void)removeListnersWithPath:(NSString *)path{
    NSString *paths = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
    NSLog(@"Caches: %@", paths);
    NSString *tempPath = [NSString stringWithFormat:@"/%@.plist",path];
    NSString *notifyArrPath = [paths stringByAppendingString:tempPath];
    
 
    
    NSError *error = nil;
     BOOL isRemove = [[NSFileManager defaultManager] removeItemAtPath:notifyArrPath error:&error];
//     [self getNotifyModelArrDataByPath:@"listners"];
}
//NotifyListener 方法
//-(void)NotifyListener:(NSString *)callbackId action:(NSString *)action{
//    NotifyListener *notify = [[NotifyListener alloc] init];
//    notify.callbackId = callbackId;
//    notify.action = action;
//}
@end
