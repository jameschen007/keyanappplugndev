(function(){
	var imageIndexIdNum = 0;
			var index = 0;
			var size = null;
			
		var detail = {
	
		imageList: document.getElementById('image-list'),
		submitBtn:document.getElementById('submit'),
		description:document.getElementById('reformDesc')
		
	};
	detail.files = [];
	detail.uploader = null;
	detail.deviceInfo = null;
	
	detail.getFileInputArray = function() {
		return [].slice.call(detail.imageList.querySelectorAll('.file'));
	};

	detail.addFile = function(path) {
		detail.files.push({
			name: "images" + index,
			path: path,
			id: "img-" + index
		});
		index++;
//		mui.alert('addFile1:'+JSON.stringify(detail.files));
	};
	/**
	 * 初始化图片域占位
	 */
	
	
detail.clearForm = function() {
	var imageIndexIdNum = 0;
			var index = 0;
			var size = null;
		detail.files = [];
	detail.uploader = null;
	detail.deviceInfo = null;

	};
	detail.newPlaceholder = function() {
		var fileInputArray = detail.getFileInputArray();
		if(fileInputArray &&
			fileInputArray.length > 0 &&
			fileInputArray[fileInputArray.length - 1].parentNode.classList.contains('space')) {
			return;
		};
		imageIndexIdNum++;
		var placeholder = document.createElement('div');
		placeholder.setAttribute('class', 'image-item space');
		var up = document.createElement("div");
		up.setAttribute('class', 'image-up')
		//删除图片
		var closeButton = document.createElement('div');
		closeButton.setAttribute('class', 'image-close');
		closeButton.innerHTML = 'X';
		closeButton.id = "img-" + index;
		//小X的点击事件
		closeButton.addEventListener('tap', function(event) {
			setTimeout(function() {
				for(var temp = 0; temp < detail.files.length; temp++) {
					if(detail.files[temp].id == closeButton.id) {
						detail.files.splice(temp, 1);
					}
				}
				detail.imageList.removeChild(placeholder);
			}, 0);
			return false;
		}, false);

		//
		var fileInput = document.createElement('div');
		fileInput.setAttribute('class', 'file');
		fileInput.setAttribute('id', 'image-' + imageIndexIdNum);
		fileInput.addEventListener('tap', function(event) {
			var self = this;
			var index = (this.id).substr(-1);

			plus.gallery.pick(function(e) {
				//				console.log("event:"+e);
				var name = e.substr(e.lastIndexOf('/') + 1);
				console.log("name:" + name);
//mui.toast('压缩成功2！');
				plus.zip.compressImage({
					src: e,
					dst: '_doc/' + name,
					overwrite: true,
					quality: 50
				}, function(zip) {
//					mui.toast('压缩成功3！');
//					mui.toast('压缩成功3！'+e);
					size += zip.size;
					
//					mui.alert("filesize:" + zip.size + ",totalsize:" + size);
					if(size > (10 * 1024 * 1024)) {
						return mui.toast('文件超大,请重新选择~');
					}
					if(!self.parentNode.classList.contains('space')) { //已有图片
//						mui.toast('压缩成功2！');
						detail.files.splice(index - 1, 1, {
							name: "images" + index,
							path: e
						});
					} else { //加号
//						mui.toast('压缩成功1！');
						placeholder.classList.remove('space');
						
						detail.addFile(zip.target);
						detail.newPlaceholder();
					}
					up.classList.remove('image-up');
//					mui.toast('压缩成功3！');
					placeholder.style.backgroundImage = 'url(' + zip.target + ')';
				}, function(zipe) {
					mui.toast('压缩失败！')
				});

			}, function(e) {
				mui.toast(e.message);
			}, {});
		}, false);
		placeholder.appendChild(closeButton);
		placeholder.appendChild(up);
		placeholder.appendChild(fileInput);
		detail.imageList.appendChild(placeholder);
		

	};
	detail.newPlaceholder();
	detail.submitBtn.addEventListener('tap', function(event) {

			//判断网络连接
		if(mui.os.plus){
		if(plus.networkinfo.getCurrentType() == plus.networkinfo.CONNECTION_NONE) {
            return mui.toast("连接网络失败，请稍后再试");
        }
		}
												if(detail.description.value == '') {
			return mui.toast('请填写整改描述');
		}
				
				var filesArr = detail.files;
//				logAlert('filesArr.length:'+filesArr.length);
				if(filesArr.length == 0) {
			return mui.toast('请添加整改照片');
		}
				
		
		var user = getCurrentUser();
			var self = plus.webview.currentWebview();
				jsonData = self.jsonData;
		var problemDataId = JSON.stringify(jsonData.hseProblemSolve[0].problemId);
//		mui.toast(user.id.toString());
//		mui.toast(problemDataId);
//		mui.toast(detail.description.value);
//		mui.toast(JSON.stringify(detail.files));
		
		detail.send(mui.extend({}, detail.deviceInfo, {
			loginId:user.id.toString(),
			problemId:problemDataId,		
			description:detail.description.value,
			images: detail.files
		}))
	}, false);
	detail.send = function(content) {
//			var user = getCurrentUser();
//			mui.alert(user.id);
		var server=httpDomain+ "/hse/submitRenovateResult";
//		var server="http://111.231.52.72/hdxt/api/hse/createFile"

		detail.uploader = plus.uploader.createUpload(server, {
			method: 'POST'
		}, function(upload, status) {
			plus.nativeUI.closeWaiting()
			console.log("upload cb:"+upload.responseText);
//			detail.clearForm();
//			mui.alert('detail.uploade');
				var wobj = plus.webview.getWebviewById("reform");
						if(wobj){
						wobj.reload(true);
								}
//						mui.alert("上传成功");
				mui.back();
			
			if(status==200){
				var data = JSON.parse(upload.responseText);
//				mui.alert("上传成功："+upload.responseText);
				//上传成功，重置表单
				if (data.ret === 0 && data.desc === 'Success') {
					mui.toast('反馈成功~')
					console.log("upload success");
//					detail.clearForm();
				}
			}else{
				console.log("upload fail");
							mui.alert("上传失败："+status);

			}
			
		});
		//添加上传数据
		mui.each(content, function(index, element) {
			if (index !== 'images') {
//				mui.alert("addData:"+index+","+element);
//				console.log(index);
				detail.uploader.addData(index, element)
			} 
		});
		//添加上传文件
		mui.each(detail.files, function(index, element) {
			var f = detail.files[index];
			var fileNum = index+ 1;
//			mui.alert("addFile:"+JSON.stringify(f));
			detail.uploader.addFile(f.path, {
//				key: f.name
				key: 'file'+fileNum
			});
		});
//		mui.alert(JSON.stringify(detail.files)); 
		//开始上传任务
		detail.uploader.start();
	
		
			
			
	
		plus.nativeUI.showWaiting();
	};
	
})();
