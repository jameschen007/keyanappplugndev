(function(){
	var imageIndexIdNum = 0;
			var index = 1;
		var done_detail = {
	
		imageList: document.getElementById('image-list'),
		
	};
	done_detail.getFileInputArray = function() {
		return [].slice.call(done_detail.imageList.querySelectorAll('.file'));
	};
	done_detail.addFile = function(path) {
		done_detail.files.push({
			name: "images" + index,
			path: path,
			id: "img-" + index
		});
		index++;
	};
	/**
	 * 初始化图片域占位
	 */
	done_detail.newPlaceholder = function() {
		var fileInputArray = done_detail.getFileInputArray();
		if(fileInputArray &&
			fileInputArray.length > 0 &&
			fileInputArray[fileInputArray.length - 1].parentNode.classList.contains('space')) {
			return;
		};
		imageIndexIdNum++;
		var placeholder = document.createElement('div');
		placeholder.setAttribute('class', 'image-item space');
		var up = document.createElement("div");
		up.setAttribute('class', 'image-up')
		//删除图片
		var closeButton = document.createElement('div');
		closeButton.setAttribute('class', 'image-close');
		closeButton.innerHTML = 'X';
		closeButton.id = "img-" + index;
		//小X的点击事件
		closeButton.addEventListener('tap', function(event) {
			setTimeout(function() {
				for(var temp = 0; temp < done_detail.files.length; temp++) {
					if(done_detail.files[temp].id == closeButton.id) {
						done_detail.files.splice(temp, 1);
					}
				}
				done_detail.imageList.removeChild(placeholder);
			}, 0);
			return false;
		}, false);

		//
		var fileInput = document.createElement('div');
		fileInput.setAttribute('class', 'file');
		fileInput.setAttribute('id', 'image-' + imageIndexIdNum);
		fileInput.addEventListener('tap', function(event) {
			var self = this;
			var index = (this.id).substr(-1);

			plus.gallery.pick(function(e) {
				//				console.log("event:"+e);
				var name = e.substr(e.lastIndexOf('/') + 1);
				console.log("name:" + name);

				plus.zip.compressImage({
					src: e,
					dst: '_doc/' + name,
					overwrite: true,
					quality: 50
				}, function(zip) {
					size += zip.size
					console.log("filesize:" + zip.size + ",totalsize:" + size);
					if(size > (10 * 1024 * 1024)) {
						return mui.toast('文件超大,请重新选择~');
					}
					if(!self.parentNode.classList.contains('space')) { //已有图片
						done_detail.files.splice(index - 1, 1, {
							name: "images" + index,
							path: e
						});
					} else { //加号
						placeholder.classList.remove('space');
						done_detail.addFile(zip.target);
						done_detail.newPlaceholder();
					}
					up.classList.remove('image-up');
					placeholder.style.backgroundImage = 'url(' + zip.target + ')';
				}, function(zipe) {
					mui.toast('压缩失败！')
				});

			}, function(e) {
				mui.toast(e.message);
			}, {});
		}, false);
		placeholder.appendChild(closeButton);
		placeholder.appendChild(up);
		placeholder.appendChild(fileInput);
		done_detail.imageList.appendChild(placeholder);
	};
	done_detail.newPlaceholder();
	done_detail.submitBtn.addEventListener('tap', function(event) {
		if(done_detail.question.value == '') {
			return mui.toast('请输入问题名称');
		}
		if(done_detail.question.value.length > 20) {
			return mui.toast('问题名称超长,请重新填写~');
		}
			var filesArr = done_detail.files;
//				logAlert('filesArr.length:'+filesArr.length);
				if(filesArr.length == 0) {
			return mui.toast('请添加整改照片');
		}
		
		//判断网络连接
		if(plus.networkinfo.getCurrentType() == plus.networkinfo.CONNECTION_NONE) {
			return mui.toast("连接网络失败，请稍后再试");
		}
		done_detail.send(mui.extend({}, done_detail.deviceInfo, {
			content: done_detail.question.value,
			images: done_detail.files,
			score: '' + starIndex
		}))
	}, false)
	
})();
