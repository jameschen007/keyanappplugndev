document.write('<script src="../chat/js/webIM.js"><\/script>');
var self;



document.addEventListener("plusready", function() {
	var _BARCODE = 'TIMManager',
		B = window.plus.bridge;
	self = this;
	var TIMManager = {
		callIMCmd: function(Argus, successCallback, errorCallback) {
			
			
			var success = typeof successCallback !== 'function' ? null : function(args) {
					
					successCallback(args);
					
				},
				fail = typeof errorCallback !== 'function' ? null : function(code) {
					
					errorCallback(code);
				};
			callbackID = B.callbackId(success, fail);
			if (mui.os.ios) {
					
					 var arguDic = JSON.parse(Argus);
			 
				if (arguDic.action == 'registerIMListener') {

					if (arguDic.type == 'action_new_msg') {
						localStorage.newMsgListner = '' + callbackID;
//						mui.alert('action_new_msg:'+localStorage.newMsgListner);
					}
					if(arguDic.type == 'action_update_conversation') {
						
						localStorage.conversationUpdateLisnter = '' + callbackID;
//						mui.alert('action_update_conversation:'+localStorage.conversationUpdateLisnter);
					}
					if (arguDic.type == 'action_new_msg2') {
						localStorage.newMsgListner2 = '' + callbackID;
//						mui.alert('action_new_msg2:'+localStorage.newMsgListner2);
					}
					if(arguDic.type == 'action_update_conversation2') {
						
						localStorage.conversationUpdateLisnter2 = '' + callbackID;
//						mui.alert('action_update_conversation2:'+localStorage.conversationUpdateLisnter2);
					}
					if(arguDic.type == 'action_new_msg3') {
						
						localStorage.newMsgListner3 = '' + callbackID;
//						mui.alert('action_new_msg3:'+localStorage.newMsgListner3);
					}
				}
//				mui.alert('callIMCmd:'+JSON.stringify(Argus));
			} else{
				
			}
			
			return B.exec(_BARCODE, "callIMCmd", [callbackID, Argus]);
		},
		callIMCmdSync: function(Argus) {

			return B.exec(_BARCODE, "callIMCmdSync", [Argus]);
		},
		registerIMListener: function(Argus, IdCallback, successCallback, errorCallback) {

			var success = typeof successCallback !== 'function' ? null : function(args) {
					successCallback(args);
				
					
				},
				fail = typeof errorCallback !== 'function' ? null : function(code) {
					errorCallback(code);
					
					
				};
			callbackID = B.callbackId(success, fail);
			IdCallback(callbackID);
			

			return B.exec(_BARCODE, "registerIMListener", [callbackID, Argus]);
		},
		iosregisterIMListener: function(Argus, successCallback, errorCallback) {
			var success = typeof successCallback !== 'function' ? null : function(args) {
					successCallback(args);
				},
				fail = typeof errorCallback !== 'function' ? null : function(code) {
					errorCallback(code);
				};
			callbackID = B.callbackId(success, fail);

			return B.exec(_BARCODE, "registerIMListener", [callbackID, Argus]);
		},
		unregisterIMListener: function(Argus, successCallback, errorCallback) {
			var success = typeof successCallback !== 'function' ? null : function(args) {
					successCallback(args);
				},
				fail = typeof errorCallback !== 'function' ? null : function(code) {
					errorCallback(code);
				};
			callbackID = B.callbackId(success, fail);

			return B.exec(_BARCODE, "unregisterIMListener", [callbackID, Argus]);
		}

	};
	window.plus.TIMManager = TIMManager;
}, true);

function iosregisterListener(action,callbackId, succ, fail) {
	var param = {};
	param.action = action;
	param.CallBackID = callbackId;

	if(mui.os.plus) {
//		logAlert('registerListener2ios');
		plus.TIMManager.iosregisterIMListener(JSON.stringify(param),
			succ,
			fail
		);
	} else {

	}
}
function iOSRegisterIMListener(type, succ, fail) {

	var param = {};
	param.action = "registerIMListener";

	param.type = type;
	

	IMCmdFuntion(param, succ, fail);
}
function registerListener(action, succ, fail) {
	var param = {};
	param.action = action;
	var id = '';
	if(mui.os.plus) {
		
		if(mui.os.ios){
			
			plus.TIMManager.registerIMListener()
				plus.TIMManager.registerIMListener(JSON.stringify(param), function(idCallback) {
				id = idCallback;
				
			},
			succ,
			fail
		);
		
		}else{
//			logAlert('registerListener1android');
				plus.TIMManager.registerIMListener(JSON.stringify(param), function(idCallback) {
				id = idCallback;
			},
			succ,
			fail
		);
		}
	
	} else {
//		logAlert('registerListener2');
	}
	return id;
}

function unregisterListener(action, callbackId, succ, fail) {
	var param = {};
	param.action = action;
	param.CallBackID = callbackId;
//		mui.alert('unregisterListener');
	if(mui.os.plus) {
		plus.TIMManager.unregisterIMListener(JSON.stringify(param),
			succ,
			fail
		);
	} else {

	}
}

function deleteMessage(msg) {

	var param = {};
	param.action = "sync_cmd_delete_msg";

	param.msgId = msg.msgId;
	param.uniqueId = msg.uniqueId;
	param.timestamp = msg.timestamp;
	param.rand = msg.rand;
	param.seq = msg.seq;
	param.self = msg.self;

	return SyncIMCmdFuntion(param);
}


function resendMessage(identifier, type, msg, succ, fail) {

	var param = {};
	param.action = "cmd_resend_msg";
	param.identifier = identifier;
	param.type = type;

	param.msgId = msg.msgId;
	param.uniqueId = msg.uniqueId;
	param.timestamp = msg.timestamp;
	param.rand = msg.rand;
	param.seq = msg.seq;
	param.self = msg.self;

	IMCmdFuntion(param, succ, fail);
}

function getSoundFile(msg, succ, fail) {

	var param = {};
	param.action = "cmd_sound_file";

	param.msgId = msg.msgId;
	param.uniqueId = msg.uniqueId;
	param.timestamp = msg.timestamp;
	param.rand = msg.rand;
	param.seq = msg.seq;
	param.self = msg.self;

	IMCmdFuntion(param, succ, fail);
}

function getLocalMessageList(identifier, type, msg, size, succ, fail) {
	var param = {};
	param.action = "cmd_msg_list";
	param.identifier = identifier;
	if(msg) {
		param.msgId = msg.msgId;
		param.uniqueId = msg.uniqueId;
		param.timestamp = msg.timestamp;
		param.rand = msg.rand;
		param.self = msg.self;

	}

	param.type = type;
	param.size = size;
	IMCmdFuntion(param, succ, fail);
}

function sendImageMsg(identifier, type, filePath, succ, fail) {
	var param = {};
	param.msgType = "image";
	param.filePath = filePath;
	sendMsg(identifier, type, param, succ, fail);
}

function sendTextMsg(identifier, type, content, succ, fail) {
	var param = {};
	param.msgType = "text";
	param.content = content;
	sendMsg(identifier, type, param, succ, fail);
}

function sendVoiceMsg(identifier, type, filePath, duration, succ, fail) {
	var param = {};
	param.msgType = "voice";
	param.filePath = filePath;
	param.duration = duration;
	sendMsg(identifier, type, param, succ, fail);
}

function sendMsg(identifier, type, param, succ, fail) {
	param.action = "cmd_send_msg";
	param.identifier = identifier;
	param.type = type;
	IMCmdFuntion(param, succ, fail);
}

function quitGroup(identifier, succ, fail) {
	var param = {};
	param.action = "cmd_quit_group";
	param.identifier = identifier;
	IMCmdFuntion(param, succ, fail);
}

function sendEvent(event, data, succ, fail) {
	var param = {};
	param.action = "cmd_send_event";
	param.event = event;
	param.data = data;
	IMCmdFuntion(param, succ, fail);
}

function readMessage(identifier, type, succ, fail) {
	var param = {};
	param.action = "cmd_read_msg";
	param.type = type;
	param.identifier = identifier;
	IMCmdFuntion(param, succ, fail);
}

function deleteGroupMember(identifier, members, succ, fail) {
	var param = {};
	param.action = "cmd_delete_group_member";
	param.members = members;
	param.identifier = identifier;
	IMCmdFuntion(param, succ, fail);
}


function addGroupMember(identifier, members, succ, fail) {
	var param = {};
	param.action = "cmd_add_group_member";
	param.members = members;
	param.identifier = identifier;
	IMCmdFuntion(param, succ, fail);
}

function getGroupMember(identifier, succ, fail) {
	var param = {};
	param.action = "cmd_get_group_member";
	param.identifier = identifier;
	IMCmdFuntion(param, succ, fail);
}

function updateGroupName(identifier, name, succ, fail) {
	var param = {};
	param.action = "cmd_update_group_name";
	param.name = name;
	param.identifier = identifier;
	IMCmdFuntion(param, succ, fail);
}

function getConvList(type, succ, fail) {
	var param = {};
	param.action = "cmd_conversations";
	param.type = type;
//	param.notCheckUser = true;
	param.notCheckUser = false;
//	logAlert("getConvList param:"+type);	
	IMCmdFuntion(param, succ, fail);
}
function testSendParam(sendParam,succ,fail){
	
	
//	var param = {};
//	param.action = sendParam.action;
//	param.identifier = sendParam.username;
//	param.userSig = sendParam.userSigInfo.userSig;
//	param.action = "cmd_conversations";
//	if(sendParam.type){
//	param.type = type;
//	}
	
//	param.notCheckUser = false;
//	logAlert("getConvList param:"+type);	
//logAlert("testSendParam:"+JSON.stringify(sendParam));
//	var parseParam = JSON.parse(param);
	logAlert('testSendParam');
	IMCmdFuntion(sendParam, succ, fail);
	
	
}

function createGroupChat(name, members, succ, fail) {
	var param = {};
	param.action = "cmd_create_group";
	param.name = name;
	param.members = members;
	IMCmdFuntion(param, succ, fail);
}

function logout(succ, fail) {
	if(true) {
//		mui.alert("logout");
			var name = getLoginUserId();
			var param = {
					username: name
				}
			
					post('/logout', param, success, error);
					function success(data, param, dataStringify) {
					
//							
					localStorage.removeItem("cid");
						
				logAlert("数据数据-----" + dataStringify);
						
						mui.alert("退出当前账户成功");
						
//					

					}

					function error(info) {
					mui.alert("退出失败！");
//						console.log();
					}
//		mui.alert('logout');
		return;
	}
	var userInfo = getCurrentUser();
	if(userInfo && userInfo.userSigInfo) {
		var param = {};
		param.action = "cmd_logout";
		param.identifier = userInfo.username;

		if(mui.os.plus) {
			plus.TIMManager.callIMCmd(JSON.stringify(param),
				succ,
				fail
			);
		} else {
			callIMCmd(JSON.stringify(param),
				succ,
				fail
			);
		}

	}

}

function login(succ, fail) {
//		logAlert("login:1");
	var userInfo = getCurrentUser();
	if(userInfo && userInfo.userSigInfo) {
		var param = {};
		param.action = "cmd_login";
		param.identifier = userInfo.username;
		param.userSig = userInfo.userSigInfo.userSig;

		if(mui.os.plus) {
//					logAlert('login1');
			plus.TIMManager.callIMCmd(JSON.stringify(param),
				succ,
				fail
			);
		} else {
			callIMCmd(JSON.stringify(param),
				succ,
				fail
			);
			
		}

		return;
	}
	var param = {
		disableLoading: true
	}
	post('/tls', param, function(data, param, dataStringify) {
		var userSigInfo = data.responseResult;

		var userInfo = getCurrentUser();
		if(userInfo) {
//			logAlert('post/tls success '+JSON.stringify(userInfo));
			userInfo.userSigInfo = userSigInfo;
			storeUserInfo(userInfo);
//			logAlert('login2');
			login(succ, fail);
		} else {
			fail("error when store data");
			console.log('tls auth not found the local userInfo data');
		}

	}, function(info) {
//		logAlert('post/tls error'+JSON.stringify(info));
		fail(info);
	});
}

var contactInfo = [];
var groupUserInfo = [];

function fetchContacts() {
	var localModules = localStorage.getItem("contact_modules" + "_" + getLoginUserId());
	if(localModules) {
		return;
	}
	get('/getUserList', {}, function(data, param, dataStringify) {
		var modulesData = data.responseResult;
		localStorage.setItem("contact_modules" + "_" + getLoginUserId(), JSON.stringify(modulesData));
		//notify.

	}, function(info) {
		console.log("请求失败！" + info);
	});

};

function findUserName(identifier) {
	if(contactInfo.length == 0) {
		var localModules = localStorage.getItem("contact_modules" + "_" + getLoginUserId());
		if(localModules) {
			var result = JSON.parse(localModules);
			contactInfo = result.data;
		} else {
			fetchContacts();
			return identifier;

		}
	}

	for(var i = 0; i < groupUserInfo.length; i++) {
		if(groupUserInfo[i].username == identifier) {
			return groupUserInfo[i].realname;
		}

	}

	for(var i = 0; i < contactInfo.length; i++) {
		if(contactInfo[i].username == identifier) {
			groupUserInfo.push(contactInfo[i]);
			return contactInfo[i].realname;
		}

	}

	return identifier;
}

function checkMessageContentLink(msg) {
	 var strRegex = "^((https|http|ftp|rtsp|mms)?://)"
   + "?(([0-9a-z_!~*'().&=+$%-]+: )?[0-9a-z_!~*'().&=+$%-]+@)?" //ftp的user@ 
      + "(([0-9]{1,3}\.){3}[0-9]{1,3}" // IP形式的URL- 199.194.52.184 
      + "|" // 允许IP和DOMAIN（域名）
      + "([0-9a-z_!~*'()-]+\.)*" // 域名- www. 
      + "([0-9a-z][0-9a-z-]{0,61})?[0-9a-z]\." // 二级域名 
      + "[a-z]{2,6})" // first level domain- .com or .museum 
      + "(:[0-9]{1,4})?" // 端口- :80 
      + "((/?)|" // a slash isn't required if there is no file name 
      + "(/[0-9a-z_!~*'().;?:@&=+$,%#-]+)+/?)$"; 
    var re=new RegExp(strRegex,'g'); 

	var content = msg.content;
   
	var item = content.match(re);
	var splitContent = [];


	if(item && item.length > 0) {

		for(var i = 0; i < item.length; i++) {
			var text = item[i];
			//alert('msg==' + text);
			if(!text) {
				continue
			}
			var index = content.indexOf(text);

			var tag = {
				info: content.substring(index, text.length),
				haslink: true
			};

			if(index > 0) {
				splitContent.push(tag);
				var normal = {
					info: content.substring(0, index)
				};
				splitContent.push(normal);

			} else {
				// from 0
				splitContent.push(tag);
			}

			content = content.substring(index + text.length, content.length);

			if(i == item.length - 1 && content.length > 0) {
				splitContent.push({
					info: content.substring(0, index)
				});
				
			}

		}
	}
	if(splitContent.length > 0) {
		msg.splitContent = splitContent;
		//alert('msg==' + JSON.stringify(msg.splitContent));
	}

}

function replaceUser(content) {
	var r = /\[(.+?)\]/g;
	var item = content.match(r);

	if(item && item.length > 0) {

		for(var i = 0; i < item.length; i++) {
			var text = item[i];
			content = content.replace(text, findUserName(text.substring(1, text.length - 1)));
		}
	}
	return content;
}


function SyncIMCmdFuntion(param) {
	var userInfo = getCurrentUser();
	
	if(param.notCheckUser || userInfo && userInfo.userSigInfo) {
		if(mui.os.plus) {
			
			return plus.TIMManager.callIMCmdSync(JSON.stringify(param));
		} else {
			//return SyncIMCmdFuntion(JSON.stringify(param));
		}
	} else {

		var succLogin = function(succResult) {
			SyncIMCmdFuntion(param);

		};

		var failed = function(failedResult) {
			fail(failedResult);
		};

		login(succLogin, failed);

	}

}
function initIMSDK(param,succ,fail){

//	logAlert('initIMSDK');
	IMCmdFuntion(param, succ, fail);
		
}

function IMCmdFuntion(param, succ, fail) {
	var userInfo = getCurrentUser();
		
//	if(param.notCheckUser == false ) {
		if(param.notCheckUser || userInfo && userInfo.userSigInfo ) {
		
//		logAlert("hasUserInfo："+param.notCheckUser);
		if(mui.os.plus) {
//			logAlert("hasPlus1"+JSON.stringify(param));
//		plus.TIMManager.callIMCmd(JSON.stringify(param),
//				succ,
//				fail
//			);
			mui.plusReady(function() {
//					logAlert("hasPlus1"+JSON.stringify(param));
				plus.TIMManager.callIMCmd(JSON.stringify(param),
					succ,
					fail
				);
				

			});

		} else {
//				logAlert("noPlus");
			callIMCmd(JSON.stringify(param),
				succ,
				fail
			);
		}
	} else {
//	logAlert("login:31"+param.type);
		var succLogin = function(succResult) {
//			logAlert("succResult:"+JSON.stringify(succResult));
			IMCmdFuntion(param, succ, fail);

		};

		var failed = function(failedResult) {
//			logAlert("failed:"+failedResult);
			fail(failedResult);
		};

		login(succLogin, failed);

	}

}


function getMessageById(msgId,records){
	for (var i = records.length-1; i >= 0; i--) {
		if(records[i].msgId == msgId){
			return records[i];
		}
	}
	return null;
}

function removeMessageById(msgId,records){
	for (var i = records.length-1; i >= 0; i--) {
		if(records[i].msgId == msgId){
			records.splice(i,1);
			return  records;
			
		}
	}
	return null;
}
function getMessageByIdWithiOS(uniqueId,records){
	for (var i = records.length-1; i >= 0; i--) {
		if(records[i].uniqueId == uniqueId){
			return records[i];
		}
	}
	return null;
}

function removeMessageByIdWithiOS(uniqueId,records){
	for (var i = records.length-1; i >= 0; i--) {
		if(records[i].uniqueId == uniqueId){
			records.splice(i,1);
			return  records;
			
		}
	}
	return null;
}
