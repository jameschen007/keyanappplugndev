package com.example.H5PlusPlugin;

/**
 * Created by Administrator on 2017/11/17 0017.
 */

import android.annotation.TargetApi;
import android.app.KeyguardManager;
import android.content.Context;
import android.os.Build;

/**
 * Created by Alen   on 2017/11/15 0015.
 * 指纹识别模块
 */
public class FingerprintUtil {
    public static final int DEVICENOTSUPPORTED = 1011;//设备不支持
    public static final int DEVICENOTSAFEGUARD = 1012;//设备未处于安全保护中
    public static final int DEVICENOTREGUIDFIN = 1013;//设备没有注册过指纹
    public static final int SUCCESS = 1000;//支持指纹识别
    public static final int FINGETSUCCESS = 1002;//指纹识别成功
    public static final int ERROR = 1001;//指纹识别失败


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static  void callFingerPrint(final OnCallBackListenr listener,Context context){

        ResultObj res = checkFingerPrint("搜索",context);//检查指纹识别
        if(res.getCode()!=SUCCESS){
            if (listener != null){
                if(res.getCode()==DEVICENOTSUPPORTED){//判断设备是否支持
                    listener.onSupportFailed(res);
                }else if(res.getCode()==DEVICENOTSAFEGUARD){//判断设备是否处于安全保护中
                    listener.onInsecurity(res);
                }else if(res.getCode()==DEVICENOTSAFEGUARD){//设备没有注册过指纹
                    listener.onEnrollFailed(res);
                }
            }
            return;
        }
        //if (listener != null)
            //listener.onAuthenticationStart(res); //开始指纹识别


    }

    /**
     * 取消指纹识别
     */
    public static void cancelFingerPrint(){

    }

    /**
     * 判断当前设备是否支持指纹识别
     * @return
     */
    public static ResultObj checkFingerPrint(Context context){

        return FingerprintUtil.checkFingerPrint("ss",context);
    }

    /**
     * 判断当前设备是否支持指纹识别
     * @param managerCompat
     * @return
     */
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static ResultObj checkFingerPrint(String managerCompat,Context context){

        return new ResultObj(FingerprintUtil.SUCCESS,"支持指纹识别");
    }


    public interface  OnCallBackListenr{
        void onSupportFailed(ResultObj error);
        void onInsecurity(ResultObj error);
        void onEnrollFailed(ResultObj error);
        void onAuthenticationStart(ResultObj error);
        void onAuthenticationError(int errMsgId, CharSequence errString);
        void onAuthenticationFailed(ResultObj error);
        void onAuthenticationHelp(int helpMsgId, CharSequence helpString);
    }

    public static class ResultObj{
        private int code;
        private String msg;
        public ResultObj(int code,String msg){
            this.code = code;
            this.msg = msg;
        }

        public ResultObj getResult(){
            return  this;
        }
        public int getCode(){
            return  code;
        }
        public String getMsg(){
            return  msg;
        }
    }

    public static  void cancel(){

    }


}
