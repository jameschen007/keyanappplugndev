/*!
 * ======================================================
 * report Template For MUI (http://dev.dcloud.net.cn/mui)
 * =======================================================
 * @version:1.0.0
 * @author:cuihongbao@dcloud.io
 */
(function() {
	var index = 1;
	var size = null;
	var imageIndexIdNum = 0;
	var starIndex = 0;

	var report = {
		question: document.getElementById('question'),
		eleration: document.getElementById('eleration'),
		roomno: document.getElementById('roomno'),
		imageList: document.getElementById('image-list'),
		submitBtn: document.getElementById('submit'),
		description:document.getElementById('description'),
		unitPicker:document.getElementById('unitPicker'),
		wrokshopPicker:document.getElementById('wrokshopPicker'),
		responsibleDeptPicker:document.getElementById('responsibleDeptPicker'),
		responsibleTeamPicker:document.getElementById('responsibleTeamPicker'),
	};
	
	report.files = [];
	report.uploader = null;
	report.deviceInfo = null;
	mui.plusReady(function() {
  
	});
	/**
	 *提交成功之后，恢复表单项 
	 */
	report.clearForm = function() {
		report.question.value = '';
		report.imageList.innerHTML = '';
		report.newPlaceholder();
		report.files = [];
		index = 0;
		size = 0;
		imageIndexIdNum = 0;
		starIndex = 0;

	};
	report.getFileInputArray = function() {
		return [].slice.call(report.imageList.querySelectorAll('.file'));
	};
	report.addFile = function(path) {
		report.files.push({
			name: "images" + index,
			path: path,
			id: "img-" + index
		});
		index++;
	};
	/**
	 * 初始化图片域占位
	 */
	report.newPlaceholder = function() {
		var fileInputArray = report.getFileInputArray();
		if(fileInputArray &&
			fileInputArray.length > 0 &&
			fileInputArray[fileInputArray.length - 1].parentNode.classList.contains('space')) {
			return;
		};
		imageIndexIdNum++;
		var placeholder = document.createElement('div');
		placeholder.setAttribute('class', 'image-item space');
		var up = document.createElement("div");
		up.setAttribute('class', 'image-up')
		//删除图片
		var closeButton = document.createElement('div');
		closeButton.setAttribute('class', 'image-close');
		closeButton.innerHTML = 'X';
		closeButton.id = "img-" + index;
		//小X的点击事件
		closeButton.addEventListener('tap', function(event) {
			setTimeout(function() {
				for(var temp = 0; temp < report.files.length; temp++) {
					if(report.files[temp].id == closeButton.id) {
						report.files.splice(temp, 1);
					}
				}
				report.imageList.removeChild(placeholder);
			}, 0);
			return false;
		}, false);

		//
		var fileInput = document.createElement('div');
		fileInput.setAttribute('class', 'file');
		fileInput.setAttribute('id', 'image-' + imageIndexIdNum);
		fileInput.addEventListener('tap', function(event) {
			var self = this;
			var index = (this.id).substr(-1);

			plus.gallery.pick(function(e) {
				//				console.log("event:"+e);
				var name = e.substr(e.lastIndexOf('/') + 1);
				console.log("name:" + name);

				plus.zip.compressImage({
					src: e,
					dst: '_doc/' + name,
					overwrite: true,
					quality: 50
				}, function(zip) {
					size += zip.size
//					mui.alert(e);
//					console.log("filesize:" + zip.size + ",totalsize:" + size);
					if(size > (10 * 1024 * 1024)) {
						return mui.toast('文件超大,请重新选择~');
					}
					if(!self.parentNode.classList.contains('space')) { //已有图片
						report.files.splice(index - 1, 1, {
							name: "images" + index,
							path: e
						});
					} else { //加号
						placeholder.classList.remove('space');
						report.addFile(zip.target);
						report.newPlaceholder();
					}
					up.classList.remove('image-up');
					placeholder.style.backgroundImage = 'url(' + zip.target + ')';
				}, function(zipe) {
					mui.toast('压缩失败！')
				});

			}, function(e) {
				mui.toast(e.message);
			}, {});
		}, false);
		placeholder.appendChild(closeButton);
		placeholder.appendChild(up);
		placeholder.appendChild(fileInput);
		report.imageList.appendChild(placeholder);
	};
	report.newPlaceholder();
	report.submitBtn.addEventListener('tap', function(event) {
		if(report.question.value == '') {
			return mui.toast('请输入问题名称');
		}
		if(report.question.value.length > 20) {
			return mui.toast('问题名称超长,请重新填写~')
		}
		
		if(report.unitPicker.innerText == '选择机组...') {
			return mui.toast('请选择机组');
		}
			if(report.wrokshopPicker.innerText == '选择厂房...') {
			return mui.toast('请选择厂房');
		}
				if(report.eleration.value == '') {
			return mui.toast('请选择标高');
		}
					if(report.roomno.value == '') {
			return mui.toast('请选择房间号');
		}
								if(report.responsibleDeptPicker.innerText == '选择责任部门...') {
			return mui.toast('请选择责任部门');
		}
											if(report.responsibleTeamPicker.innerText == '选择责任班组...') {
			return mui.toast('请选择责任班组');
		}
													if(report.description.value == '') {
			return mui.toast('请填写问题描述');
		}
				
				var filesArr = report.files;
//				logAlert('filesArr.length:'+filesArr.length);
				if(filesArr.length == 0) {
			return mui.toast('请添加问题描述照片');
		}
													

		//判断网络连接
		if(mui.os.plus){
		if(plus.networkinfo.getCurrentType() == plus.networkinfo.CONNECTION_NONE) {
            return mui.toast("连接网络失败，请稍后再试");
        }
		}
		
//		var param = new FormData() ;
//  	 param.append('problemTitle', report.question.value);
// param.append('unit', report.unitPicker.innerText);
//  param.append('wrokshop', report.wrokshopPicker.innerText);
// param.append('eleration', report.eleration.value);
// param.append('roomno', report.roomno.value);
// param.append('description', report.description.value);
// param.append('responsibleDeptId', JSON.stringify(report.responsibleDeptPicker.value));
//  param.append('responsibleTeamId', JSON.stringify(report.responsibleTeamPicker.value));
//     mui.alert("file！187" + param);
//     	console.log("请求成功！file！187" + JSON.stringify(param));
   
//   	mui.alert("图片数组！filesArr！119" + JSON.stringify(filesArr));
//   	mui.alert("请求成功！112" + JSON.stringify(param));
// var filesSendArr = [];
//    var fileInputArray2 = report.getFileInputArray();
////    mui.alert("filesArr"+JSON.stringify(filesArr));
// 
//        for (var i=0; i < filesArr.length ; i++) {
// 	var item = filesArr[i];
//
//        if (item.path) {
//        	var images=new Image(); 
//          images.src=item.path; 
//          var imgData=getBase64Image(images); 
////        var fileNew = {uri: item.path, type: 'multipart/form-data', name: item.name};   //这里的key(uri和type和name)不能改变,
//        param.append("file",imgData);   //这里的files就是后台需要的key
////			mui.alert(item.path);
//     };
// };
// for (var i=0; i < filesArr.length ; i++) {
// 	var item = filesArr[i];
//
//        if (item.path) {
//        var fileNew = {uri: item.path, type: 'multipart/form-data', name: item.name};   //这里的key(uri和type和name)不能改变,
//        param.append("file",fileNew);   //这里的files就是后台需要的key
////			mui.alert(item.path);
//     };
// };
		

// param.append("file",filesArr);


// mui.alert("file！188" + JSON.stringify(param));
 	
		
//	uploadFile('/hse/create', filesSendArr, success, error);
//mui.alert(JSON.stringify(param));
//uploadFile('/hse/createFile', param, success, error);
	

			
		var user = getCurrentUser();
//		mui.alert(user.id.toString());
		report.send(mui.extend({}, report.deviceInfo, {
//			content: report.question.value,
			loginId:user.id.toString(),
			problemTitle:report.question.value,
			images: report.files,
			unit: report.unitPicker.innerText,
			wrokshop:report.wrokshopPicker.innerText,
			eleration:report.eleration.value,
			roomno:report.roomno.value,
			description:report.description.value,
			responsibleDeptId:report.responsibleDeptPicker.value,
			responsibleTeamId:report.responsibleTeamPicker.value
		}))
	}, false);

		function success(data, param, dataStringify) {
				console.log("请求成功！112" + JSON.stringify(param));
				mui.alert("请求成功！112" + JSON.stringify(param));
			};

				function error(info) {
//					console.log("请求失败！" + info);
					mui.alert("请求失败！" + info);
				};
	
	report.send = function(content) {
//			var user = getCurrentUser();
//			mui.alert(user.id);
		var server= httpDomain +"/hse/create";
		report.uploader = plus.uploader.createUpload(server, {
			method: 'POST'
		}, function(upload, status) {
			plus.nativeUI.closeWaiting()
			console.log("upload cb:"+upload.responseText);

			report.clearForm();
			mui.back();
		
			if(status==200){
				var data = JSON.parse(upload.responseText);
//				logAlert("上传成功："+upload.responseText);

				//上传成功，重置表单
				if (data.ret === 0 && data.desc === 'Success') {
					mui.toast('反馈成功~')
					console.log("upload success");
//					report.clearForm();
				}
			}else{
				console.log("upload fail");
//							logAlert("上传失败："+status);
							mui.alert("上传失败："+status);

			}
			
		});
		//添加上传数据
		mui.each(content, function(index, element) {
			if (index !== 'images') {
//				mui.alert("addData:"+index+","+element);
//				console.log(index);
				report.uploader.addData(index, element)
			} 
		});
		//添加上传文件
		mui.each(report.files, function(index, element) {
			var f = report.files[index];
			var fileNum = index+ 1;
//			logAlert(fileNum);
//			logAlert("addFile"+fileNum+":"+JSON.stringify(f));
			report.uploader.addFile(f.path, {
//				key: f.name
				key: 'file'+fileNum
			});
		});
		
//		mui.alert(JSON.stringify(report.files)); 
		//开始上传任务
		report.uploader.start();
		
		plus.nativeUI.showWaiting();
	};

})();