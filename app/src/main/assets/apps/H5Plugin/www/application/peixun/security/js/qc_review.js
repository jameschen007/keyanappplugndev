/*!
 * ======================================================
 * report Template For MUI (http://dev.dcloud.net.cn/mui)
 * =======================================================
 * @version:1.0.0
 * @author:cuihongbao@dcloud.io
 */
(function() {
	var index = 1;
	var size = null;
	var imageIndexIdNum = 0;
	var starIndex = 0;
	var review = {
		
		imageList: document.getElementById('image-list'),
				submitBtn: document.getElementById('submit'),
	};

	review.files = [];
	review.uploader = null;
	review.deviceInfo = null;
	mui.plusReady(function() {
		//		//设备信息，无需修改
		//		report.deviceInfo = {
		//			appid: plus.runtime.appid, 
		//			imei: plus.device.imei, //设备标识
		//			images: report.files, //图片文件
		//			p: mui.os.android ? 'a' : 'i', //平台类型，i表示iOS平台，a表示Android平台。
		//			md: plus.device.model, //设备型号
		//			app_version: plus.runtime.version,
		//			plus_version: plus.runtime.innerVersion, //基座版本号
		//			os:  mui.os.version,
		//			net: ''+plus.networkinfo.getCurrentType()
		//		}
	});
	/**
	 *提交成功之后，恢复表单项 
	 */
	review.clearForm = function() {
	
		review.imageList.innerHTML = '';
		review.newPlaceholder();
		review.files = [];
		index = 0;
		size = 0;
		imageIndexIdNum = 0;
		starIndex = 0;

	};
	review.getFileInputArray = function() {
		return [].slice.call(review.imageList.querySelectorAll('.file'));
	};
	review.addFile = function(path) {
		review.files.push({
			name: "images" + index,
			path: path,
			id: "img-" + index
		});
		index++;
	};
	/**
	 * 初始化图片域占位
	 */
	review.newPlaceholder = function() {
		var fileInputArray = review.getFileInputArray();
		if(fileInputArray &&
			fileInputArray.length > 0 &&
			fileInputArray[fileInputArray.length - 1].parentNode.classList.contains('space')) {
			return;
		};
		imageIndexIdNum++;
		var placeholder = document.createElement('div');
		placeholder.setAttribute('class', 'image-item space');
		var up = document.createElement("div");
		up.setAttribute('class', 'image-up')
		//删除图片
		var closeButton = document.createElement('div');
		closeButton.setAttribute('class', 'image-close');
		closeButton.innerHTML = 'X';
		closeButton.id = "img-" + index;
		//小X的点击事件
		closeButton.addEventListener('tap', function(event) {
			setTimeout(function() {
				for(var temp = 0; temp < review.files.length; temp++) {
					if(review.files[temp].id == closeButton.id) {
						review.files.splice(temp, 1);
					}
				}
				review.imageList.removeChild(placeholder);
			}, 0);
			return false;
		}, false);

		//
		var fileInput = document.createElement('div');
		fileInput.setAttribute('class', 'file');
		fileInput.setAttribute('id', 'image-' + imageIndexIdNum);
		fileInput.addEventListener('tap', function(event) {
			var self = this;
			var index = (this.id).substr(-1);

			plus.gallery.pick(function(e) {
				//				console.log("event:"+e);
				var name = e.substr(e.lastIndexOf('/') + 1);
				console.log("name:" + name);

				plus.zip.compressImage({
					src: e,
					dst: '_doc/' + name,
					overwrite: true,
					quality: 50
				}, function(zip) {
					size += zip.size
					console.log("filesize:" + zip.size + ",totalsize:" + size);
					if(size > (10 * 1024 * 1024)) {
						return mui.toast('文件超大,请重新选择~');
					}
					if(!self.parentNode.classList.contains('space')) { //已有图片
						review.files.splice(index - 1, 1, {
							name: "images" + index,
							path: e
						});
					} else { //加号
						placeholder.classList.remove('space');
						review.addFile(zip.target);
						review.newPlaceholder();
					}
					up.classList.remove('image-up');
					placeholder.style.backgroundImage = 'url(' + zip.target + ')';
				}, function(zipe) {
					mui.toast('压缩失败！')
				});

			}, function(e) {
				mui.toast(e.message);
			}, {});
		}, false);
		placeholder.appendChild(closeButton);
		placeholder.appendChild(up);
		placeholder.appendChild(fileInput);
		review.imageList.appendChild(placeholder);
	};
	review.newPlaceholder();
	review.submitBtn.addEventListener('tap', function(event) {
		if(review.question.value == '') {
			return mui.toast('请输入问题名称');
		}
		if(review.question.value.length > 20) {
			return mui.toast('问题名称超长,请重新填写~')
		}
		//判断网络连接
		if(plus.networkinfo.getCurrentType() == plus.networkinfo.CONNECTION_NONE) {
			return mui.toast("连接网络失败，请稍后再试");
		}
		review.send(mui.extend({}, review.deviceInfo, {
			content: review.question.value,
			images: review.files,
			score: '' + starIndex
		}))
	}, false)
	review.send = function(content) {
		review.uploader = plus.uploader.createUpload(url, {
			method: 'POST'
		}, function(upload, status) {
			//			plus.nativeUI.closeWaiting()
			console.log("upload cb:" + upload.responseText);
			if(status == 200) {
				var data = JSON.parse(upload.responseText);
				//上传成功，重置表单
				if(data.ret === 0 && data.desc === 'Success') {
										mui.toast('提交成功~')
					console.log("upload success");
										review.clearForm();
				}
			} else {
				console.log("upload fail");
			}

		});
		//添加上传数据
		mui.each(content, function(index, element) {
			if(index !== 'images') {
				console.log("addData:" + index + "," + element);
				//				console.log(index);
				review.uploader.addData(index, element)
			}
		});
		//添加上传文件
		mui.each(review.files, function(index, element) {
			var f = review.files[index];
			console.log("addFile:" + JSON.stringify(f));
			review.uploader.addFile(f.path, {
				key: f.name
			});
		});
		//开始上传任务
		review.uploader.start();
		mui.alert("感谢反馈，点击确定关闭", "问题反馈", "确定", function() {
			review.clearForm();
			mui.back();
		});
		//		plus.nativeUI.showWaiting();
	};

})();