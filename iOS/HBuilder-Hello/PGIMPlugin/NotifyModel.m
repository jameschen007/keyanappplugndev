//
//  NotifyModel.m
//  HBuilder
//
//  Created by 江源泉 on 2018/3/7.
//  Copyright © 2018年 DCloud. All rights reserved.
//

#import "NotifyModel.h"

@implementation NotifyModel
//static NotifyModel *_sharedNotifyModel = nil;
//+ (NotifyModel *)sharedNotifyModel{
//    @synchronized(_sharedNotifyModel)
//    {
//        if (_sharedNotifyModel == nil) {
//            _sharedNotifyModel = [[NotifyModel alloc] init];
//        }
//        return _sharedNotifyModel;
//    }
//
//}
-(instancetype)init{
    if (self = [super init]) {
        
    }
    return self;
}
#pragma mark - NSCoding协议方法 (一定要实现)

//当进行归档操作的时候就会调用该方法

//在该方法中要写清楚要存储对象的哪些属性

- (void)encodeWithCoder:(NSCoder *)aCoder

{
    
    NSLog(@"调用了encodeWithCoder方法");
    
    [aCoder encodeObject:_callbackId forKey:@"callbackId"];
    

    
    [aCoder encodeObject:_action forKey:@"action"];

    
}

//当进行解档操作的时候就会调用该方法

//在该方法中要写清楚要提取对象的哪些属性

- (id)initWithCoder:(NSCoder *)aDecoder

{
    
    NSLog(@"调用了initWithCoder方法");
    
    if (self = [super init]) {
        
        self.callbackId = [aDecoder decodeObjectForKey:@"callbackId"];

        
        self.action = [aDecoder decodeObjectForKey:@"action"];
 
        
    }
    
    return self;
    
}
#pragma mark - NSCoping
- (id)copyWithZone:(NSZone *)zone {
    NotifyModel *copyModel = [[[self class] allocWithZone:zone] init];
    copyModel.callbackId = [self.callbackId copyWithZone:zone];
    copyModel.action = [self.action copyWithZone:zone];
    
    return copyModel;
}

@end
