package com.example.H5PlusPlugin;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.dcloud.common.DHInterface.IWebview;
import io.dcloud.common.DHInterface.StandardFeature;
import io.dcloud.common.util.JSUtil;

import static io.dcloud.common.constant.DOMException.toJSON;


/**
 * 5+ SDK 扩展插件示例
 * 5+ 扩扎插件在使用时需要以下两个地方进行配置
 * 		1  WebApp的mainfest.json文件的permissions节点下添加JS标识
 * 		2  assets/data/properties.xml文件添加JS标识和原生类的对应关系
 * 本插件对应的JS文件在 assets/apps/H5Plugin/js/test.js
 * 本插件对应的使用的HTML assest/apps/H5plugin/index.html
 *
 * 更详细说明请参考文档http://ask.dcloud.net.cn/article/66
 * **/
public class FingerPlugin extends StandardFeature
{

    public void onStart(Context pContext, Bundle pSavedInstanceState, String[] pRuntimeArgs) {

        /**
         * 如果需要在应用启动时进行初始化，可以继承这个方法，并在properties.xml文件的service节点添加扩展插件的注册即可触发onStart方法
         * */
    }

    /**
     * 检查系统是否支持指纹识别
     * @param pWebview
     * @param array
     */
    public void checkFingerPrint(IWebview pWebview, JSONArray array){
        String callbackId  = array.optString(0);
        FingerprintUtil.ResultObj res = FingerprintUtil.checkFingerPrint(pWebview.getContext());
        if(res.getCode()==FingerprintUtil.SUCCESS){
            String msg = toJSON(res.getCode(), res.getMsg());
            Log.d("haiji", "FingerPrint " + res.getCode() + "  " + res.getMsg());
            JSUtil.execCallback(pWebview,callbackId,msg, JSUtil.OK, true,false);//成功回调
        }else{
            Log.d("haiji", "FingerPrint "+res.getMsg());
            String error = toJSON(res.getCode(), res.getMsg());
            JSUtil.execCallback(pWebview,callbackId,error, JSUtil.ERROR,true, false);//失败回调
        }
    }

    /**
     * 开始进行指纹识别
     * @param pWebview
     * @param array
     */
    public void callFingerPrint(final IWebview pWebview, JSONArray array){
        final String callbackId  = array.optString(0);
//        //开始指纹识别
//        FingerprintUtil.callFingerPrint(new FingerprintUtil.OnCallBackListenr() {
//            @Override
//            public void onSupportFailed(FingerprintUtil.ResultObj code) {
//                String error = toJSON(code.getCode(), code.getMsg());
//                Log.d("haiji", "FingerPrint "+code.getMsg());
//                JSUtil.execCallback(pWebview, callbackId, error, JSUtil.ERROR, true,false);//失败回调
////                JsObjectCommonMethod.showToast("当前设备不支持指纹");
//            }
//
//            @Override
//            public void onInsecurity(FingerprintUtil.ResultObj code) {
//                String error = toJSON(code.getCode(), code.getMsg());
//                Log.d("haiji", "FingerPrint "+code.getMsg());
//                JSUtil.execCallback(pWebview, callbackId, error, JSUtil.ERROR, true,false);//失败回调
////                JsObjectCommonMethod.showToast("当前设备没有设置密码保护");
//            }
//
//            @Override
//            public void onEnrollFailed(FingerprintUtil.ResultObj code) {
//                String error = toJSON(code.getCode(), code.getMsg());
//                Log.d("haiji", "FingerPrint "+code.getMsg());
//                JSUtil.execCallback(pWebview, callbackId, error, JSUtil.ERROR,true, false);//失败回调
//            }
//
//            @Override
//            public void onAuthenticationStart(FingerprintUtil.ResultObj code) {
//                String error = toJSON(code.getCode(), code.getMsg());
//                Log.d("haiji", "FingerPrint "+code.getMsg());
//                JSUtil.execCallback(pWebview, callbackId, error, JSUtil.OK,true, true);//成功回调
//            }
//
//            @Override
//            public void onAuthenticationError(int errMsgId, CharSequence errString) {
//                String error = toJSON(errMsgId, errString.toString());
//                Log.d("haiji", "FingerPrint "+error.toString());
//                JSUtil.execCallback(pWebview, callbackId, error, JSUtil.ERROR, true,false);//失败回调
//            }
//
//            @Override
//            public void onAuthenticationFailed(FingerprintUtil.ResultObj code) {
//                String error = toJSON(code.getCode(), code.getMsg());
//                Log.d("haiji", "FingerPrint "+error);
//                JSUtil.execCallback(pWebview, callbackId, error, JSUtil.ERROR, true,false);//失败回调
//            }
//
//            @Override
//            public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
//                String error = toJSON(helpMsgId, helpString.toString());
//                Log.d("haiji", "FingerPrint "+error);
//                JSUtil.execCallback(pWebview, callbackId, error, JSUtil.ERROR,true, false);//失败回调
//            }
//
//
//        },pWebview.getContext());
    }

    /**
     * 取消指纹识别
     * @param pWebview
     * @param array
     */
    public void calcelFingerPrint(final IWebview pWebview, JSONArray array){
        Log.d("haiji", "FingerPrint 取消指纹识别");
        FingerprintUtil.cancelFingerPrint();
    }


}
