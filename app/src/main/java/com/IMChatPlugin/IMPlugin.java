package com.IMChatPlugin;

import android.annotation.TargetApi;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.util.Log;

import com.tencent.imsdk.IMCoreWrapper;
import com.tencent.imsdk.TIMCallBack;
import com.tencent.imsdk.TIMConnListener;
import com.tencent.imsdk.TIMConversation;
import com.tencent.imsdk.TIMConversationType;
import com.tencent.imsdk.TIMElem;
import com.tencent.imsdk.TIMElemType;
import com.tencent.imsdk.TIMGroupEventListener;
import com.tencent.imsdk.TIMGroupManager;
import com.tencent.imsdk.TIMGroupMemberInfo;
import com.tencent.imsdk.TIMGroupMemberRoleType;
import com.tencent.imsdk.TIMGroupReceiveMessageOpt;
import com.tencent.imsdk.TIMGroupTipsElem;
import com.tencent.imsdk.TIMImageElem;
import com.tencent.imsdk.TIMLogLevel;
import com.tencent.imsdk.TIMManager;
import com.tencent.imsdk.TIMMessage;
import com.tencent.imsdk.TIMMessageListener;
import com.tencent.imsdk.TIMMessageStatus;
import com.tencent.imsdk.TIMOfflinePushListener;
import com.tencent.imsdk.TIMOfflinePushNotification;
import com.tencent.imsdk.TIMRefreshListener;
import com.tencent.imsdk.TIMSNSChangeInfo;
import com.tencent.imsdk.TIMSdkConfig;
import com.tencent.imsdk.TIMSoundElem;
import com.tencent.imsdk.TIMTextElem;
import com.tencent.imsdk.TIMUserConfig;
import com.tencent.imsdk.TIMUserProfile;
import com.tencent.imsdk.TIMUserStatusListener;
import com.tencent.imsdk.TIMValueCallBack;
import com.tencent.imsdk.ext.group.TIMGroupAssistant;
import com.tencent.imsdk.ext.group.TIMGroupAssistantListener;
import com.tencent.imsdk.ext.group.TIMGroupBaseInfo;
import com.tencent.imsdk.ext.group.TIMGroupCacheInfo;
import com.tencent.imsdk.ext.group.TIMGroupManagerExt;
import com.tencent.imsdk.ext.group.TIMGroupMemberResult;
import com.tencent.imsdk.ext.group.TIMUserConfigGroupExt;
import com.tencent.imsdk.ext.message.TIMConversationExt;
import com.tencent.imsdk.ext.message.TIMManagerExt;
import com.tencent.imsdk.ext.message.TIMMessageExt;
import com.tencent.imsdk.ext.message.TIMMessageLocator;
import com.tencent.imsdk.ext.message.TIMUserConfigMsgExt;
import com.tencent.imsdk.ext.sns.TIMFriendGroup;
import com.tencent.imsdk.ext.sns.TIMFriendshipProxyListener;
import com.tencent.imsdk.ext.sns.TIMUserConfigSnsExt;
import com.tencent.qalsdk.sdk.MsfSdkUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import io.dcloud.HDApplication;
import io.dcloud.common.DHInterface.IWebview;
import io.dcloud.common.DHInterface.StandardFeature;
import io.dcloud.common.util.JSUtil;
import tencent.tls.platform.TLSAccountHelper;
import tencent.tls.platform.TLSLoginHelper;

/**
 * Created by mac on 17/12/12.
 */

public class IMPlugin extends StandardFeature {

     private static  final String C2C = "c2c";

     private static  final String GROUP = "group";

     private static  final String tag = "IMPlugin";

     private static  Context context;

     private  static  final  String ACTION_NEW_MSG = "action_new_msg";

    private  static  final  String ACTION_UPDATE_CONVERSATION = "action_update_conversation";

    public void onStart(Context pContext, Bundle pSavedInstanceState, String[] pRuntimeArgs) {

        /**
         * 如果需要在应用启动时进行初始化，可以继承这个方法，并在properties.xml文件的service节点添加扩展插件的注册即可触发onStart方法
         * */
    }

    void saveLoginInfo(String identifier, String userSig){
        SharedPreferences sharedPreferences = getIMPref();
        sharedPreferences.edit().putString("identifier",identifier).putString("userSig",userSig).commit();

    }
    SharedPreferences getIMPref(){
        return mApplicationContext.getSharedPreferences("IMUserInfo",Context.MODE_PRIVATE);
    }
    void clearLoginInfo(){
        getIMPref().edit().clear().commit();

    }
    boolean isLogin(){
        return  TIMManager.getInstance().getLoginUser()!=null;
    }

    void relogin(final TIMCallBack timCallBack){

        String identifier = getIMPref().getString("identifier",null);
        String userSig = getIMPref().getString("userSig",null);

        TIMManager.getInstance().login(identifier, userSig, new TIMCallBack() {
            @Override
            public void onError(int code, String desc) {
                //错误码code和错误描述desc，可用于定位请求失败原因
                //错误码code列表请参见错误码表

                Log.d(tag, "relogin failed. code: " + code + " errmsg: " + desc);
                timCallBack.onError(code,desc);
            }

            @Override
            public void onSuccess() {
                Log.d(tag, "relogin succ");
                timCallBack.onSuccess();
                // 调用方法将原生代码的执行结果返回给js层并触发相应的JS层回调函数
                        }
        });

    }


    public void callIMCmd(IWebview pWebview, JSONArray array)
    {
        // 原生代码中获取JS层传递的参数，
        // 参数的获取顺序与JS层传递的顺序一致
        String CallBackID = array.optString(0);

        try {
            JSONObject cmdObject = new JSONObject(array.optString(1));
            String action = cmdObject.optString("action");
            Log.i(tag, "action=="+action+";CallBackID=="+CallBackID);

            if("cmd_init".equals(action)){
                initIM(pWebview,CallBackID,cmdObject);
            }else if("cmd_login".equals(action)){
                login(pWebview,CallBackID,cmdObject);
            }else if("cmd_logout".equals(action)){
                logout(pWebview,CallBackID,cmdObject);
            }else if("cmd_conversations".equals(action)){
                getConversationList(pWebview,CallBackID,cmdObject);
            }else if("cmd_send_event".equals(action)){
                sendEvent(pWebview,CallBackID,cmdObject);
            }else if("cmd_open_push".equals(action)){
                openPush(pWebview,CallBackID,cmdObject);
            }else {
                final  IWebview iWebview = pWebview;
                final  JSONObject iCmdObject = cmdObject;
                final  String iCallBackID = CallBackID;
                final  String iAction = action;
                //check login
                if(isLogin()){
                    callIMCmdOnLogin(iWebview,iCmdObject,iCallBackID,iAction);
                }else{
                    relogin(new TIMCallBack() {
                        @Override
                        public void onError(int i, String s) {
                            callIMCmdOnLogin(iWebview,iCmdObject,iCallBackID,iAction);
                        }

                        @Override
                        public void onSuccess() {
                            callIMCmdOnLogin(iWebview,iCmdObject,iCallBackID,iAction);
                        }
                    });
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(tag, "exception =="+Log.getStackTraceString(e));
        }


    }

    void callIMCmdOnLogin(IWebview pWebview, JSONObject cmdObject,String CallBackID,String action){
        if("cmd_create_group".equals(action)){
            createGroup(pWebview,CallBackID,cmdObject);

        }else if("cmd_send_msg".equals(action)){
            sendMessage(pWebview,CallBackID,cmdObject);
        }else if("cmd_msg_list".equals(action)){
            getLocalMessageList(pWebview,CallBackID,cmdObject);
        }else if("cmd_sound_file".equals(action)){
            getVoiceFile(pWebview,CallBackID,cmdObject);
        }else if("cmd_quit_group".equals(action)){
            quitGroup(pWebview,CallBackID,cmdObject);
        }else if("cmd_delete_group_member".equals(action)){
            deleteGroupMember(pWebview,CallBackID,cmdObject);
        }else if("cmd_add_group_member".equals(action)){
            addGroupMember(pWebview,CallBackID,cmdObject);
        }else if("cmd_get_group_member".equals(action)){
            getGroupMember(pWebview,CallBackID,cmdObject);
        }else if("cmd_update_group_name".equals(action)){
            updateGroupName(pWebview,CallBackID,cmdObject);
        }else if("cmd_read_msg".equals(action)){
            setReadMsg(pWebview,CallBackID,cmdObject);
        }else if("cmd_resend_msg".equals(action)){
            resendMessage(pWebview,CallBackID,cmdObject);
        }
    }


    public String callIMCmdSync(IWebview pWebview, JSONArray array)
    {
        // 原生代码中获取JS层传递的参数，
        // 参数的获取顺序与JS层传递的顺序一致

        try {
            JSONObject cmdObject = new JSONObject(array.optString(0));
            String action = cmdObject.optString("action");
            Log.i(tag, "sync action=="+action);
             if("sync_cmd_login_status".equals(action)){
                //JSONObject jsonObject =
                //connected ,,
                return JSUtil.wrapJsVar( isLogin());
            }else if("sync_cmd_delete_msg".equals(action)){
                 return deleteMessage(pWebview,cmdObject);

             }else if("sync_cmd_push_enable".equals(action)){
                 return pushEnable(pWebview,cmdObject);

             }

        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(tag, "exception =="+Log.getStackTraceString(e));
        }
        return null;


    }

    public void registerIMListener(IWebview pWebview, JSONArray array)
    {
        // 原生代码中获取JS层传递的参数，
        // 参数的获取顺序与JS层传递的顺序一致
        String CallBackID = array.optString(0);

        try {
            JSONObject cmdObject = new JSONObject(array.optString(1));
            String action = cmdObject.optString("action");
            Log.i(tag, "register action=="+action+";CallBackID="+CallBackID);
            addListeners(pWebview,CallBackID,action);


        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(tag, "exception =="+Log.getStackTraceString(e));
        }

    }

    public  void sendEvent(final IWebview pWebview,final String CallBackID,JSONObject cmdObject){
        String event = cmdObject.optString("event");
        try {
            JSONObject info = cmdObject.optJSONObject("data");
            JSONObject data = createListenerData(info);
            notifyListeners(event,data);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }



    public void unregisterIMListener(IWebview pWebview, JSONArray array)
    {
        try {
            JSONObject cmdObject = new JSONObject(array.optString(1));
            String action = cmdObject.optString("action");

            String callbackId = cmdObject.optString("CallBackID");
            Log.i(tag, "unregister action=="+action+";callbackId=="+callbackId);
            removeListeners(callbackId);
            JSUtil.execCallback(pWebview, callbackId, cmdObject, JSUtil.OK, false);

        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(tag, "exception =="+Log.getStackTraceString(e));
        }

    }




    static  final  class NotifyListener{

         public  IWebview pWebview;
         public  String CallBackID;
         public  String action;

        public NotifyListener(IWebview pWebview,String callbackId,String action){
            this.pWebview = pWebview;
            this.CallBackID = callbackId;
            this.action = action;

        }

    }


    static  final List<NotifyListener> listners = new ArrayList<NotifyListener>();

    static  synchronized void notifyListeners(String action,JSONObject data){

        for (int i = listners.size()-1; i>=0; i--) {
            NotifyListener notifyListener = listners.get(i);
            if(notifyListener.action.equals(action)){
                Log.i(tag,"notify the listener action:"+action);
                if(notifyListener.pWebview!=null){
                    JSUtil.execCallback(notifyListener.pWebview, notifyListener.CallBackID, data, JSUtil.OK, true);
                }else{
                    Log.i(tag,"notify the listener remove the notify listener "+action);
                    listners.remove(i);
                }

            }
        }


    }

    static synchronized void removeListeners(String callbackId){

        for (int i = listners.size() -1 ; i>=0; i--) {
            NotifyListener notifyListener = listners.get(i);
            if(notifyListener.CallBackID.equals(callbackId)){
               // JSUtil.execCallback(notifyListener.pWebview, notifyListener.CallBackID, data, JSUtil.OK, false);
                listners.remove(i);
                Log.i(tag,"remove the listener =="+callbackId+"; action="+notifyListener.action);
                break;
            }
        }

    }

    static synchronized void addListeners(IWebview pWebview,String callbackId,String action){
         NotifyListener notifyListener = new NotifyListener(pWebview,callbackId,action);
        for (int i = 0; i < listners.size(); i++) {
            NotifyListener item = listners.get(i);
            if(item.CallBackID.equals(callbackId)){
                Log.i(tag,"already exsit listeners==="+item.action);
                return;
            }
        }
        Log.i(tag,"add listeners==="+action+";callbackId="+callbackId);

        listners.add(notifyListener);

    }



    private void initIM(final IWebview pWebview,final String CallBackID,JSONObject cmdObject){
           // 走sso
           init(pWebview.getContext().getApplicationContext());
           final JSONArray newArray = new JSONArray();
           JSUtil.execCallback(pWebview, CallBackID, newArray, JSUtil.OK, false);
    }


    private  String deleteMessage(final IWebview pWebview,JSONObject cmdObject) {
        TIMMessage msg = findCursorMessage(cmdObject);
        if(msg!=null){
            TIMMessageExt timMessageExt = new TIMMessageExt(msg);
            return JSUtil.wrapJsVar(timMessageExt.remove());
        }

        return  JSUtil.wrapJsVar(false);

    }
        private  void resendMessage(final IWebview pWebview,final String CallBackID,JSONObject cmdObject){
        //构造一条消息
        TIMMessage msg = findCursorMessage(cmdObject);
        TIMConversation conversation = getConversation(cmdObject);
//发送消息
        conversation.sendMessage(msg, new TIMValueCallBack<TIMMessage>() {//发送消息回调
            @Override
            public void onError(int code, String desc) {//发送消息失败
                //错误码code和错误描述desc，可用于定位请求失败原因
                //错误码code含义请参见错误码表
                Log.d(tag, "send message failed. code: " + code + " errmsg: " + desc);
            }

            @Override
            public void onSuccess(TIMMessage msg) {//发送消息成功

                JSONObject msgObj = null;
                try {
                    msgObj = parseMsg(msg);
                    Log.e(tag, "reSendMsg ok =="+msgObj);
                    JSUtil.execCallback(pWebview, CallBackID, msgObj, JSUtil.OK, false);
                } catch (JSONException e) {
                    e.printStackTrace();

                }


            }
        });
    }

    private  void sendMessage(final IWebview pWebview,final String CallBackID,JSONObject cmdObject){
        //构造一条消息
        TIMMessage msg = new TIMMessage();

//添加语音
        TIMElem elem = null;
        String msgType = cmdObject.optString("msgType");
        Log.d(tag, "addMessage msgType="+msgType);
        if("voice".equals(msgType)){
            elem = new TIMSoundElem();
            TIMSoundElem soundElem = (TIMSoundElem) elem;
            String filePath = cmdObject.optString("filePath");
            soundElem.setPath(filePath); //填写语音文件路径
            int duration = cmdObject.optInt("duration");
            soundElem.setDuration(duration);  //填写语音时长
            Log.i(tag,"voice filePath=="+filePath+"; duration=="+duration);
        }else if("text".equals(msgType)){
            elem = new TIMTextElem();
            TIMTextElem textElem = (TIMTextElem) elem;
            String content = cmdObject.optString("content");
            textElem.setText(content);
            Log.i(tag,"content=="+content);
        }else if("image".equals(msgType)){
            elem = new TIMImageElem();
            TIMImageElem imageElem = (TIMImageElem) elem;
            String filePath = cmdObject.optString("filePath");
            if (filePath.startsWith("file://")){
                filePath =filePath.substring("file://".length());
            }
            imageElem.setPath(filePath); //填写文件路径

            Log.i(tag,"image filePath=="+filePath);
        }

//将elem添加到消息
        if(msg.addElement(elem) != 0) {
            Log.d(tag, "addElement failed");
            return;
        }
        TIMConversation conversation = getConversation(cmdObject);
//发送消息
        conversation.sendMessage(msg, new TIMValueCallBack<TIMMessage>() {//发送消息回调
            @Override
            public void onError(int code, String desc) {//发送消息失败
                //错误码code和错误描述desc，可用于定位请求失败原因
                //错误码code含义请参见错误码表
                Log.d(tag, "send message failed. code: " + code + " errmsg: " + desc);
            }

            @Override
            public void onSuccess(TIMMessage msg) {//发送消息成功

                JSONObject msgObj = null;
                try {
                    msgObj = parseMsg(msg);
                    Log.e(tag, "SendMsg ok =="+msgObj);
                    JSUtil.execCallback(pWebview, CallBackID, msgObj, JSUtil.OK, false);
                } catch (JSONException e) {
                    e.printStackTrace();

                }


            }
        });
    }

    private static List<TIMGroupCacheInfo> groupCacheInfos = null;
    private List<TIMGroupCacheInfo> getGroupsInfo(){

        List<TIMGroupCacheInfo> groupInfos = TIMGroupAssistant.getInstance().getGroups(null);
        if (groupInfos == null) return new ArrayList<>();
        Log.i(tag, "group info size=="+groupInfos.size());
        //sort
        Collections.sort(groupInfos, new Comparator<TIMGroupCacheInfo>() {
            public int compare(TIMGroupCacheInfo o1, TIMGroupCacheInfo o2) {

                long timeGap =  o2.getGroupInfo().getCreateTime() -  o1.getGroupInfo().getCreateTime();
                if (timeGap > 0) return  1;
                else if (timeGap < 0) return -1;
                return 0;

            }
        });//使用Collections的sort方法，并且重写compare方法
        groupCacheInfos = groupInfos;
        return  groupInfos;
    }


    private void createGroup(final IWebview pWebview,final String CallBackID,JSONObject cmdObject){
        String name = cmdObject.optString("name");
        JSONArray members = cmdObject.optJSONArray("members");
        //创建公开群，且不自定义群ID
        TIMGroupManager.CreateGroupParam param = new TIMGroupManager.CreateGroupParam("Private", name);
//指定群简介
       // param.setIntroduction("hello world");
//指定群公告
       // param.setNotification("welcome to our group");

//添加群成员
        List<TIMGroupMemberInfo> infos = new ArrayList<TIMGroupMemberInfo>();

        for (int i = 0; i < members.length(); i++) {
            TIMGroupMemberInfo member = null;
            try {
                member = new TIMGroupMemberInfo(members.getJSONObject(i).optString("username"));
                member.setRoleType(TIMGroupMemberRoleType.Normal);
                infos.add(member);
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
        param.setMembers(infos);

//设置群自定义字段，需要先到控制台配置相应的key
//        try {
//            param.setCustomInfo("GroupKey1", "wildcat".getBytes("utf-8"));
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }

//创建群组
        TIMGroupManager.getInstance().createGroup(param, new TIMValueCallBack<String>() {
            @Override
            public void onError(int code, String desc) {
                Log.d(tag, "create group failed. code: " + code + " errmsg: " + desc);
                JSONObject error = new JSONObject();
                try {
                    error.put("code",code);
                    error.put("desc",desc);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                JSUtil.execCallback(pWebview, CallBackID, error, JSUtil.ERROR, false);
            }

            @Override
            public void onSuccess(String s) {
                Log.d(tag, "create group succ, groupId:" + s);

                JSUtil.execCallback(pWebview, CallBackID, s, JSUtil.OK, false);
            }
        });
    }


    private  static List<TIMMessage> messageList = new ArrayList<>();


    private static TIMMessage findCursorMessage(JSONObject cmdObject){
        String msgId =cmdObject.optString("msgId");
        long getMsgUniqueId =  cmdObject.optLong("uniqueId");
        long getSeq =  cmdObject.optLong("seq");
        long timestamp =  cmdObject.optLong("timestamp");
        long rand =  cmdObject.optLong("rand");
        boolean isSelf =  cmdObject.optBoolean("self");


        if(msgId == null){
            return null;
        }



        for (int i = 0; i < messageList.size(); i++) {
            TIMMessage timMessage = messageList.get(i);
            //seq, rand, timestamp, isSelf
            if(timMessage.getMsgId().equals(msgId)){
                Log.i(tag,"find message from cache=="+timMessage.getMsgId());
                return timMessage;

            }
        }
        Log.i(tag,"messageList=="+messageList.size());
        return  null;
    }




    private static void addCursorMessage(TIMMessage message){
        for (int i = 0; i < messageList.size(); i++) {
            TIMMessage timMessage = messageList.get(i);
            //seq, rand, timestamp, isSelf
            if(timMessage.getMsgId().equals(message.getMsgId())){
                //update
                Log.i(tag, "update message =="+message.getMsgId());
                messageList.set(i,message);
                return;

            }
        }
        Log.i(tag, "add message =="+message.getMsgId());
        messageList.add(message);
    }

    public enum FileType{
        IMG,
        AUDIO,
        VIDEO,
        FILE,
    }

    private static File cacheDir = null;


    /**
     * 判断外部存储是否可用
     *
     */
    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        Log.e(tag, "ExternalStorage not mounted");
        return false;
    }
    /**
     * 创建临时文件
     *
     * @param type 文件类型
     */
    private static File getTempFile(FileType type){
        try{
            File file = File.createTempFile(type.toString(), null, cacheDir);
            file.deleteOnExit();
            return file;
        }catch (IOException e){
            return null;
        }
    }

    private  void setReadMsg(final IWebview pWebview,final String CallBackID,JSONObject cmdObject){
        String peer = cmdObject.optString("identifier");
        String type = cmdObject.optString("type");
        TIMConversationType conversationType = TIMConversationType.C2C;
        if(type.equals(GROUP)){
            conversationType = TIMConversationType.Group;
        }
        TIMConversation conversation = TIMManager.getInstance().getConversation(
                conversationType,    //会话类型：单聊
                peer);                      //会话对方用户帐号
//获取会话扩展实例
        TIMConversationExt conExt = new TIMConversationExt(conversation);
//将此会话的所有消息标记为已读
        conExt.setReadMessage(null, new TIMCallBack() {
            @Override
            public void onError(int code, String desc) {
                JSUtil.execCallback(pWebview, CallBackID, desc, JSUtil.ERROR, false);
                Log.e(tag, "setReadMessage failed, code: " + code + "|desc: " + desc);
            }

            @Override
            public void onSuccess() {
                Log.d(tag, "setReadMessage succ");
                JSUtil.execCallback(pWebview, CallBackID, "ok", JSUtil.OK, false);
            }
        });
    }


    private  void updateGroupName(final IWebview pWebview,final String CallBackID,JSONObject cmdObject){
        String groupId = cmdObject.optString("identifier");
        String name = cmdObject.optString("name");
        TIMGroupManagerExt.ModifyGroupInfoParam param = new TIMGroupManagerExt.ModifyGroupInfoParam(groupId);
        param.setGroupName(name);
        TIMGroupManagerExt.getInstance().modifyGroupInfo(param, new TIMCallBack() {
            @Override
            public void onError(int code, String desc) {
                Log.e(tag, "modify group info failed, code:" + code +"|desc:" + desc);
                JSUtil.execCallback(pWebview, CallBackID, desc, JSUtil.ERROR, false);
            }

            @Override
            public void onSuccess() {


                Log.e(tag, "modify group info succ");
                JSUtil.execCallback(pWebview, CallBackID, "ok", JSUtil.OK, false);

            }
        });




    }


    private  void getGroupMember(final IWebview pWebview,final String CallBackID,JSONObject cmdObject){
        String groupId = cmdObject.optString("identifier");
        //创建回调
        TIMValueCallBack<List<TIMGroupMemberInfo>> cb = new TIMValueCallBack<List<TIMGroupMemberInfo>> () {
            @Override
            public void onError(int code, String desc) {
                Log.e(tag, "addGroupMember onErr. code: " + code + " errmsg: " + desc);
                JSUtil.execCallback(pWebview, CallBackID, desc, JSUtil.ERROR, false);
            }

            @Override
            public void onSuccess(List<TIMGroupMemberInfo> infoList) {//参数返回群组成员信息
                JSONArray members = new JSONArray();
                for(TIMGroupMemberInfo info : infoList) {
                    Log.d(tag, "user: " + info.getUser() +
                            "join time: " + info.getJoinTime() +
                            "role: " + info.getRole());
                    JSONObject member = new JSONObject();
                    try {
                        member.put("username",info.getUser());

                               String roleName = info.getRole().name();
                               if(roleName.equals("Owner")){
                                   member.put("role","Owner");
                               }else  if(roleName.equals("Admin")){
                                   member.put("role","Admin");
                               }else  if(roleName.equals("Normal")){
                                   member.put("role","Normal");
                               }else  if(roleName.equals("NotMember")){
                                   member.put("role","NotMember");
                               }

                        members.put(member);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                JSUtil.execCallback(pWebview, CallBackID, members, JSUtil.OK, false);

            }
        };

//获取群组成员信息
        TIMGroupManagerExt.getInstance().getGroupMembers(
                groupId, //群组Id
                cb);     //回调
    }


    private  void addGroupMember(final IWebview pWebview,final String CallBackID,JSONObject cmdObject){
        //创建待加入群组的用户列表
        ArrayList list = new ArrayList();


        JSONArray users = cmdObject.optJSONArray("members");
        for (int i = 0; i < users.length(); i++) {
            list.add(users.optJSONObject(i).optString("username"));
        }
        String groupId = cmdObject.optString("identifier");

//回调
        TIMValueCallBack<List<TIMGroupMemberResult>> cb = new TIMValueCallBack<List<TIMGroupMemberResult>>() {
            @Override
            public void onError(int code, String desc) {
                Log.e(tag, "addGroupMember onErr. code: " + code + " errmsg: " + desc);
                JSUtil.execCallback(pWebview, CallBackID, desc, JSUtil.ERROR, false);
            }

            @Override
            public void onSuccess(List<TIMGroupMemberResult> results) { //群组成员操作结果
                for(TIMGroupMemberResult r : results) {
                    Log.d(tag, "result: " + r.getResult()  //操作结果:  0:添加失败；1：添加成功；2：原本是群成员
                            + " user: " + r.getUser());    //用户帐号
                }
                JSONArray delelteInfos = new JSONArray();

                JSUtil.execCallback(pWebview, CallBackID, delelteInfos, JSUtil.OK, false);
            }
        };

//将list中的用户加入群组
        TIMGroupManagerExt.getInstance().inviteGroupMember(
                groupId,   //群组Id
                list,      //待加入群组的用户列表
                cb);       //回调
    }

    private  void deleteGroupMember(final IWebview pWebview,final String CallBackID,JSONObject cmdObject){
        //创建待踢出群组的用户列表
        ArrayList list = new ArrayList();

        JSONArray users = cmdObject.optJSONArray("members");
        for (int i = 0; i < users.length(); i++) {
            list.add(users.optJSONObject(i).optString("username"));
        }
        String groupId = cmdObject.optString("identifier");

        TIMGroupManagerExt.DeleteMemberParam param = new TIMGroupManagerExt.DeleteMemberParam(groupId, list);

        TIMGroupManagerExt.getInstance().deleteGroupMember(param, new TIMValueCallBack<List<TIMGroupMemberResult>>() {
            @Override
            public void onError(int code, String desc) {
                Log.e(tag, "deleteGroupMember onErr. code: " + code + " errmsg: " + desc);
                JSUtil.execCallback(pWebview, CallBackID, desc, JSUtil.ERROR, false);
            }

            @Override
            public void onSuccess(List<TIMGroupMemberResult> results) { //群组成员操作结果
                for(TIMGroupMemberResult r : results) {
                    Log.d(tag, "result: " + r.getResult()  //操作结果:  0：删除失败；1：删除成功；2：不是群组成员
                            + " user: " + r.getUser());    //用户帐号

                }
                JSONArray delelteInfos = new JSONArray();

                JSUtil.execCallback(pWebview, CallBackID, delelteInfos, JSUtil.OK, false);

            }
        });

    }

    private  void quitGroup(final IWebview pWebview,final String CallBackID,JSONObject cmdObject){


        //创建回调
        TIMCallBack cb = new TIMCallBack() {
            @Override
            public void onError(int code, String desc) {
                //错误码code和错误描述desc，可用于定位请求失败原因
                //错误码code含义请参见错误码表
                Log.e(tag, "quit group error:"+code+";desc="+desc);
                JSUtil.execCallback(pWebview, CallBackID, desc, JSUtil.ERROR, false);
            }

            @Override
            public void onSuccess() {
                Log.i(tag, "quit group succ");
                JSUtil.execCallback(pWebview, CallBackID, "OK", JSUtil.OK, false);
            }
        };

        //退出群组
        String groupId = cmdObject.optString("identifier");
        TIMGroupManager.getInstance().quitGroup(
                groupId,  //群组Id
                cb);      //回调
    }

    private void getVoiceFile(final IWebview pWebview,final String CallBackID,JSONObject cmdObject){
        TIMMessage message = findCursorMessage(cmdObject);
        if(message == null){
            Log.i(tag,"not find the message info=="+cmdObject.toString());
            return;
        }
        final TIMSoundElem elem = (TIMSoundElem) message.getElement(0);
        final File tempAudio = getTempFile(FileType.AUDIO);
        elem.getSoundToFile(tempAudio.getAbsolutePath(), new TIMCallBack() {
            @Override
            public void onError(int i, String s) {
                Log.i(tag,"get sound file failed code = "+i+";info="+s);
                JSUtil.execCallback(pWebview, CallBackID, s, JSUtil.ERROR, false);
            }

            @Override
            public void onSuccess() {
                Log.i(tag,"get sound file path = " +tempAudio.getAbsolutePath());

                JSUtil.execCallback(pWebview, CallBackID, tempAudio.getAbsolutePath(), JSUtil.OK, false);
            }
        });


    }
    private static  String getName(TIMGroupMemberInfo info){
        if (info.getNameCard().equals("")){
            return info.getUser();
        }
        return info.getNameCard();
    }

    private  static  String parseGroupTips(JSONObject msgJson, TIMMessage message){
        final TIMGroupTipsElem e = (TIMGroupTipsElem) message.getElement(0);
        StringBuilder stringBuilder = new StringBuilder();
        Iterator<Map.Entry<String,TIMGroupMemberInfo>> iterator = e.getChangedGroupMemberInfo().entrySet().iterator();
        switch (e.getTipsType()){
            case CancelAdmin:
            case SetAdmin:
                return "管理员变更";
            case Join:
                while(iterator.hasNext()){
                    Map.Entry<String,TIMGroupMemberInfo> item = iterator.next();
                    stringBuilder.append("[").append(getName(item.getValue())).append("]");
                    stringBuilder.append(" ");
                }
                return stringBuilder +
                        "加入群";
            case Kick:
                return "["+e.getUserList().get(0)+"]" + "被踢出群";
            case ModifyMemberInfo:
                while(iterator.hasNext()){
                    Map.Entry<String,TIMGroupMemberInfo> item = iterator.next();
                    stringBuilder.append("[").append(getName(item.getValue())).append("]");
                    stringBuilder.append(" ");
                }
                return stringBuilder + "资料变更";
            case Quit:
                return "["+e.getOpUser() +"]"+ "退出群";
            case ModifyGroupInfo:
                return  "群资料变更";
        }
        return "";
    }

    private  static JSONObject parseMsg(TIMMessage msg) throws JSONException{

        JSONObject msgObj = new JSONObject();
        TIMElem elem = msg.getElement(0);
        TIMElemType msgType = elem.getType();
        Log.i(tag, "parse msg: " + msg.timestamp() + " self: " + msg.isSelf());
        Log.d(tag, "elem type: " + msgType.name());
        if (msgType == TIMElemType.Text) {
            TIMTextElem textElem = (TIMTextElem) elem;
            msgObj.put("type", "text");
            msgObj.put("content", textElem.getText());
        } else if (msgType == TIMElemType.Sound) {
            TIMSoundElem voiceElem = (TIMSoundElem) elem;
            msgObj.put("type", "sound");
            msgObj.put("path",voiceElem.getPath());
            msgObj.put("duration",voiceElem.getDuration());
        }else if (msgType == TIMElemType.Image) {
            TIMImageElem imageElem = (TIMImageElem) elem;
            msgObj.put("type", "image");
            msgObj.put("path",imageElem.getPath());
            msgObj.put("content", "file://"+imageElem.getPath());
        }else if(msgType == TIMElemType.GroupTips){
            msgObj.put("type", "tips");
            msgObj.put("content", parseGroupTips(msgObj,msg));
        }

        msgObj.put("msgId", msg.getMsgId());
        msgObj.put("uniqueId", msg.getMsgUniqueId());
        msgObj.put("timestamp", msg.timestamp());
        msgObj.put("rand", msg.getRand());
        msgObj.put("self", msg.isSelf());
        msgObj.put("seq", msg.getSeq());
        msgObj.put("sender", msg.getSender());
        msgObj.put("peer", msg.getConversation().getPeer());
        TIMMessageStatus status = msg.status();
        String msgStatus = "";
        if(TIMMessageStatus.Sending == status){
            msgStatus = "sending";
        }else if(TIMMessageStatus.SendSucc == status){
            msgStatus = "sendSucc";
        }else if(TIMMessageStatus.SendFail == status){
            msgStatus = "sendFail";
        }
        msgObj.put("status", msgStatus);
       // TIMMessageExt msgExt = new TIMMessageExt(msg);


        String conversationType = C2C;
        if(msg.getConversation().getType() == TIMConversationType.Group){
            conversationType = GROUP;
        }
        msgObj.put("conversationType",conversationType );

        addCursorMessage(msg);
        return  msgObj;
    }

    private  void getMessage(TIMConversation conversation,TIMMessage timMessage,int size , TIMValueCallBack<List<TIMMessage>> listTIMValueCallBack){

        final TIMConversationExt tIMConversationExt = new TIMConversationExt(conversation);
        tIMConversationExt.getMessage(size, //获取此会话最近的10条消息
                timMessage, listTIMValueCallBack);//不指定从哪条消息开始获取 - 等同于从最新的消息开始往前);
    }

    private void getLocalMessageList(final IWebview pWebview,final String CallBackID,JSONObject cmdObject){

        final TIMConversation conversation = getConversation(cmdObject);

        int  size = cmdObject.optInt("size");



        TIMMessage timMessage = findCursorMessage(cmdObject);
        

        Log.d(tag,  "find timMessage:"+(timMessage !=null )+ " get message count: " + size);
        //获取此会话的消息
        getMessage(conversation,timMessage,size,new TIMValueCallBack<List<TIMMessage>>() {//回调接口

            @Override
            public void onError(int code, String desc) {//获取消息失败
                //接口返回了错误码code和错误描述desc，可用于定位请求失败原因
                //错误码code含义请参见错误码表
                Log.d(tag, "get message failed. code: " + code + " errmsg: " + desc);
                JSUtil.execCallback(pWebview, CallBackID, new JSONArray(), JSUtil.ERROR, false);
            }

            @Override
            public void onSuccess(List<TIMMessage> msgs) {//获取消息成功

                //遍历取得的消息
                Log.d(tag, "get message succ");

                JSUtil.execCallback(pWebview, CallBackID, parseMsgList(msgs), JSUtil.OK, false);

                //
            }
        });

    }

    private  static JSONArray parseMsgList(List<TIMMessage> msgs){
        JSONArray newArray = new JSONArray();
        Log.i(tag, "TIMMessage list: " + msgs.size());

        try {

            for (TIMMessage msg : msgs) {
                //  lastMsg = msg;
                //可以通过timestamp()获得消息的时间戳, isSelf()是否为自己发送的消息
                JSONObject msgObj = parseMsg(msg);
                newArray.put(msgObj);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i(tag, "msg list: " + newArray.toString());
        return  newArray;

    }

    private  void getConversationList(final IWebview pWebview,final String CallBackID,JSONObject cmdObject){
        String type = cmdObject.optString("type");
        boolean inited = IMCoreWrapper.get().isIMCoreUserInited();
        final List<Integer> indexList = new ArrayList<>();

        if(!inited){
            Log.i(tag, "user not init ok");
            return;
        }
        Log.i(tag, ";IMCoreWrapper.get().isIMCoreUserInited()="+ IMCoreWrapper.get().isIMCoreUserInited());
        List<TIMConversation> conversationList = TIMManagerExt.getInstance().getConversationList();
        if(conversationList == null){
            conversationList = new ArrayList<>();
        }
        Log.i(tag, "get total count: "+conversationList.size());


        List<TIMGroupCacheInfo> groupInfos = null;
        List<TIMConversation> filterConversationList = new ArrayList<>();
        if(type.equals(GROUP) ){


            TIMGroupManagerExt.getInstance().getGroupList(new TIMValueCallBack<List<TIMGroupBaseInfo>>() {
                @Override
                public void onError(int i, String s) {

                }

                @Override
                public void onSuccess(List<TIMGroupBaseInfo> timGroupBaseInfos) {
                    final  int count = timGroupBaseInfos.size();
                    final JSONArray newArray = new JSONArray();


                    for (int i = 0; i < count;i++) {

                        final TIMGroupBaseInfo groupCacheInfo = timGroupBaseInfos.get(i);

                        try{
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("identifier",groupCacheInfo.getGroupId());
                            String  convType = GROUP;
                            jsonObject.put("name",groupCacheInfo.getGroupName());
                            jsonObject.put("type",convType);
                            //获取会话扩展实例
                            TIMConversation con = TIMManager.getInstance().getConversation(TIMConversationType.Group, groupCacheInfo.getGroupId());
                            TIMConversationExt conExt = new TIMConversationExt(con);
//获取会话未读数
                            long num = conExt.getUnreadMessageNum();
                            Log.d(tag, "unread msg num: " + num);
                            jsonObject.put("unread",num);
                            jsonObject.put("timestamp",groupCacheInfo.getSelfInfo().getJoinTime());
                            JSONObject queryJson = new JSONObject();
                            queryJson.put("identifier",groupCacheInfo.getGroupId());
                            queryJson.put("type",GROUP);
                            jsonObject.put("lastmsg",null);
                            newArray.put(jsonObject);
                            TIMConversation conversation =  getConversation(queryJson);
                            getMessage(conversation,null,1,new TIMValueCallBack<List<TIMMessage>>() {//回调接口

                                @Override
                                public void onError(int code, String desc) {//获取消息失败
                                    //接口返回了错误码code和错误描述desc，可用于定位请求失败原因
                                    //错误码code含义请参见错误码表
                                    Log.d(tag, "get message failed. code: " + code + " errmsg: " + desc);
                                    indexList.add(0);
                                    Log.i(tag, indexList.size() + " indexList size: " + newArray.length());
                                    if(indexList.size() == newArray.length()){
                                        Log.i(tag, "get conversation size: " + newArray.toString());

                                        JSUtil.execCallback(pWebview, CallBackID, newArray, JSUtil.OK, false);
                                    }
                                }

                                @Override
                                public void onSuccess(List<TIMMessage> msgs) {//获取消息成功

                                    //遍历取得的消息

                                    JSONArray msgListJson = parseMsgList(msgs);
                                    if(msgListJson!=null && msgListJson.length() > 0){
                                        try {
                                            JSONObject conversationJson  = null;
                                            String conversationId = msgs.get(0).getConversation().getPeer();
                                            for (int j = 0; j < newArray.length(); j++) {
                                                JSONObject tempConversationJson = newArray.getJSONObject(j);
                                                String  id = tempConversationJson.getString("identifier");
                                                if(conversationId.equals(id)){
                                                    conversationJson = tempConversationJson;
                                                    break;
                                                }
                                            }

                                            conversationJson.put("lastmsg",msgListJson.getJSONObject(0));
                                            conversationJson.put("timestamp",msgListJson.getJSONObject(0).optLong("timestamp"));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    indexList.add(0);
                                    Log.i(tag, indexList.size() + " indexList size: " + newArray.length());
                                    if(indexList.size() == newArray.length()){
                                        Log.i(tag, "get conversation size: " + newArray.toString());
                                        JSUtil.execCallback(pWebview, CallBackID, newArray, JSUtil.OK, false);
                                    }

                                    //
                                }
                            });



                        }catch (JSONException e){
                            e.printStackTrace();
                            Log.i(tag, "get conversation ---error:"+Log.getStackTraceString(e));
                        }
                    }

                    Log.i(tag, "get conversation simple info: " + newArray.toString());


                }
            });

            //if not login....

//           groupInfos = getGroupsInfo();
//            JSONArray newArray = new JSONArray();
//
//
//            final  int count = groupInfos.size();
//
//            for (int i = 0; i < count;i++) {
//
//                final TIMGroupCacheInfo groupCacheInfo = groupInfos.get(i);
//
//                try{
//                    JSONObject jsonObject = new JSONObject();
//                    jsonObject.put("identifier",groupCacheInfo.getGroupInfo().getGroupId());
//                    String  convType = GROUP;
//                    jsonObject.put("name",groupCacheInfo.getGroupInfo().getGroupName());
//                    jsonObject.put("type",convType);
//
//                    newArray.put(jsonObject);
//
//
//
//                }catch (JSONException e){
//                    e.printStackTrace();
//                    Log.i(tag, "get conversation ---error:"+Log.getStackTraceString(e));
//                }
//            }
//
//            Log.i(tag, "get conversation size: " + newArray.toString() + " type: " + type);
//
//
//            JSUtil.execCallback(pWebview, CallBackID, newArray, JSUtil.OK, false);
           return;

        }else if(type.equals("c2c")){
            for (TIMConversation conversation:conversationList
                    ) {
                if(conversation.getType() == TIMConversationType.C2C){
                    filterConversationList.add(conversation);
                }

            }

        }else{
            groupInfos = getGroupsInfo();

            filterConversationList = conversationList;

        }




        JSONArray newArray = new JSONArray();


        final  int count = filterConversationList.size();

        for (int i = 0; i < count;i++) {

           final TIMConversation conversation = filterConversationList.get(i);

           try{
               JSONObject jsonObject = new JSONObject();
               jsonObject.put("identifier",conversation.getPeer());
               String  convType = C2C;
               Log.i(tag, conversation.getType().name()+" find conversation ---"+TIMConversationType.Group.name());
               if(conversation.getType() == TIMConversationType.Group){
                   convType =GROUP;
                   for (TIMGroupCacheInfo item : groupInfos){
                       if (conversation.getPeer().equals(item.getGroupInfo().getGroupId())){
                           Log.i(tag, "find conversation ---break");
                           jsonObject.put("name",item.getGroupInfo().getGroupName());
                           break;
                       }
                   }

               }else if(conversation.getType() == TIMConversationType.Invalid || conversation.getType() == TIMConversationType.System){
                   Log.i(tag, "get conversation --type-error:"+conversation.getType().name());
                   continue;
               }
               jsonObject.put("type",convType);
               TIMConversationExt conExt = new TIMConversationExt(conversation);
//获取会话未读数
               long num = conExt.getUnreadMessageNum();
               Log.d(tag, "unread msg num: " + num);
               jsonObject.put("unread",num);

               newArray.put(jsonObject);



           }catch (JSONException e){
               e.printStackTrace();
               Log.i(tag, "get conversation ---error:"+Log.getStackTraceString(e));
           }
        }

        Log.i(tag, "get conversation size: " + newArray.toString() + " type: " + type);


        JSUtil.execCallback(pWebview, CallBackID, newArray, JSUtil.OK, false);





    }

    private  TIMConversation getConversation(JSONObject cmdObject){
        //获取会话扩展实例
        String type = cmdObject.optString("type");
        String identifier = cmdObject.optString("identifier");
        Log.i(tag,"identifier=="+identifier+"; type=="+type);
        TIMConversationType timConversationType = TIMConversationType.C2C;
        if(type.equals(GROUP)){
            timConversationType = TIMConversationType.Group;
        }

        TIMConversation con = TIMManager.getInstance().getConversation(timConversationType, identifier);
        return  con;
    }

    private  void logout(final IWebview pWebview,final String CallBackID, final JSONObject object){
        final String identifier = object.optString("identifier");
        if(identifier!=null)
        {
            //登出
            TIMManager.getInstance().logout(new TIMCallBack() {
                @Override
                public void onError(int code, String desc) {

                    //错误码code和错误描述desc，可用于定位请求失败原因
                    //错误码code列表请参见错误码表
                    Log.d(tag, "logout failed. code: " + code + " errmsg: " + desc);
                    //初始化本地存储
                    JSUtil.execCallback(pWebview, CallBackID, new JSONArray(), JSUtil.ERROR, false);
                }

                @Override
                public void onSuccess() {
                    //登出成功
                    clearLoginInfo();
                    Log.i(tag, "logout sucess");
                    JSUtil.execCallback(pWebview, CallBackID, new JSONArray(), JSUtil.OK, false);
                }
            });

        }
    }



    @TargetApi(Build.VERSION_CODES.N)
    private String pushEnable(final IWebview pWebview, JSONObject cmdObject){
        NotificationManager manager =(NotificationManager) mApplicationContext.getSystemService(Context.NOTIFICATION_SERVICE);;
        boolean isOpened = manager.areNotificationsEnabled();
        return  JSUtil.wrapJsVar(isOpened);
    }

    private  void openPush(final IWebview pWebview,final String CallBackID, final JSONObject object){
        // 根据isOpened结果，判断是否需要提醒用户跳转AppInfo页面，去打开App通知权限
        Intent intent = new Intent();
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", mApplicationContext.getPackageName(), null);
        intent.setData(uri);
        mApplicationContext.startActivity(intent);
    }

    private void login(final IWebview pWebview,final String CallBackID, final JSONObject object)
    {
        // 原生代码中获取JS层传递的参数，
        // 参数的获取顺序与JS层传递的顺序一致


        final String identifier = object.optString("identifier");
        final String userSig = object.optString("userSig");
        Log.i(tag,"identifier=="+identifier+"; userSig=="+userSig);



        //初始化本地存储
        TIMManagerExt.getInstance().initStorage(identifier, new TIMCallBack() {
            @Override
            public void onError(int code, String desc) {
                callLogin(pWebview,CallBackID,identifier,userSig);
                Log.e(tag, "initStorage failed, code: " + code + "|descr: " + desc);
            }

            @Override
            public void onSuccess() {
                Log.i(tag, "initStorage succ");
                callLogin(pWebview,CallBackID,identifier,userSig);
               // JSUtil.execCallback(pWebview, CallBackID, new JSONArray(), JSUtil.OK, false);
            }
        });

    }


    void callLogin(final IWebview pWebview, final String CallBackID, final String identifier, final String userSig){
        // identifier为用户名，userSig 为用户登录凭证
        final JSONArray newArray = new JSONArray();
        TIMManager.getInstance().login(identifier, userSig, new TIMCallBack() {
            @Override
            public void onError(int code, String desc) {
                //错误码code和错误描述desc，可用于定位请求失败原因
                //错误码code列表请参见错误码表
                Log.d(tag, "login failed. code: " + code + " errmsg: " + desc);
                // 调用方法将原生代码的执行结果返回给js层并触发相应的JS层回调函数
                JSUtil.execCallback(pWebview, CallBackID, newArray, JSUtil.ERROR, false);
            }

            @Override
            public void onSuccess() {
                saveLoginInfo(identifier,userSig);
                Log.d(tag, "login succ");
                // 调用方法将原生代码的执行结果返回给js层并触发相应的JS层回调函数
                JSUtil.execCallback(pWebview, CallBackID, newArray, JSUtil.OK, false);
            }
        });
    }

    private  static  JSONObject createListenerData(Object info) throws  JSONException{
        JSONObject data = new JSONObject();
        data.put("data",info);

        return  data;
    }


    private  static  boolean inited = false;

    public static void init(final Context mContext) {
        Log.i(tag, "init im sdk");
        if(inited){
            Log.i(tag, "already init im sdk");
            return;
        }
        inited = true;
        context = mContext;
       cacheDir = !isExternalStorageWritable()?mContext.getFilesDir():mContext.getExternalCacheDir();
        TIMSdkConfig config = new TIMSdkConfig(1400054346);
        config.enableLogPrint(true)
                .setLogLevel(TIMLogLevel.DEBUG);
        //初始化imsdk
        TIMManager.getInstance().init(mContext, config);
        if(MsfSdkUtils.isMainProcess(mContext)) {
            TIMManager.getInstance().setOfflinePushListener(new TIMOfflinePushListener() {
                @Override
                public void handleNotification(TIMOfflinePushNotification notification) {
                    if (notification.getGroupReceiveMsgOpt() == TIMGroupReceiveMessageOpt.ReceiveAndNotify){
                        //消息被设置为需要提醒
                        //notification.doNotify(context, R.drawable.icon);
                    }
                }
            });
        }


        //基本用户配置
        TIMUserConfig userConfig = new TIMUserConfig()
//                //设置群组资料拉取字段
//                .setGroupSettings(initGroupSettings())
//                //设置资料关系链拉取字段
//                .setFriendshipSettings(initFriendshipSettings())
                //设置用户状态变更事件监听器
                .setUserStatusListener(new TIMUserStatusListener() {
                    @Override
                    public void onForceOffline() {
                        //被其他终端踢下线
                        Log.i(tag, "onForceOffline");
                    }

                    @Override
                    public void onUserSigExpired() {
                        //用户签名过期了，需要刷新userSig重新登录SDK
                        Log.i(tag, "onUserSigExpired");
                    }
                })
                //设置连接状态事件监听器
                .setConnectionListener(new TIMConnListener() {
                    @Override
                    public void onConnected() {
                        Log.i(tag, "onConnected");
                    }

                    @Override
                    public void onDisconnected(int code, String desc) {
                        Log.i(tag, "onDisconnected");
                    }

                    @Override
                    public void onWifiNeedAuth(String name) {
                        Log.i(tag, "onWifiNeedAuth");
                    }
                })
                //设置群组事件监听器
                .setGroupEventListener(new TIMGroupEventListener() {
                    @Override
                    public void onGroupTipsEvent(TIMGroupTipsElem elem) {
                        Log.i(tag, "onGroupTipsEvent, type: " + elem.getTipsType());
                    }
                })
                //设置会话刷新监听器
                .setRefreshListener(new TIMRefreshListener() {
                    @Override
                    public void onRefresh() {
                        Log.i(tag, "onRefresh");
                    }

                    @Override
                    public void onRefreshConversation(List<TIMConversation> conversations) {
                        Log.i(tag, "onRefreshConversation, conversation size: " + conversations.size());
                        JSONObject data = null;
                        try {
                            data = createListenerData(new JSONObject());
                            notifyListeners(ACTION_UPDATE_CONVERSATION,data);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

//消息扩展用户配置
        userConfig = new TIMUserConfigMsgExt(userConfig)
                //禁用消息存储
                .enableStorage(false)
                //开启消息已读回执
                .enableReadReceipt(true);

//资料关系链扩展用户配置
        userConfig = new TIMUserConfigSnsExt(userConfig)
                //开启资料关系链本地存储
                .enableFriendshipStorage(true)
                //设置关系链变更事件监听器
                .setFriendshipProxyListener(new TIMFriendshipProxyListener() {
                    @Override
                    public void OnAddFriends(List<TIMUserProfile> users) {
                        Log.i(tag, "OnAddFriends");
                    }

                    @Override
                    public void OnDelFriends(List<String> identifiers) {
                        Log.i(tag, "OnDelFriends");
                    }

                    @Override
                    public void OnFriendProfileUpdate(List<TIMUserProfile> profiles) {
                        Log.i(tag, "OnFriendProfileUpdate");
                    }

                    @Override
                    public void OnAddFriendReqs(List<TIMSNSChangeInfo> reqs) {
                        Log.i(tag, "OnAddFriendReqs");
                    }

//                    @Override
//                    public void OnAddFriendGroups(List<TIMFriendGroup> friendgroups) {
//                        Log.i(tag, "OnAddFriendGroups");
//                    }
//
//                    @Override
//                    public void OnDelFriendGroups(List<String> names) {
//                        Log.i(tag, "OnDelFriendGroups");
//                    }
//
//                    @Override
//                    public void OnFriendGroupUpdate(List<TIMFriendGroup> friendgroups) {
//                        Log.i(tag, "OnFriendGroupUpdate");
//                    }
                });

//群组管理扩展用户配置
        userConfig = new TIMUserConfigGroupExt(userConfig)
                //开启群组资料本地存储
                .enableGroupStorage(true)
                //设置群组资料变更事件监听器
                .setGroupAssistantListener(new TIMGroupAssistantListener() {
                    @Override
                    public void onMemberJoin(String groupId, List<TIMGroupMemberInfo> memberInfos) {
                        Log.i(tag, "onMemberJoin");
                        JSONObject data = null;
                        try {
                            data = createListenerData(new JSONObject());
                            notifyListeners(ACTION_UPDATE_CONVERSATION,data);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onMemberQuit(String groupId, List<String> members) {
                        Log.i(tag, "onMemberQuit");
                        JSONObject data = null;
                        try {
                            data = createListenerData(new JSONObject());
                            notifyListeners(ACTION_UPDATE_CONVERSATION,data);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onMemberUpdate(String groupId, List<TIMGroupMemberInfo> memberInfos) {
                        Log.i(tag, "onMemberUpdate");
                        JSONObject data = null;
                        try {
                            data = createListenerData(new JSONObject());
                            notifyListeners(ACTION_UPDATE_CONVERSATION,data);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onGroupAdd(TIMGroupCacheInfo groupCacheInfo) {
                        Log.i(tag, "onGroupAdd");
                        JSONObject data = null;
                        try {
                            data = createListenerData(new JSONObject());
                            notifyListeners(ACTION_UPDATE_CONVERSATION,data);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onGroupDelete(String groupId) {
                        Log.i(tag, "onGroupDelete");
                        JSONObject data = null;
                        try {
                            data = createListenerData(new JSONObject());
                            notifyListeners(ACTION_UPDATE_CONVERSATION,data);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onGroupUpdate(TIMGroupCacheInfo groupCacheInfo) {
                        Log.i(tag, "onGroupUpdate");
                        JSONObject data = null;
                        try {
                            data = createListenerData(new JSONObject());
                            notifyListeners(ACTION_UPDATE_CONVERSATION,data);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

//将用户配置与通讯管理器进行绑定
        TIMManager.getInstance().setUserConfig(userConfig);




        //设置消息监听器，收到新消息时，通过此监听器回调
        TIMManager.getInstance().addMessageListener(new TIMMessageListener() {
            @Override
            public boolean onNewMessages(List<TIMMessage> list) {

                Log.i(tag, "onNewMessages");

                JSONObject data = new JSONObject();
                try {
                    JSONArray msgList =  parseMsgList(list);
                    data = createListenerData(msgList);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                notifyListeners(ACTION_NEW_MSG,data);
                return false;
            }//消息监听器

        });





    }
}
