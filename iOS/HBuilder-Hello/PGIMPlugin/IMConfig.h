//
//  IMConfig.h
//  HBuilder-Hello
//
//  Created by 江源泉 on 2018/2/28.
//  Copyright © 2018年 DCloud. All rights reserved.
//

#ifndef IMConfig_h
#define IMConfig_h

#define kSdkAppId @"1400054346"
#define kSdkAccountType @"20198"
#define kIMPlugin_AutoSendLogin_Key @"kIMPlugin_AutoSendLogin_Key"
#define kIMPlugin_AutoLogin_Key @"kIMPlugin_AutoLogin_Key"
#define kIMPlugin_AutoGetGroupList_Key @"kIMPlugin_AutoGetGroupList_Key"
#define kIMPlugin_AutoGetConversationList_Key @"kIMPlugin_AutoGetConversationList_Key"

#define kIMPlugin_CurrentUserIdentifier_Key @"kIMPlugin_CurrentUserIdentifier_Key"
#define kIMPlugin_CurrentUserSig_Key @"kIMPlugin_CurrentUserSig_Key"
#define testUsetSign @"eJxFkE1Pg0AQQP8LZ2Nm2V3KmnigfLQVW2Jbm-S0oWWhk*pCltWAxv8uEhqv72Uy8*bb2T-v7vOmwULmVlJTOA8OOHcjVl2DRsm8tMoMmHDOXYCb-VSmxVoPwgXCiUsB-iUWSlsscRz8KoGDDwzIZFusBryOX8PVoqySPakPTyJc5laf0jh6m0fHLjstWQxd-3KpUnc1uxxhHWAcUN*gaJKMaoPXeVuw1BWthk2WRNzbbkOqvE0gdr2O4PG2rLjKMfAvgQ0nckaZN0mL72pMI0LMOPHpxPPzuf7QVtq*UeNHfn4B33FWew__"

#define GROUP @"group"
#define C2C @"c2c"
#define ACTION_NEW_MSG @"action_new_msg"
#define ACTION_UPDATE_CONVERSATION @"action_update_conversation"



#endif /* IMConfig_h */
