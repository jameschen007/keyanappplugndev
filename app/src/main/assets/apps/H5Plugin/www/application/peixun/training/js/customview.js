// NOTE: 
// Modifying the URL below to another server will likely *NOT* work. Because of browser
// security restrictions, we have to use a file server with special headers
// (CORS) - most servers don't support cross-origin browser requests.
//

//
// Disable workers to avoid yet another cross-origin issue (workers need the URL of
// the script to be loaded, and currently do not allow cross-origin scripts)
//
PDFJS.disableWorker = false;

var pdfDoc = null,
	pageNum = 1,
	pageRendering = false,
	pageNumPending = null,
	scale = 1.0,
	canvas = document.getElementById('the-canvas'),
	page,
	ctx = canvas.getContext('2d');
var DEFAULT_SCALE_DELTA = 1.1;
var MIN_SCALE = 0.25;
var MAX_SCALE = 10.0;

function setPDFDoc(pdfdoc){
	pdfDoc =pdfdoc;
}

function renderScale(scaleSet) {
	if(!pdfDoc) {
		return
	}
	scale = scaleSet;
	renderPage(pageNum);
}

function pdfViewZoomOut() {
	var newScale = scale;

	newScale = (newScale * DEFAULT_SCALE_DELTA).toFixed(2);
	newScale = Math.ceil(newScale * 10) / 10;
	newScale = Math.min(MAX_SCALE, newScale);

	scale = newScale;
	queueRenderPage(pageNum);
}

function pdfViewZoomIn() {
	var newScale = scale;

	newScale = (newScale / DEFAULT_SCALE_DELTA).toFixed(2);
	newScale = Math.floor(newScale * 10) / 10;
	newScale = Math.max(MIN_SCALE, newScale);

	scale = newScale;
	queueRenderPage(pageNum);
}

/**
 * Get page info from document, resize canvas accordingly, and render page.
 * @param num Page number.
 */
function renderPage(num) {
	pageRendering = true;
	// Using promise to fetch the page
	pdfDoc.getPage(num).then(function(pageobj) {
		page = pageobj;
		var viewport = page.getViewport((window.innerWidth - 4) / page.getViewport(scale).width);
		//var viewport = page.getViewport(scale);
		canvas.height = viewport.height;
		canvas.width = viewport.width;

		// Render PDF page into canvas context
		var renderContext = {
			canvasContext: ctx,
			viewport: viewport
		};
		var renderTask = page.render(renderContext);

		// Wait for rendering to finish
		renderTask.promise.then(function() {
			pageRendering = false;
			if(pageNumPending !== null) {
				// New page rendering is pending
				renderPage(pageNumPending);
				pageNumPending = null;
			}
		});
	});

	// Update page counters
	document.getElementById('page_num').textContent = pageNum;
	document.getElementById('pageNumber').value = pageNum;
	
	
}

/**
 * If another page rendering in progress, waits until the rendering is
 * finished. Otherwise, executes rendering immediately.
 */
function queueRenderPage(num) {
	if(pageRendering) {
		pageNumPending = num;
	} else {
		renderPage(num);
	}
}

function jumpPage(index) {
	if(index <= 1 || index >= pdfDoc.numPages) {
		return;
	}
	pageNum = index;
	queueRenderPage(pageNum);
}

/**
 * Displays previous page.
 */
function onPrevPage() {
	if(pageNum <= 1) {
		return;
	}
	pageNum--;
	queueRenderPage(pageNum);
}

/**
 * Displays next page.
 */
function onNextPage() {
	if(pageNum >= pdfDoc.numPages) {
		return;
	}
	pageNum++;
	queueRenderPage(pageNum);
}

