//
//  NotifyModel.h
//  HBuilder
//
//  Created by 江源泉 on 2018/3/7.
//  Copyright © 2018年 DCloud. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NotifyModel : NSObject<NSCoding>
@property (nonatomic,retain) NSString *callbackId;
@property (nonatomic,retain) NSString *action;

//+ (NotifyModel *)sharedNotifyModel;

@end
