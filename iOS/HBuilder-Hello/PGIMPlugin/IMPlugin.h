//
//  IMPlugin.h
//  HBuilder
//
//  Created by 江源泉 on 2018/2/1.
//  Copyright © 2018年 DCloud. All rights reserved.
//

#import "PGPlugin.h"
#import "PGMethod.h"
#import "NotifyModel.h"
#import <Foundation/Foundation.h>
#import <ImSDK/ImSDK.h>
#import <IMMessageExt/IMMessageExt.h>
#import <IMGroupExt/IMGroupExt.h>
#import <IMFriendshipExt/IMFriendshipExt.h>

typedef void (^Result)(id response,NSError *error);

@interface IMPlugin : PGPlugin<TIMConnListener,TIMUserStatusListener,TIMRefreshListener,TIMFriendshipListener,TIMGroupListener,TIMMessageListener>
@property(nonatomic,strong) NSMutableArray *messageList;
@property(nonatomic,strong) NSMutableArray *messageModelList;
@property(nonatomic,strong) NSMutableArray *listners;
@property(nonatomic,strong) NSArray *listnersArr;
@property(nonatomic,strong) NSString *getConCallBackId;
@property(nonatomic,strong) NSDictionary *getConDic;

+ (instancetype)sharedInstance;
-(instancetype)init;
// **** 指令方法  **** //
-(void)callIMCmd:(PGMethod *)command;  //同步指令
-(void)registerIMListener:(PGMethod *)command; //注册监听
-(void)unregisterIMListener:(PGMethod *)command; //注销监听
-(NSData*)callIMCmdSync:(PGMethod *)command; //异步指令




// **** 功能方法  **** //
//***********************     初始化IMSDK     ***********************//
-(void)initIM;
//***********************     登录IMSDK     ***********************//
-(void)reLogin:(NSString *)cbId resultDic:(NSDictionary *)resultDic;
- (int)login:(TIMLoginParam*)param cbId:(NSString *)cbId resultDic:(NSDictionary *)resultDic succ:(TIMLoginSucc)succ fail:(TIMFail)fail;
//登录后获取群组列表
- (int)loginGetGroupList:(TIMLoginParam*)param cbId:(NSString *)cbId resultDic:(NSDictionary *)resultDic succ:(TIMLoginSucc)succ fail:(TIMFail)fail;
// 设置是否已登录
- (void)setAutoLogin:(BOOL)autologin;
// 判断是否已登录并保存本地
- (BOOL)isAutoLogin;
// 判断SDK是否已登录
- (BOOL)isLogin;
// 设置是否发送登录指令  ，因为发送登录指令后50s内不能重复发送，否则报错
- (void)setAutoSendLogin:(BOOL)autoSendLogin;
// 判断是否发送登录指令
- (BOOL)isAutoSendLogin;
// 设置是否获取群组列表
- (void)setGroupList:(BOOL)autoGet;
// 判断是否获取群组列表
- (BOOL)isGetGroupList;

//***********************     登出IMSDK     ***********************//
- (void)logout:(NSString *)callBackid Result:(NSDictionary *)resultDic succ:(TIMLoginSucc)succ fail:(TIMFail)fail;
//***********************     获取群列表     ***********************//
-(void)getGroupList:(NSString *)callBackid Result:(NSDictionary *)resultDic;
//***********************     获取会话对讲机列表     ***********************//
-(void)getConversationList:(NSString *)callBackid Result:(NSDictionary *)resultDic;
//***********************     创建群组     ***********************//
-(void)creatGroup:(NSString *)callBackid Result:(NSDictionary *)resultDic;
//***********************     发送语音文件消息     ***********************//
-(void)sendMessage:(NSString *)callBackid Result:(NSDictionary *)resultDic;
//***********************     获取本地消息列表     ***********************//
-(void)getLocalMessageList:(NSString *)callBackid Result:(NSDictionary *)resultDic;
//获取会话本地消息列表
-(void)getLocalMessage:(TIMConversation *)Conversation timMessage:(TIMMessage *)timMessage size:(int)size callBackid:(NSString *)callBackid;
//获取会话最新一条消息
-(void)getMessage:(TIMConversation *)Conversation groupDic:(NSDictionary *)groupDic timMessage:(TIMMessage *)timMessage size:(int)size callBackid:(NSString *)callBackid callback:(Result)callback;
//解析会话消息列表
-(NSArray *)parseMsgList:(NSArray *)MsgList;
//解析消息转变为jsonDic 格式
-(NSDictionary *)parseMsg:(TIMMessage *)msg;
//解析群系统消息 ，返回显示消息
-(NSString *)parseGroupTips:(NSDictionary *)groupDic TipsMsg:(TIMMessage *)TipsMsg;
//***********************     录音并获取语音文件     ***********************//
-(void)getVoiceFile:(NSString *)callBackid Result:(NSDictionary *)resultDic;
//***********************     退出群组     ***********************//
-(void)quitGroup:(NSString *)callBackid Result:(NSDictionary *)resultDic;
//***********************     删除群成员     ***********************//
-(void)deleteGroupMember:(NSString *)callBackid Result:(NSDictionary *)resultDic;
//***********************     添加群成员     ***********************//
-(void)addGroupMember:(NSString *)callBackid Result:(NSDictionary *)resultDic;
//***********************     获取群成员信息     ***********************//
-(void)getGroupMember:(NSString *)callBackid Result:(NSDictionary *)resultDic;
//***********************     更新群组名     ***********************//
-(void)updateGroupName:(NSString *)callBackid Result:(NSDictionary *)resultDic;
//***********************     设置会话的所以消息标记为已读     ***********************//
-(void)setReadMsg:(NSString *)callBackid Result:(NSDictionary *)resultDic;
//***********************     发送文本消息     ***********************//
-(void)resendMessage:(NSString *)callBackid Result:(NSDictionary *)resultDic;
//***********************     发送事件通知     ***********************//
-(void)sendEvent:(NSString *)callBackid Result:(NSDictionary *)resultDic;


@end



//通知回调

