function callMock(type, apiName, body, succ) {
	var result = {};
	result.code == 1000;
	var responseResult = {};
	result.responseResult = responseResult;

	if(type == 'post') {
		if(apiName == '/exam') {
			result.result = testQuestions();

		}
	} else {

		if(apiName == '/getUserList') {
			result.responseResult.data = testContacts();

		}
	}
	console.log('api='+apiName+' mock data ='+JSON.stringify(result));
	succ(result, body, JSON.stringify(result));
}

function testContacts() {
	contacts = [];
	for(var i = 0; i < 15; i++) {

		var newContact = {
			name: 'test' + i,

		};
		contact = newContact;
		contacts.push(contact);
	}
	return contacts;
}

function testQuestions() {
	questions = [];
	for(var i = 0; i < 5; i++) {
		var itemsArray = [{
			'content': '呵呵呵呵😑',
			'index': 'a'
		}, {
			'content': '加班好造孽',
			'index': 'b'
		}, {
			'content': '😂😂😂😂😂',
			'index': 'c'
		}, {
			'content': '下班了',
			'index': 'd'
		}];
		multiCorrect = ['a', 'b', 'c'];
		if(i == 3) {
			multiCorrect = ['b', 'c'];
		}
		var question = questions[i];
		if(!question) {
			var type = 'multi';
			if(i % 2 == 0) {
				type = 'single';
				correct = 'b';
			}
			if(i == 4) {
				type = 'YesOrNo';
				itemsArray = [{
					'content': '正确',
					'index': 'a'
				}, {
					'content': '错误',
					'index': 'b'
				}, ];
				correct = 'a';
			}

			var newQuestion = {
				type: type,
				subject: i + 1 + '、这是一个测试题目：',
				items: itemsArray,
				score: 3,
				correct: correct,
				multiCorrect: multiCorrect

			};
			question = newQuestion;
			questions.push(newQuestion);
		}
	}
	return questions;
}